<?php

class MY_Form_validation extends CI_Form_validation {

    /**
     * Is Unique
     *
     * Check if the input value doesn't already exist
     * in the specified database field.
     *
     * @param	string	$str
     * @param	string	$field
     * @return	bool
     */
    public function is_unique_with_except_record($str, $field) {
        sscanf($field, '%[^.].%[^.].%[^.].%[^.]', $table, $field, $column, $value);
        return isset($this->CI->db) ? ($this->CI->db->limit(1)->where($field, $str)->where_not_in($column, $value)->get($table)->num_rows() === 0) : FALSE;
    }

}
