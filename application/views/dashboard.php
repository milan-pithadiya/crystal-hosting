<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
      <link rel="stylesheet" href="<?=base_url('assets/css/new-css.css')?>">
    </head>
    <body>
      <div class="main aos-all" id="transcroller-body">
        <?php $this->load->view('include/header');?>
        <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
        <!-- about start -->
        <div class="allpage_banner_about allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'about.jpg')?>);">
            <h1 class="title_h1">Dashboard</h1>
            <p><a href="<?=base_url()?>">Home </a> / Dashboard</p>
        </div>
        <!-- domain_style start -->
        <div class="padding_all text-center domain_style">
          <div class="container">
            <h5></h5>
            <div class="row">
              <!-- <div class="col-md-3 pull-md-left sidebar">
                <div menuitemname="Client Details" class="panel panel-default">
                  <div class="panel-heading">
                      <h3 class="panel-title"> <i class="fa fa-user"></i>&nbsp; Your Info
                      </h3>
                  </div>
                  <div class="panel-body">
                    <p><strong><span id="first_name"></span> <span id="last_name"></span></strong></p><p></p>
                    <p>
                      <span id="address"></span>
                      <span id="country"></span>,<span id="state"></span>,<span id="city"></span>
                    </p>
                  </div>
                  <div class="panel-footer clearfix">
                    <a href="<?=base_url('profile')?>" class="btn btn-success btn-sm btn-block">
                      <i class="fa fa-pencil"></i> Update
                    </a>
                  </div>
                  <div menuitemname="Client Contacts" class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">
                      <i class="fa fa-folder-o"></i>&nbsp;                Contacts
                      </h3>
                    </div>
                    <div class="list-group">
                      <div menuitemname="No Contacts" class="list-group-item" id="          Secondary_Sidebar-Client_Contacts-No_Contacts">
                        No Contacts Found
                      </div>
                    </div>
                    <div class="panel-footer clearfix">
                      <a href="#" class="btn btn-default btn-sm btn-block">
                      <i class="fa fa-plus"></i> New Contact...
                      </a>
                    </div>
                  </div>
                  <div menuitemname="Client Shortcuts" class="panel panel-default">
                    <div class="panel-heading">
                      <h3 class="panel-title">
                      <i class="fa fa-bookmark"></i>&nbsp;  Shortcuts
                      </h3>
                    </div>
                    <div class="list-group">
                      <a menuitemname="Register New Domain" href="<?=base_url('domains')?>" class="list-group-item" id="Secondary_Sidebar-Client_Shortcuts-Register_New_Domain">
                        <i class="fa fa-globe fa-fw"></i>&nbsp; Register a New Domain
                      </a>
                      <a menuitemname="Logout" href="<?=base_url('login/logout')?>" class="list-group-item" id="     Secondary_Sidebar-Client_Shortcuts-Logout">
                        <i class="fa fa-arrow-left fa-fw"></i>&nbsp;  Logout
                      </a>
                    </div>
                  </div>
                </div>
              </div> -->
              <?php $this->load->view('include/user_sidebar');?>
              <div class="col-md-9 ">
                <h3 class="well">Welcome.. <?=$this->session->username.' !';?></h3>
              
                <div class="row">
                  <div class="col-sm-3 col-xs-6 tile">
                    <a href="<?=base_url('my-hostings')?>">
                        <div class="icon"><i class="fa fa-cube"></i></div>
                        <div class="stat"><?=get_user_hosting_count($this->session->login_id)?></div>
                        <div class="title">Hostings</div>
                        <div class="highlight bg-color-blue"></div>
                    </a>
                  </div>
                  <div class="col-sm-3 col-xs-6 tile">
                      <a href="<?=base_url('my-domains')?>">
                          <div class="icon"><i class="fa fa-globe"></i></div>
                          <div class="stat"><?=get_user_domain_count($this->session->login_id)?></div>
                          <div class="title">Domains</div>
                          <div class="highlight bg-color-blue"></div>
                      </a>
                  </div>
                  <div class="col-sm-3 col-xs-6 tile">
                      <a href="<?=base_url('ticket')?>">
                          <div class="icon"><i class="fa fa-comments"></i></div>
                          <div class="stat"><?=get_user_ticket_count($this->session->login_id)?></div>
                          <div class="title">Tickets</div>
                          <div class="highlight bg-color-blue"></div>
                      </a>
                  </div>
                  <div class="col-sm-3 col-xs-6 tile">
                      <a href="<?=base_url('my-invoices')?>">
                          <div class="icon"><i class="fa fa-credit-card-alt" aria-hidden="true"></i></div>
                          <div class="stat"><?=get_user_invoice_count($this->session->login_id)?></div>
                          <div class="title title-4">Invoice</div>
                          <div class="highlight bg-color-blue"></div>
                      </a>
                  </div>
                  <div class="col-md-12 home-kb-search">
                      <!-- <input name="search" class="form-control input-lg" placeholder="Enter a question here to search our knowledgebase for answers..." type="text">
                      <i class="fa fa-search"></i> -->
                  </div>
                </div>
                <div class="invoice_style">
                  <div class="row">
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 mb-20">
                      <div class="unpaid_invoice">
                        <div class="unpaid_invoice_header">
                          <div class="unpaid_invoice_title">
                            <ul>
                              <li><h5>Unpaid Invoice</h5></li>
                              <li class="pay-now-btn"><h5><a href="<?=base_url('my-invoices')?>" class="pay_now">  Pay Now</a></h5></li>
                            </ul>
                          </div>
                        </div>
                        <div class="unpaid_invoice_content" id="unpaid_invoice">
                          <!-- <p>You have 1 overdue invoice(s) with a total balance due of Rs.26318.84 INR. Pay them now to avoid any interruptions in service.</p> -->
                        </div>
                      </div>
                    </div>
                    <!-- <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 mb-20">
                      <div class="unpaid_invoice">
                        <div class="unpaid_invoice_header">
                          <div class="unpaid_invoice_title">
                            <ul>
                              <li><h5>Register A New Domain</h5></li>
                            </ul>
                          </div>
                        </div>
                        <div class="unpaid_invoice_content">
                          <form class="row">
                            <div class="form-group col-md-8 col-lg-8 col-sm-6 col-xs-12">
                              <input type="text" name="domain" class="form-control">
                            </div>
                            <div  class="form-group col-md-4 col-lg-4 col-sm-6 col-xs-12">
                              <button type="button" class="btn btn-success">Register</button>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div> -->
                    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 mb-20">
                      <div class="unpaid_invoice">
                        <div class="unpaid_invoice_header">
                          <div class="unpaid_invoice_title">
                            <ul>
                              <li><h5>Recent Support Tickets</h5></li>
                              <li class="pay-now-btn"><h5><a href="<?=base_url('ticket')?>" class="pay_now">  Open New Tickets</a></h5></li>
                            </ul>
                          </div>
                        </div>
                        <div id="view_tickets"></div>
                      </div>
                    </div>
                    <!-- <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12 mb-20">
                      <div class="unpaid_invoice">
                        <div class="unpaid_invoice_header">
                          <div class="unpaid_invoice_title">
                            <ul>
                              <li><h5>Recent News</h5></li>
                              <li class="pay-now-btn"><h5><a href="#" class="pay_now">  View All</a></h5></li>
                            </ul>
                          </div>
                        </div>
                        <div class="unpaid_invoice_content">
                          <a href="#">Update Your GST Number<br><span class="text-last-update">21/08/2017</span></a>
                        </div>
                      </div>
                    </div> -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- domain_style over -->
        <!-- help_line start -->
        <div class="padding_all help_line" style="background-image: url(<?=base_url(IMAGES.'mail.jpg')?>);">
          <div class="container text-center">
            <h1 class="h1_title">Need Help?</h1>
              <h4>Let us help you make the right decision!</h4>
            <div class="row margin_top">
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border">
                  <a href="javascript:;"><i class="fa fa-phone"></i></a>
                  <h3>Call Us</h3>
                  <p>Give us a call & ask all of your questions</p>
                </div>
              </div>
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border ">
                  <a href="javascript:;"><i class="fa fa-pencil"></i></a>
                  <h3>Email Us</h3>
                  <p>Send us an email with all of your questions</p>
                </div>
              </div>
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border margin_row">
                  <a href="javascript:;"><i class="fa fa-video-camera"></i></a>
                  <h3>Live Chat</h3>
                  <p>Chat with a member of our support team now</p>
                </div>
              </div>
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border margin_row">
                  <a href="javascript:;"><i class="fa fa-certificate"></i></a>
                  <h3>Real Reviews</h3>
                  <p>Read what real customers have to say</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php $this->load->view('include/footer');?>  
    </div>
    <?php $this->load->view('include/footer_js');?>
    <script type="text/javascript">
      $(document).ready(function () {
        get_user_profile('<?=$this->session->login_id?>');
        get_user_tickets();
        get_user_unpaid_invoice();
      });
      // function get_user_profile(user_id){
      //     var uurl = BASE_URL+"api/user/getProfile";
      //     // var post_data = {id:user_id};
      //      $.ajax({
      //          url: uurl,
      //          method: 'POST',
      //          dataType:'json',
      //          data: {id:user_id},
      //          //async: false,
      //          beforeSend: function(){
      //            $('.mask').show();
      //            $('#loader').show();
      //          },
      //          success: function(response){
      //           if (response.result=="Success") {
      //             var userdata = response.data;
      //             console.log(userdata);
      //             $('#first_name').text(userdata.first_name);
      //             $('#last_name').text(userdata.last_name);
      //             $('#email').text(userdata.email);
      //             $('#address').html(nl2br(userdata.address));
      //             $('#country').text(userdata.country_id_info);
      //             $('#state').text(userdata.state_id_info);
      //             $('#city').text(userdata.city_id_info);
      //           }
      //          },
      //          error: function(xhr) {
      //          //alert(xhr.responseText);
      //          },
      //          complete: function(){
      //            $('.mask').hide();
      //            $('#loader').hide();
      //          },
      //      });
          
      // }
    </script> 
    <script type="text/javascript">
      function get_user_tickets(){
          var uurl = BASE_URL+"dashboard/get_user_tickets";
          // var post_data = {id:user_id};
           $.ajax({
               url: uurl,
               method: 'POST',
               dataType:'html',
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){
                if (response) {
                  $('#view_tickets').html(response);
                }
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
           });
          
        }
        function get_user_unpaid_invoice(){
          var uurl = BASE_URL+"dashboard/get_user_unpaid_invoice";
           $.ajax({
               url: uurl,
               method: 'POST',
               dataType:'html',
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){
                console.log(response);
                if (response) {
                  $('#unpaid_invoice').html(response);
                }
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
           });
          
        }
    </script>
   </body>
</html> 