<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('include/header');
?>  
<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/chat-model.css')?>">   
<style type="text/css">
    .btn-primary {
        color: #fff !important;
        background-color: #009fff !important;
        border-color: #007bff !important;
    }
</style>
<!-- Login Area Start Here -->
<section class="s-space-bottom-full bg-accent-shadow-body">
    <div class="container-fluid">
        <div class="breadcrumbs-area">
            <ul>
                <li><a href="<?= base_url()?>">Home</a> -</li>
                <li class="active">Notification list</li>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12"> 
                <?php  $this->load->view('include/messages');?>
                <div class="success_message"></div>
                
                <div class="tab-content my-account-wrapper gradient-wrapper input-layout1">
                    <div role="tabpanel" class="tab-pane fade active show" id="personal">
                        <div class="gradient-title">
                        <h3>Notification list</h3>
                    </div>
                    
                    <div class="row">
                        <div class="col-lg-12">
                           <div class="gradient-wrapper item-mb">                                   
                                <div id="category-view" class="category-list-layout3">
                                    <div class="row">
                                        <?php
                                            if (isset($notification_list) && $notification_list !=null) 
                                            {                                               
                                                $profile_photo = $this->Production_model->get_all_with_where('user','','',array());

                                                foreach ($notification_list as $key => $value) {
                                                    $id = $value['product_id'];
                                                    $notification_id = $value['notification_id'];
                                                ?>
                                                    <div class="col-xl-4 col-lg-6 col-md-4 col-sm-6 col-12">
                                                        <div class="product-box item-mb zoom-gallery">
                                                            <div class="item-mask-wrapper">
                                                                <div class="">
                                                                    <?php
                                                                        if ($profile_photo != null) {
                                                                            ?>
                                                                                <a href="<?= base_url('post-management/details/'.$id)?>">
                                                                                    <img src="<?= $profile_photo !=null ? base_url('assets/uploads/profile_photo/').$profile_photo[0]['profile_photo'] : base_url('assets/uploads/profile_photo/default-image.png')?>" class="mg-fluid" alt="logo">
                                                                                </a>
                                                                            <?php
                                                                        }
                                                                    ?>
                                                                </div>
                                                            </div>
                                                            <div class="item-content">
                                                                <!-- <div class="title-ctg">Clothing</div> -->
                                                                
                                                                <h3 class="long-title"><a href="<?= base_url('post-management/details/'.$id)?>"><?= $value['title']?></a> </h3>
                                                                <!-- <i class="fa fa-clock-o" aria-hidden="true"></i><?= date('d M, Y h:i:s A',strtotime($value['created_date']))?> -->
                                                                <h3><?= $value['message']?></h3>

                                                                <?php
                                                                    if ($value['type'] == 2 && trim($value['buttons']) == 'Show') {
                                                                    ?>
                                                                        <h4><u>Post : <span class="post_status"><?= $value['post_status']?></span></u></h4>
                                                                        <a href="javascript:void(0)" title="Accept" class="product-details-btns btn btn-success change_status"  data-status='1' data-product-id="<?= $value['product_id']?>" data-seller-id="<?= $this->session->userdata('login_id')?>" data-buyer-id="<?= $value['buyer_id']?>">Accept</a>

                                                                        <a href="javascript:void(0)" title="Reject" class="product-details-btns btn btn-danger change_status"  data-status='2' data-product-id="<?= $value['product_id']?>" data-seller-id="<?= $this->session->userdata('login_id')?>" data-buyer-id="<?= $value['buyer_id']?>">Reject</a> 
  
                                                                    <?php }
                                                                    if ($value['type'] == 2 && $value['post_status'] == 'Accept') {
                                                                        
                                                                        if ($value['buttons'] == 'Show') {
                                                                            $buyer_id = $value['buyer_id'];
                                                                            $seller_id = $this->session->userdata('login_id');
                                                                        }
                                                                        if ($value['buttons'] == 'Hide') {
                                                                            $buyer_id = $this->session->userdata('login_id');
                                                                            $seller_id = $value['buyer_id'];
                                                                        }
                                                                    ?>
                                                                        <a style="color:#fff;" href="javascript:void(0)" class="btn btn-warning chat_seller" data-product-id="<?= $value['product_id']?>"
                                                                        data-buyer-id="<?= $buyer_id?>" data-seller-id="<?= $seller_id?>" data-message-send-by="<?= $this->session->userdata('login_id')?>"  data-toggle="modal" 
                                                                        data-target="#chatModal">
                                                                            Chat
                                                                        </a>
                                                                    <?php }
                                                                ?>
                                                                <a href="<?= base_url('notification/remove_notification/'.$this->session->userdata('login_id').'/'.$notification_id.'/1/1')?>" title="Remove" class="product-details-btns btn btn-primary" onclick="return confirm('Are you sure you want to delete this notification?');">Remove</a>  
                                                            </div>
                                                        </div>
                                                    </div>
                                                <?php } 
                                            }
                                            else{
                                                ?>
                                                    <h6 style="margin-left: 39%;">Notification not Available...!</h6>
                                                <?php
                                            }
                                        ?>
                                    </div>
                                </div>
                            </div>
                            <div class="gradient-wrapper mb--xs mb-30 border-none">
                                <?php
                                    if (isset($pagination) && $pagination !=null) {
                                        echo $pagination;
                                    }
                                ?>
                           </div>
                       </div>
                   </div>
                </div>                       
            </div>                    
        </div>
    </div>
</section>
<!-- Login Area End Here -->

<!--Chat module-->
<div id="chatModal" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">          
            <div class="modal-body">
                <div class="container">
                    <form method="post" id="form">
                        <input type="hidden" name="seller_id" class="seller_id">
                        <input type="hidden" name="buyer_id" class="buyer_id"> <!-- user-id -->
                        <input type="hidden" name="product_id" class="product_id">
                        <input type="hidden" name="message_send_by" class="message_send_by">

                        <h3 class=" text-center">Messaging</h3>
                        <div class="messaging">
                            <div class="inbox_msg">
                                <div class="mesgs">
                                    <div class="msg_history">
                                        <!-- <div class="incoming_msg">
                                            <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                                            <div class="received_msg">
                                                <div class="received_withd_msg">
                                                    <p>Test which is a new approach to have all
                                                        solutions
                                                    </p>
                                                    <span class="time_date"> 11:01 AM    |    June 9</span>
                                                </div>
                                            </div>
                                        </div> -->

                                        <!-- send message -->
                                        <div class="send_msg">
                                            <!-- <div class="outgoing_msg">
                                                <div class="sent_msg">
                                                    <p>Test which is a new approach to have all
                                                        solutions
                                                    </p>
                                                    <span class="time_date"> 11:01 AM    |    June 9</span> 
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                    <div class="type_msg">
                                        <div class="input_msg_write">
                                            <span class="message_error"></span>
                                            <input type="text" class="write_msg" name="message" placeholder="Type a message" />
                                            <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div><br>
                            <?php
                                //if (isset($notification_list) && $notification_list !=null) {
                                ?>
                                    <!-- <div class="row col-md-12">
                                        <a href="javascript:void(0)" class="btn btn-primary btn-xs">Cleare chat</a>
                                    </div> -->
                                <?php //}
                            ?>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<?php $this->load->view('include/copyright');?>

<script type="text/javascript" src="http://stevenlevithan.com/assets/misc/date.format.js"></script>
<script type="text/javascript">
    $(document).on('click', '.change_status', function() {
            
        var seller_id = $(this).data('seller-id'); // who sell product
        var buyer_id = $(this).data('buyer-id'); // who want product
        var product_id = $(this).data('product-id');
        var status = $(this).data('status');

        $.ajax({
            type: 'post',
            url: '<?= base_url('chat/accept_reject_chat')?>',
            data: {product_id:product_id,buyer_id:buyer_id,seller_id:seller_id,status:status},
            dataType: 'json',
            success: function (response) {
                if (response.message){
                    $(".success_message").html('<div class="col-md-12 text-center offset4 alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a> <strong> Success </strong>' + response.message + '</div>');
                }
                $('.post_status').text(response.post_status);
            }
        });
    });

    // ============= chat-module start =============//
    $(document).ready(function(){
        seller_chat();

        chatWindow = document.getElementById('chatModal'); 
        var xH = chatWindow.scrollHeight; 
        chatWindow.scrollTo(0, xH);

        $(document).on('click', '.msg_send_btn', function() {
            chat_form();
        });

        $("#form").keypress(function (e) {
            if (e.keyCode == 13) {
                chat_form();
                e.preventDefault();
            }
        });

        /* Click the chat button*/
        
        $(document).on('click', '.chat_seller', function() { //User chat histroty.                       
            $('.write_msg').val('');
            var seller_id = $(this).data('seller-id');
            var buyer_id = $(this).data('buyer-id');
            var product_id = $(this).data('product-id');
            var message_send_by = $(this).data('message-send-by');

            $('.seller_id').val(seller_id);
            $('.buyer_id').val(buyer_id);
            $('.product_id').val(product_id);
            $('.message_send_by').val(message_send_by);

            chat_histroy();
        });

        function chat_histroy(){
            $.ajax({
                type: 'post',
                url: '<?= base_url('chat/user_chat_list')?>',
                data: $('form').serialize(),
                dataType: 'html',
                success: function (response) {
                    $('.msg_history').html(response);
                    myscroll = $('.msg_history');
                    myscroll.scrollTop(myscroll.get(0).scrollHeight);
                }
            });
        }

        function chat_form(){
            var message = $('.write_msg').val();           
            var datetime = dateFormat(new Date(), "d-m-yyyy h:MM:ss TT");

            $('.error').remove();
            if($.trim($('[name="message"]').val()) == ''){
                $('<label id="message-error" class="error" for="message" style="">Please type message</label>').insertAfter('.write_msg');
            } 
            else {
                $.ajax({
                    type: 'post',
                    url: '<?= base_url('chat/add')?>',
                    data: $('form').serialize(),
                    dataType: 'json',
                    success: function (response) {
                        if (response.result == 'success'){
                            $('.msg_history').append('<div class="outgoing_msg"><div class="sent_msg"><p>'+message+'</p><span class="time_date"> '+datetime+' </span></div></div>');
                            $('.write_msg').val('');                          
                            myscroll = $('.msg_history');
                            myscroll.scrollTop(myscroll.get(0).scrollHeight);
                        }
                    }
                });
            }
        }
    });

    setInterval(function(){
       seller_chat(); // this will run after every 5 seconds
    }, 5000);

    function seller_chat(){
        if ($('body').hasClass('modal-open')) {
            $.ajax({
                type: 'post',
                url: '<?= base_url('chat/seller_chat')?>',
                data: $('form').serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.result == 'success'){
                        $('.msg_history').append(response.html);
                        myscroll = $('.msg_history');
                        myscroll.scrollTop(myscroll.get(0).scrollHeight);
                    }
                }
            });
        }
    }
    // ============= chat-module end =============//
</script>
<?php $this->load->view('include/footer');?>