<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
   <head>
      <?php $this->load->view('include/header_js');?>
      <style type="text/css">
        .bg-img img{
            width: 100%;
        }
        .error{
            color: red;
            display: block;
            text-align: left;
        }
    </style>
   </head>
   <body>
    <div class="main aos-all" id="transcroller-body">
      <?php $this->load->view('include/header');?>
        <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
        <!-- slider start -->
        <div class=" demo-2">
            <!-- Codrops top bar -->
            <?php if (isset($slider_details) && $slider_details !=null){ ?>
                <div id="slider" class="sl-slider-wrapper">
                    <div class="sl-slider">
                        <?php
                            

                            foreach ($slider_details as $key => $value) { ?>
                        
                                <div class="sl-slide" data-orientation="horizontal" data-slice1-rotation="-25" data-slice2-rotation="-25" data-slice1-scale="2" data-slice2-scale="2">
                                    <div class="sl-slide-inner">
                                        <div class="bg-img bg-img-1" style="background-image:url();">
                                            <img src="<?=base_url(HOME_SLIDER).$value['slider_image']?>">
                                        </div>
                                        <!-- <h2><?php echo $value['slider_image']; ?></h2> -->
                                        <!-- <blockquote><p><?php echo $value['slider_image']; ?></p><cite><?php echo $value['slider_image']; ?></cite></blockquote> -->
                                    </div>
                                </div>
                                <?php 
                            }
                        ?>
                    </div>
                    <nav id="nav-dots" class="nav-dots">
                        <?php 
                            foreach ($slider_details as $key => $value) { ?>
                                <span class="<?=($key == 0)?'nav-dot-current':''?>"></span>
                                <?php 
                            } 
                        ?>
                    </nav>
                </div>
            <?php }?>
        </div>
        <!-- domain_search start -->
        <div class="domain_search padding_all">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-1 col-md-5">
                        <h3>Find your Perfect Domain Name:</h3>
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Quia aliquam tempora deleniti quibusdam labore quo fuga.</p>
                    </div>
                    <div class="col-md-6">
                        <div class="search_box">
                            <input type="text" name="">
                            <button class="btn btn-primary">Search</button>
                        </div><hr>
                        <h4><span>.com$25</span> | <span>.net$25</span> | <span>.org$25</span> | <span>.in$25</span> | <span>.biz$25</span></h4>
                    </div>
                </div>
            </div>
        </div>
        <!-- domain_search over -->
        <!-- domain_style start -->
        <div class="padding_all text-center domain_style">
            <div class="container">
                    <h1 class="h1_title">WE ARE ANNOUNCING PERFECT PACKAGE FOR YOU</h1>
                    <h5>VIPLHOSTING Providing you with a hesitate free web hosting service we take words look the believable.</h5>
                <div class="row">
                    <?php if (isset($announce_details) && $announce_details !=null){ 
                            // echo "<pre>";print_r($announce_details);exit;

                            foreach ($announce_details as $announce_key => $announce_value) {
                                ?>
                                <div class="col-md-3 col-xs-12 margin_top" data-aos="flip-left">
                                    <div class="domain_box">
                                        <div class="title">
                                            <h3><?php echo $announce_value['name']; ?></h3>
                                        </div>
                                        <div class="domain_box_white">
                                            <h1><?php echo $announce_value['price']; ?>$</h1>
                                            <p>/ Year Regularly</p>
                                        </div>
                                        <ul class="domain_ul">
                                            <?php
                                                if($announce_value['id']!="")
                                                {
                                                    $announce_meta_details = $this->Production_model->get_all_with_where('announce_meta','','',array('announce_id'=>$announce_value['id']));
                                                    foreach ($announce_meta_details as $announce_meta_key => $announce_meta_value) { ?>

                                                        <li><?php echo $announce_meta_value['data']; ?> </li>
                                                        <?php 
                                                    }
                                                    
                                                }
                                            ?>   
                                        </ul>
                                        <button class="btn_order" data-toggle="modal" data-target="#myModal<?php echo $announce_value['id']; ?>">Order Now</button>
                                    </div>
                                </div>
                                <!-- <div id="myModal<?=$announce_value['id']; ?>" class="modal fade" role="dialog">
                                  <div class="modal-dialog">
                                    
                                    <div class="modal-content">
                                      <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Order</h4>
                                      </div>
                                      <div class="modal-body">
                                        <form method="post" name="f1_contact" enctype="multipart/form-data" >
                                            <div class="form-group">
                                                <label class="ord_class_label">Package</label>
                                                <input type="hidden" name="package_id" id="package_id" value="<?php echo $row_annou['id']; ?>">
                                                <input type="text" name="package" id="package" value="<?php echo $row_annou['name']; ?>" class="input_all ord_class_text">
                                                <p class="msg_po" id="err_package"></p>
                                            </div>
                                            <div class="form-group">
                                                <label class="ord_class_label">Name</label>
                                                <input type="text" name="name" id="name<?php echo $row_annou['id']; ?>" class="input_all ord_class_text">
                                                <p class="msg_po" id="err_name<?php echo $row_annou['id']; ?>"></p>
                                            </div>
                                            <div class="form-group">
                                                <label class="ord_class_label">Company Name</label>
                                                <input type="text" name="company_name" id="company_name<?php echo $row_annou['id']; ?>" class="input_all ord_class_text">
                                                <p class="msg_po" id="err_company_name<?php echo $row_annou['id']; ?>"></p>
                                            </div>
                                            <div class="form-group">
                                                <label class="ord_class_label">Email</label>
                                                <input type="text" name="email" id="email<?php echo $row_annou['id']; ?>" class="input_all ord_class_text">
                                                <p class="msg_po" id="err_email<?php echo $row_annou['id']; ?>"></p>
                                            </div>
                                            <div class="form-group">
                                                <label class="ord_class_label">Contact Number </label>
                                                <input type="text" name="contact_number" id="contact_number<?php echo $row_annou['id']; ?>" class="input_all ord_class_text" onKeyPress="return isNumber(event)">
                                                <p class="msg_po" id="err_contact_number<?php echo $row_annou['id']; ?>"></p>
                                            </div>
                                            <div class="form-group">
                                                <label class="ord_class_label">Comments </label>
                                                <textarea name="comments" id="comments<?php echo $row_annou['id']; ?>" class="input_all ord_class_text" onKeyPress="return isNumber(event)">
                                                    </textarea>
                                                <p class="msg_po" id="err_comments<?php echo $row_annou['id']; ?>"></p>
                                            </div>
                                            <input type="submit" name="submit_order" class="btn_order btn-lg btn_margin" value=" Submit " onClick="return valid(<?php echo $row_annou['id']; ?>);">
                                        </form>
                                      </div>
                                      <div class="modal-footer">
                                        <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                      </div>
                                    </div>
                                  </div>
                                </div> -->
                                <?php 
                            }
                        }   
                    ?>      
                </div>
            </div>
        </div>
        <!-- help_line start -->
        <div class="padding_all help_line" style="background-image: url(<?=base_url(IMAGES.'mail.jpg')?>);">
            <div class="container text-center">
                <h1 class="h1_title">Need Help?</h1>
                    <h4>Let us help you make the right decision!</h4>
                <div class="row margin_top">
                    <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                        <div class="help_border">
                            <a href="javascript:;"><i class="fa fa-phone"></i></a>
                            <h3>Call Us</h3>
                            <p>Give us a call & ask all of your questions</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                        <div class="help_border ">
                            <a href="javascript:;"><i class="fa fa-pencil"></i></a>
                            <h3>Email Us</h3>
                            <p>Send us an email with all of your questions</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                        <div class="help_border margin_row">
                            <a href="javascript:;"><i class="fa fa-video-camera"></i></a>
                            <h3>Live Chat</h3>
                            <p>Chat with a member of our support team now</p>
                        </div>
                    </div>
                    <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                        <div class="help_border margin_row">
                            <a href="javascript:;"><i class="fa fa-certificate"></i></a>
                            <h3>Real Reviews</h3>
                            <p>Read what real customers have to say</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- help_line over -->

        <!-- services start -->
        <div class="services padding_all">
            <div class="container text-center">
                <div class="row">
                    <div class="col-md-12 col-xs-12 text-center">
                        <!-- <h2>Over 4 million customers trust our services and servers</h2> -->
                        <span class="lead" style="color: white; font-weight: bold; font-size: 35px">Over 4 million customers trust our services and servers</span>
                    </div>
                    <!-- <div class="col-md-3 col-xs-12">
                        <a href="register.php" class="btn_upnow">Signup Now</a>
                    </div> -->
                </div>
            </div>
        </div>
        <!-- services over -->
        <!-- contact up start -->
        <div class="news">
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8 col-xs-12">
                        <div class="help_box text-center" data-aos="flip-down">
                        <form id="subscribe" method="post" enctype="multipart/form-data">
                            <p>Need Help? Call Us 24/7:</p>
                            <h2><span class="fa fa-volume-control-phone"></span> +918347850002 </h2><hr>
                            <span>Sign up to Newsletter for get special offers: </span>
                            <span><input type="text" name="email" id="email" class="input_phone" placeholder="Please enter your email"></span>
                            <span><input type="button" class="btn_upnow btn_subscribe check" value="Subscribe" ></span>
                        </form> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- contact up over -->
      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?> 
    <script type="text/javascript" src="<?= base_url()?>assets/js/jquery.ba-cond.min.js"></script>
    <script type="text/javascript" src="<?= base_url()?>assets/js/jquery.slitslider.js"></script>
    <script type="text/javascript"> 
        $(function() {
        
            var Page = (function() {

                var $nav = $( '#nav-dots > span' ),
                    slitslider = $( '#slider' ).slitslider( {
                        autoplay: true,
                        onBeforeChange : function( slide, pos ) {

                            $nav.removeClass( 'nav-dot-current' );
                            $nav.eq( pos ).addClass( 'nav-dot-current' );

                        }
                    } ),

                    init = function() {

                        initEvents();
                        
                    },
                    initEvents = function() {

                        $nav.each( function( i ) {
                        
                            $( this ).on( 'click', function( event ) {
                                
                                var $dot = $( this );
                                
                                if( !slitslider.isActive() ) {

                                    $nav.removeClass( 'nav-dot-current' );
                                    $dot.addClass( 'nav-dot-current' );
                                
                                }
                                
                                slitslider.jump( i + 1 );
                                return false;
                            
                            } );
                            
                        } );

                    };

                    return { init : init };

            })();

            Page.init();

            
        });
    </script> 
    <script type="text/javascript">
        $('#subscribe').validate({
            rules: {
                email: {
                    required: true,
                    email:true,
                },
            },
            messages: {
                email: {
                  required: "Please enter Email ",
                  email:"Please enter valid Email",                     
                },
            }
        });
        var form = $( "#subscribe" );
        form.validate();
        $(document).on('click','.check',function(){
          // alert( "Valid: " + form.valid() );
          if(form.valid()){
            add_subscriber();
          }
        });
        function add_subscriber(){
           var formData = new FormData($('#subscribe')[0]);
            var uurl = BASE_URL+"api/user/add_subscriber";
            $.ajax({
               url: uurl,
               type: 'POST',
               dataType: 'json',
               data: formData,
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){
                if (response.result=="Success") {
                    $.alert(response.message);
                }
               },
               error: function(xhr) {
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
               cache: false,
               contentType: false,
               processData: false
            });
        }
    </script>
   </body>
</html> 