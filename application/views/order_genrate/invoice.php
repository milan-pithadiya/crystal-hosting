<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      
      <style>
        .invoice-box table{width:100%;line-height:inherit;text-align:left}.invoice-box table td{padding:5px;vertical-align:top}.invoice-box table tr td:nth-child(2){text-align:right}.invoice-box table tr td:nth-child(3){text-align:right}.invoice-box table tr td:nth-child(4){text-align:right}.invoice-box table tr.top table td{padding-bottom:20px}.invoice-box table tr.top table td.title{font-size:45px;line-height:100px;color:#333}.invoice-box table tr.information table td{padding-bottom:40px}.invoice-box table tr.heading td{background:#eee;border-bottom:1px solid #ddd;font-weight:700}.invoice-box table tr.details td{padding-bottom:20px}.invoice-box table tr.item td{border-bottom:1px solid #eee}.invoice-box table tr.item.last td{border-bottom:none}.invoice-box table tr.total td:nth-child(2){border-top:2px solid #eee;font-weight:700}@media only screen and (max-width:600px){.invoice-box table tr.top table td{display:block;text-align:center}.invoice-box table tr.information table td{display:block;text-align:center}}.rtl{direction:rtl;font-family:Tahoma,'Helvetica Neue',Helvetica,Helvetica,Arial,sans-serif}.rtl table{text-align:right}.rtl table tr td:nth-child(2){text-align:left}.headings td{font-weight: bold}
          table tr th, table tr td { line-height:1rem; }

    .invoice-box {
        max-width: 735px;
        margin: -10px auto 0 auto;
        padding: 30px;
        border: 1px solid #eee;
        box-shadow: 0 0 10px rgba(0, 0, 0, .15);
        font-size: 16px;
        line-height: 24px;
        font-family: 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
        color: #555;
    }
    
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    
    .invoice-box table td {
        padding: 5px;
        vertical-align: top;
    }
    
    .invoice-box table tr td:nth-child(2) {
        text-align: right;
    }
    
    .invoice-box table tr.top table td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.top table td.title {
        font-size: 45px;
        line-height: 45px;
        color: #333;
    }
    
    .invoice-box table tr.information table td {
        padding-bottom: 40px;
    }
    
    .invoice-box table tr.heading td {
        font-weight: bold;
    }
    
    .invoice-box table tr.details td {
        padding-bottom: 20px;
    }
    
    .invoice-box table tr.item td{
        border-bottom: 1px solid #eee;
    }
    
    .invoice-box table tr.item.last td {
        border-bottom: none;
    }
    
    .invoice-box table tr.total td:nth-child(2) {
        border-top: 2px solid #eee;
        font-weight: bold;
    }
    
    @media only screen and (max-width: 600px) {
        .invoice-box table tr.top table td {
            width: 100%;
            display: block;
            text-align: center;
        }
        
        .invoice-box table tr.information table td {
            width: 100%;
            display: block;
            text-align: center;
        }
    }
    
    /** RTL **/
    .rtl {
        direction: rtl;
        font-family: Tahoma, 'Helvetica Neue', 'Helvetica', Helvetica, Arial, sans-serif;
    }
    
    .rtl table {
        text-align: right;
    }
    
    .rtl table tr td:nth-child(2) {
        text-align: left;
    }
    .hr{border-bottom: 1px solid #000}
    .table-bordered>tbody>tr>td, .table-bordered>tbody>tr>th, .table-bordered>tfoot>tr>td, .table-bordered>tfoot>tr>th, .table-bordered>thead>tr>td, .table-bordered>thead>tr>th {
        border: 1px solid #ddd;
    }
    .invoice-box table {
        width: 100%;
        line-height: inherit;
        text-align: left;
    }
    * {
        margin: 0px;
        padding: 0px;
    }
    table {
        border-spacing: 0;
        border-collapse: collapse;
    }
    .table-striped>tbody>tr:nth-of-type(odd) {
        background-color: #f9f9f9;
    }
    .table-striped thead{
        border-bottom: 3px solid #ddd
    }
    .invoice-btn {
        margin-top: 30px;
    }
    .print-btn, .download-btn {
        border: 1px solid #c3c3c3;
        padding: 2% 5%;
        border-radius: 5px;
        transition: 1s;
    }
    .print-btn:hover, .download-btn:hover {
        border: 1px solid #ff812d;
        padding: 2% 5%;
        border-radius: 5px;
        background: #ff812d;
        color: #FFFFFF;
        cursor: pointer;
    }
      </style>
    </head>
    <body>
          <div class="invoice-box main-container">
              <table cellpadding="0" cellspacing="0">
                  <tr class="top" style="border-bottom: 1px solid #000">
                      <td colspan="2">
                          <table >
                              <tr>
                                  <td class="title" >
                                      <img src="<?=base_url()?>assets/image/logo.png" style="width:100px;height: 100px">
                                  </td>
                                  
                                  <td>
                                      Invoice #: <?=$invoice_no?><br>
                                      Invoice Data: <?=format_date_Mdy($invoice_date)?><br>
                                      Due Date: <?=format_date_Mdy($due_date)?>
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
                  
                  <tr class="information">
                      <td colspan="2">
                          <table cellpadding="0" cellspacing="0">
                              <tr>
                                  <td>
                                      <b>Invoice To</b><br>
                                      <?=$first_name.' '.$last_name?>.<br>
                                      <?=nl2br($address)?><br>
                                  </td>
                                  
                                  <td>
                                      <b>Pay To</b><br>
                                      Crystal Hostings<br>
                                      info@crystalinfoway.com
                                  </td>
                              </tr>
                          </table>
                      </td>
                  </tr>
              </table>
              <table  class="table-bordered" cellpadding="0" cellspacing="0">
                  <tr class="heading">
                      <td colspan="4">
                          Invoice Items
                      </td>
                  </tr>
                  <tr class="details headings" style="border-bottom: 1px solid #000">
                      <td >
                          Description
                      </td>
                      <td>Price</td>
                      <td>Quantity</td>
                      <td>Amount</td>
                  </tr>
                   <?php
                        if ($invoice_details !=null) {
                            $invoice_count = 1;
                            $sub_total = 0;
                            foreach ($invoice_details as $key => $item_row) {
                                $id = $item_row['id'];
                                $sub_total = $sub_total + $item_row['total'];
                                ?>
                                <tr class="details">
                                    <td>
                                       <?=$item_row['description'];?>
                                    </td>
                                    <td>
                                      <?=$currency_code.' '.number_format($item_row['price'], 2, '.', '').' '.$currency_name?>
                                    </td>
                                    <td><?=$item_row['quantity'];?></td>
                                    <td>
                                        <?=$currency_code.' '.number_format($item_row['total'], 2, '.', '').' '.$currency_name?>
                                    </td>
                                </tr>
                            <?php
                            }
                        }
                      ?>
                                
                                <tr class="details">
                                    <td colspan="3">
                                    </td>
                                    
                                    <td>
                                        <b>Sub Total</b> &nbsp;&nbsp;&nbsp; <?=$currency_code.' '.number_format($sub_total, 2, '.', '').' '.$currency_name?>
                                    </td>
                                </tr>
                                
                                <tr class="details">
                                    <td colspan="3">
                                    </td>
                                    
                                    <td>
                                        <?php 
                                            if(isset($gst) && $gst !=0 ){
                                               $gst_sum=$total_amount/100*$gst;
                                               $gst_total = $currency_code.' '.number_format($gst_sum, 2, '.', '').' '.$currency_name;
                                            }else{
                                                $gst_total = $currency_code.' '.number_format(0.00, 2, '.', '').' '.$currency_name;
                                            }
                                        ?>
                                        <b><?=$gst?>% GST</b> &nbsp;&nbsp;&nbsp; 
                                        
                                        <?=$gst_total?>
                                    </td>
                                </tr>
                                <tr class="details">
                                    <td colspan="3">
                                    </td>
                                    
                                    <td>
                                        <b>Total</b> &nbsp;&nbsp;&nbsp; <?=$currency_code.' '.$total_amount.' '.$currency_name?>
                                    </td>
                                </tr>
              </table>
                <!-- <table class="table table-striped" style="margin-top: 30px">
                  <thead>
                    <tr>
                      <th scope="col">Transaction Date</th>
                      <th scope="col">Gateway</th>
                      <th scope="col">Transaction ID</th>
                      <th scope="col">Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                    <tr>
                      <td>11/10/2018</td>
                      <td style="text-align: left">Done</td>
                      <td>ABG2496GRB</td>
                      <td>Rs.26450.00</td>
                    </tr>
                  </tbody>
                </table> -->
              <!-- <div  class="invoice-btn">
                  <div class="print-btn ">Print</div>
                  <div class="download-btn et-download-button">Download</div>
              </div> -->
          </div>
      
   </body>
</html> 