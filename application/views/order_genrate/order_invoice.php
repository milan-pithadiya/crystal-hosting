<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
        <title>Crystal Hositing</title>
        <link rel="icon" type="image/png" sizes="56x56" href="<?= base_url()?>assets/img/favicon.png">
        <style>
            .logo img,header{width:100%}.container,header{display:inline-block}body{text-align:center;font-family:sans-serif;margin:0;padding:0}*{box-sizing:border-box}a,a:hover{color:#fff;text-decoration:none}.logo{float:left;max-width:133px}.view-link{float:right;margin:21px 0}.view-link a{padding:8px 16px;font-size:18px;background-color:#e10a0a}.view-link a:hover{background-color:#272d32}.email-icon,.verify-btn{background-color:#e10a0a}header{padding:20px 0}.container{width:82%;margin:auto}.email-icon{padding:17px 0}.confirmation-box{box-shadow:0 0 10px 1px #848484;padding:0;margin:25px auto 31px;width:80%}.discription{padding:24px 10px}.verify-btn{padding:10px;color:#fff;display:block;width:43%;margin:auto}.verify-btn:hover{background-color:#272d32}.social a{font-size:24px;color:#fff;border-radius:6px;display:inline-block;width:40px;height:40px;margin:0 6px;text-align:center;}.fb{background-color:#3b5998}.twitter{background-color:#55acee}.email{background-color:#fbad1b}@media all and (max-width:736px){.verify-btn{font-size:13px!important}}@media all and (max-width:530px){.logo{display:inline-block;float:none}.view-link{display:block;float:none;margin:36px 0 9px}}

	            /* table */
				table { font-size: 75%; table-layout: fixed; width: 100%; }
				table { border-collapse: separate; border-spacing: 2px; }
				.meta th{ border-width: 1px;
				    padding: 5px 5px;
				    position: relative;
				    text-align: left;
				    font-size: 13px; }
				.inventory th{
					padding: 12px 3px;
				    font-size: 12px;
				}
				td,th{ border-width: 1px;
				    padding: 12px 8px;
				    position: relative;
				    text-align: left;
				    font-size: 15px; 
					color:black;}
				th, td { border-radius: 0.25em; border-style: solid; }
				th { background: #EEE; border-color: #BBB; }
				td { border-color: #DDD; }
        </style>
    </head>
	<body>
		<header>
			<div class="container">
                <a href="<?= base_url()?>" class="logo">
                    <?php
	                    $profile_photo = $this->Production_model->get_all_with_where('user','','',array());
	                    if ($profile_photo != null) {
	                    ?>
	                        <img alt="Online-cart" src="<?= $profile_photo !=null ? base_url('assets/uploads/profile_photo/').$profile_photo[0]['profile_photo'] : base_url('assets/uploads/profile_photo/default-image.png')?>">
	                    <?php }
	                ?>
                </a>
                <!-- <div class="view-link" style="background-color:#999999;color:#FFFFFF;">
                    <a href="<?= base_url()?>" style="background-color:#999999;color:#FFFFFF;">View Website</a>
                </div> -->
            </div>
		</header>
		<div style="clear:both">
		</div>
		<div class="container">
			<div class="confirmation-box col-md-8 col-md-offset-2">
				<div class="email-icon" style="text-align:center;background-color:#999999;">
                    <img src="<?= base_url()?>assets/img/mail.png" width="55px">
                </div>
				<div class="table-form">
					<div class="discription">
						<h2 style="margin:0">Crystal Hositing post</h2>
						<h4 style="color: dimgrey;">
							Lorem ipsum dolor sit amet, vel graece numquam postulant ut. No dicunt aliquid laboramus pri, eu ubique omnium sea, has iusto accusamus et. Veritus euripidis eam.
						</h4>
					</div>
					<table>
						<tr>
							<td><strong>Name :</strong></td>
							<td><?= $name?></td>
						</tr>
						<tr>
							<td><strong>Order No :</strong></td>
							<td><?= $order_id?></td>
						</tr>
						<?php
							$paymeny_data = json_decode($payment_details);
							if (isset($paymeny_data->referenceId)) {
							?>
								<tr>
									<td><strong>Cashfree Reference Id :</strong></td>
									<td><?= $paymeny_data->referenceId ?></td>
								</tr>

								<tr>
									<td><strong>Package Price :</strong></td>
									<td>
										<?php
											if (isset($currency) && $currency == 'INR') {
												echo 'Rs '. $paymeny_data->orderAmount;
											}
											elseif (isset($currency) && $currency == 'USD') {
		                                        echo '$ '.$paymeny_data->orderAmount;
		                                    }
		                                    elseif (isset($currency) && $currency == 'EUR') {
		                                        echo '€ '.$paymeny_data->orderAmount;
		                                    }
		                                    elseif (isset($currency) && $currency == 'GBP') {
		                                        echo '£ '.$paymeny_data->orderAmount;
		                                    }
										?>
									</td>
								</tr>
								<tr>
									<td><strong>signature :</strong></td>
									<td><?= $paymeny_data->signature ?></td>
								</tr>
							<?php }
						?>
						<tr>
							<td>
								<strong>
									Category :
								</strong>
							</td>
							<td>
								<?php
									$category = $this->Production_model->get_all_with_where('category','','',array('category_id'=>$category_id));
								?>
								<?= isset($category) && $category !=null ? $category[0]['category_name'] : ''?>
							</td>
						</tr>
						
						<?php
							if (isset($sub_category_id) && $sub_category_id !=null && $sub_category_id !=0) {
								$sub_category = $this->Production_model->get_all_with_where('sub_category','','',array('sub_category_id'=>$sub_category_id));
							?>
								<tr>
									<td><strong>Sub Category :</strong></td>
									<td><?= $sub_category[0]['sub_category_name']?></td>
								</tr>
							<?php }
						?>
						<tr>
							<td><strong>Add Type :</strong></td>
							<td><?= $add_type?></td>
						</tr>
						<tr>
							<td><strong>I Want :</strong></td>
							<td><?= $i_want?></td>
						</tr>
						<tr>
							<td><strong>Price :</strong></td>
							<td>
								<?php
									if (isset($currency) && $currency == 'INR') {
										echo 'Rs '. $price;
									}
									elseif (isset($currency) && $currency == 'USD') {
                                        echo '$ '.$price;
                                    }
                                    elseif (isset($currency) && $currency == 'EUR') {
                                        echo '€ '.$price;
                                    }
                                    elseif (isset($currency) && $currency == 'GBP') {
                                        echo '£ '.$price;
                                    }
								?>
							</td>
						</tr>
						<tr>
							<td><strong>Package Name :</strong></td>
							<td><?= $package_name?></td>
						</tr>
						<tr>
							<td><strong>Day :</strong></td>
							<td><?= $day;?></td>
						</tr>
						<tr>
							<td><strong>Package Expire :</strong></td>
							<td>
								<?php
									$created_date = date('Y-m-d',strtotime($post_create_date));
                                    echo $expire_post = date('Y-m-d',strtotime($created_date . + $day .'days'));
								?>
							</td>
						</tr>
						<tr>
							<td><strong>Description  :</strong>
							</td>
							<td><?= $post_description ?></td>
						</tr>
						<tr>
							<td><strong>Package Price :</strong></td>
							<td>
								<?php
									if (isset($currency) && $currency == 'INR') {
										echo 'Rs '. $package_price == 0 ? 'Free' : $package_price;
									}
									elseif (isset($currency) && $currency == 'USD') {
                                        echo '$ '.$package_price == 0 ? 'Free' : $package_price;
                                    }
                                    elseif (isset($currency) && $currency == 'EUR') {
                                        echo '€ '.$package_price == 0 ? 'Free' : $package_price;
                                    }
                                    elseif (isset($currency) && $currency == 'GBP') {
                                        echo '£ '.$package_price == 0 ? 'Free' : $package_price;
                                    }
								?>
							</td>
						</tr>
						<?php
							if (isset($get_post_image) && $get_post_image !=null) {
								?>
									<tr>
										<td>
											<strong>
												Post Image :
											</strong>
										</td>
										<td>
											<?php
												if (isset($get_post_image) && $get_post_image !=null) {
													foreach ($get_post_image as $key => $value) {
													?>
														<img src="<?= base_url(POST_NEED_IMG.$value['similar_image'])?>" height="50" width="50">
													<?php }
												}
											?>
										</td>
									</tr>
							<?php } 
						?>
					</table>
				</div>	
			</div>
		</div>
		<div class="container">
            <footer style="text-align:center;">
                <div class="col-md-8 col-md-offset-2">
                    <h3>
                        Stay in Touch
                    </h3>
                    <div class="social">
                        <a href="<?= base_url()?>" class="fb">
                            <img src="<?= base_url()?>assets/img/fb.png" width="10px">
                        </a>
                        <a href="<?= base_url()?>" class="twitter">
                            <img src="<?= base_url()?>assets/img/tw.png" width="20px">
                        </a>
                        <a href="<?= base_url()?>" class="email">
                            <img src="<?= base_url()?>assets/img/mail.png" width="20px">
                        </a>
                    </div>
                    <h4 style="margin: 32px 0">Copyright &copy; <?= date('Y')?>.All rights are reserved.</h4>
                </div>
            </footer>
        </div>
	</body>
</html>