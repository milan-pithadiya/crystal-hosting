<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
      <link rel="stylesheet" href="<?=base_url('assets/css/jquery.dataTables.min.css')?>">
    </head>
    <body>
        <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          <!-- about start -->
            <div class="allpage_banner_email allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'email.jpg')?>);">
              <h1 class="title_h1">My Invoices</h1>
              <p><a href="<?=base_url()?>">Home </a> / My Invoices</p>
            </div>
            <div class="about_all padding_all testimonial_all my_domain">
              <div class="container">
                <div class="row">
                  <div class="col-md-12 ">
                    <div class="table-responsive">
                      <table id="myTable" class="table-responsive table-bordered table-hover">  
                        <!-- <div id="overlay"><h2><i class="fa fa-refresh fa-spin"></i></h2></div> -->
                        <thead>  
                          <tr>
                            <th>No.</th>  
                            <th>Invoices</th>  
                            <th>Invoices Date</th>  
                            <th>Due Date</th>  
                            <th>Total</th>
                            <th>Status</th>  
                          </tr>  
                        </thead>  
                        <tbody>
                          
                          <?php 
                            if(isset($invoice_data) && $invoice_data !=null){ 
                              $i=1;
                              $payment_status_class = 0;
                              $payment_status_label = '';
                              
                              // echo "<pre>";print_r($invoice_data);exit;
                              foreach ($invoice_data as $key => $value) { 
                                  switch ($value['payment_status']) {
                                    case '0':
                                      $payment_status_class = 'dactive-btn';
                                      $payment_status_label = 'Unpaid';
                                      break;
                                    case '1':
                                      $payment_status_class = 'paid-btn';
                                      $payment_status_label = 'Paid';
                                      break;
                                    case '2':
                                      $payment_status_class = 'dactive-btn';
                                      $payment_status_label = 'Cancelled';
                                      break;
                                    default:
                                      break;
                                  }
                                ?>
                                <tr>  
                                  <td><?=$value['invoice_no']?></td>
                                  <td><?=$value['invoice_date']?></td>
                                  <td><?=$value['due_date']?></td>
                                  <td>Rs.<?=$value['total_amount']?> INR</td>
                                  <td class="payment_status">
                                    <a class="active-btn" href="<?=site_url(); ?>generated_invoices/<?php echo $value['file_name']; ?>" target="_blank">View</a>
                                    <a class="<?=$payment_status_class?>" href=""><?=$payment_status_label?></a>
                                    <a class="pay-btn" href="#">Pay</a>
                                  </td>
                                </tr>
                              <?php
                              }
                            }
                          ?>
                        </tbody>  
                      </table>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            
      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?> 
      <script type="text/javascript" src="<?=base_url('assets/js/jquery.dataTables.min.js')?>"></script>
      <script>
        $(document).ready(function(){
          // $("#overlay").show();
          // setTimeout(function() {
          //   $('#myTable').dataTable({
          //     fnInitComplete : function() {
          //       $("#overlay").hide();
          //    },
          //     "ordering": false
          //   });
          // }, 2000);
          
          $('#myTable').DataTable({
              // Processing indicator
              "processing": true,
              // DataTables server-side processing mode
              "serverSide": true,
              // Initial no order.
              "order": [],
              // Load data from an Ajax source
              "ajax": {
                  "url": "<?php echo base_url('my_invoices/getLists/'); ?>",
                  "type": "POST"
              },
              //Set column definition initialisation properties
              "columnDefs": [{ 
                  "targets": [0],
                  "orderable": false
              }]
          });  
        });
        
            // $('#myTable').dataTable({
            //   "processing": true,        
            //   "ordering": false,
            // });

        
      </script>
   </body>
</html> 