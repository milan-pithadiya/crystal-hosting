<?php
$request_info = get_request($request_id, $request_type);
$history_info = get_request_history($request_id, $request_type);
?>
<div class="row">
    <div class="col-md-12">
        <h3>Request Information</h3>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-striped">
                <?php
                if ($request_type == 'assets') {
                    ?>
                    <thead>
                        <tr>
                            <th>Request Date & Time</th>
                            <th>Request Id</th>
                            <th>Asset Name</th>
                            <th>Location Name</th>
                            <th>Department Name</th>                            
                            <th>Attachment</th>
                            <th>Status</th>
                            <th>Other Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($request_info) && !empty($request_info)) {
                            ?>
                            <tr>
                                <td><?php echo date('d-m-Y h:i A', strtotime($request_info['create_date'])); ?></td>
                                <td><?php echo $request_id; ?></td>
                                <td><?php echo $request_info['assets_name']; ?></td>
                                <td><?php echo $request_info['location_name']; ?></td>
                                <td><?php echo $request_info['department_name']; ?></td>                                
                                <td>
                                    <?php
                                    if ($request_info['attachment'] != '') {
                                        $path = base_url() . REQUSEST_ASSETS_DOCUMENT . $request_info['attachment'];
                                        echo '<a href="' . $path . '" download>Download & view</a>';
                                    } else {
                                        echo 'Not available';
                                    }
                                    ?>
                                </td>
                                <td><?php echo get_current_status($request_id, $request_type); ?></td>
                                <td><a href="javascript:void(0);" class="view-problem">View</a></td>
                            </tr>
                            <tr style="display:none;">
                                <td colspan="8">
                                    <p><b>Request Reason</b></p>
                                    <div class="problem">                                        
                                        <?php echo nl2br($request_info['reason_for_request']); ?>
                                    </div>
                                    <hr>
                                    <p><b>Additional Details</b></p>
                                    <div class="problem">                                        
                                        <?php echo closetags($request_info['details']); ?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                    <?php
                } else if ($request_type == 'software') {
                    ?>
                    <thead>
                        <tr>
                            <th>Request Date & Time</th>
                            <th>Request Id</th>
                            <th>Software Name</th>
                            <th>Location Name</th>
                            <th>Department Name</th>
                            <th>Attachment</th>
                            <th>Status</th>
                            <th>Other Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($request_info) && !empty($request_info)) {
                            ?>
                            <tr>
                                <td><?php echo date('d-m-Y h:i A', strtotime($request_info['create_date'])); ?></td>
                                <td><?php echo $request_id; ?></td>
                                <td><?php echo $request_info['software_name']; ?></td>
                                <td><?php echo $request_info['location_name']; ?></td>
                                <td><?php echo $request_info['department_name']; ?></td>                                
                                <td>
                                    <?php
                                    if ($request_info['document'] != '') {
                                        $path = base_url() . SOFTWARE_DOCUMENT . $request_info['document'];
                                        echo '<a href="' . $path . '" download>Download & view</a>';
                                    } else {
                                        echo 'Not available';
                                    }
                                    ?>
                                </td>
                                <td><?php echo get_current_status($request_id, $request_type); ?></td>
                                <td><a href="javascript:void(0);" class="view-problem">View</a></td>
                            </tr>
                            <tr style="display:none;">
                                <td colspan="8">
                                    <p><b>Request Reason</b></p>
                                    <div class="problem">                                        
                                        <?php echo nl2br($request_info['reason_for_request']); ?>
                                    </div>
                                    <hr>
                                    <p><b>Additional Details</b></p>
                                    <div class="problem">                                        
                                        <?php echo closetags($request_info['details']); ?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                    <?php
                } else if ($request_type == 'pendrive') {
                    ?>
                    <thead>
                        <tr>
                            <th>Request Date & Time</th>
                            <th>Request Id</th>
                            <th>Date</th>
                            <th>Location Name</th>
                            <th>Department Name</th>                            
                            <th>Attachment</th>
                            <th>Status</th>
                            <th>Other Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($request_info) && !empty($request_info)) {
                            ?>
                            <tr>
                                <td><?php echo date('d-m-Y h:i A', strtotime($request_info['create_date'])); ?></td>
                                <td><?php echo $request_id; ?></td>
                                <td>
                                    <?php echo 'Start Date: ' . date('d-m-Y', strtotime($request_info['start_date'])); ?>
                                    <?php echo '<br/>End Date: ' . date('d-m-Y', strtotime($request_info['end_date'])); ?>
                                </td>                                
                                <td><?php echo $request_info['location_name']; ?></td>
                                <td><?php echo $request_info['department_name']; ?></td>                                
                                <td>
                                    <?php
                                    if ($request_info['document'] != '') {
                                        $path = base_url() . SOFTWARE_DOCUMENT . $request_info['document'];
                                        echo '<a href="' . $path . '" download>Download & view</a>';
                                    } else {
                                        echo 'Not available';
                                    }
                                    ?>
                                </td>
                                <td><?php echo get_current_status($request_id, $request_type); ?></td>
                                <td><a href="javascript:void(0);" class="view-problem">View</a></td>
                            </tr>
                            <tr style="display:none;">
                                <td colspan="8">
                                    <p><b>Request Reason</b></p>
                                    <div class="problem">                                        
                                        <?php echo nl2br($request_info['reason_for_request']); ?>
                                    </div>
                                    <hr>
                                    <p><b>Additional Details</b></p>
                                    <div class="problem">                                        
                                        <?php echo closetags($request_info['details']); ?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                    <?php
                } else if ($request_type == 'desk-move') {
                    ?>
                    <thead>
                        <tr>
                            <th>Request Date & Time</th>
                            <th>Request Id</th>                            
                            <th>Location Name</th>
                            <th>Current Desk Location</th>
                            <th>New Desk Location</th>                            
                            <th>Attachment</th>
                            <th>Status</th>
                            <th>Other Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($request_info) && !empty($request_info)) {
                            ?>
                            <tr>
                                <td><?php echo date('d-m-Y h:i A', strtotime($request_info['create_date'])); ?></td>
                                <td><?php echo $request_id; ?></td>                                
                                <td><?php echo $request_info['location_name']; ?></td>
                                <td><?php echo $request_info['current_desk_location']; ?></td>
                                <td><?php echo $request_info['new_desk_location']; ?></td>
                                <td><?php
                                    if ($request_info['document'] != '') {
                                        $path = base_url() . DESK_MOVE_DOCUMENT . $request_info['document'];
                                        echo '<a href="' . $path . '" download>Download & view</a>';
                                    } else {
                                        echo 'Not available';
                                    }
                                    ?>
                                </td>
                                <td><?php echo get_current_status($request_id, $request_type); ?></td>
                                <td><a href="javascript:void(0);" class="view-problem">View</a></td>
                            </tr>
                            <tr style="display:none;">
                                <td colspan="8">
                                    <p><b>Request Reason</b></p>
                                    <div class="problem">                                        
                                        <?php echo nl2br($request_info['reason_for_move']); ?>
                                    </div>
                                    <hr>
                                    <p><b>Additional Details</b></p>
                                    <div class="problem">                                        
                                        <?php echo closetags($request_info['details']); ?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                    <?php
                } else if ($request_type == 'auditorium') {
                    ?>
                    <thead>
                        <tr>
                            <th>Request Date & Time</th>
                            <th>Request Id</th>                            
                            <th>Location Name</th>
                            <th>Start Date</th>
                            <th>End Date</th>                            
                            <th>Attachment</th>
                            <th>Status</th>
                            <th>Other Details</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        if (isset($request_info) && !empty($request_info)) {
                            ?>
                            <tr>
                                <td><?php echo date('d-m-Y h:i A', strtotime($request_info['create_date'])); ?></td>
                                <td><?php echo $request_id; ?></td>                                
                                <td><?php echo $request_info['location_name']; ?></td>
                                <td><?php echo date('d-m-Y h:i A', strtotime($request_info['start_date'])); ?></td>
                                <td><?php echo date('d-m-Y h:i A', strtotime($request_info['end_date'])); ?></td>                                
                                <td>
                                    <?php
                                    if ($request_info['document'] != '') {
                                        $path = base_url() . 'uploads/auditorium-document/' . $request_info['document'];
                                        echo '<a href="' . $path . '" download>Download & view</a>';
                                    } else {
                                        echo 'Not available';
                                    }
                                    ?>
                                </td>
                                <td><?php echo get_current_status($request_id, $request_type); ?></td>
                                <td><a href="javascript:void(0);" class="view-problem">View</a></td>
                            </tr>
                            <tr style="display:none;">
                                <td colspan="8">                                    
                                    <p><b>Additional Details</b></p>
                                    <div class="problem">                                        
                                        <?php echo closetags($request_info['details']); ?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                    <?php
                }
                ?>
            </table>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <h3>Request History</h3>
    </div>
    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th>Reply Date & Time</th>
                        <th>Allocated To</th>
                        <th>Comment By</th>
                        <th>Status</th>
                        <th>View Reply</th>
                        <th>Document</th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                    if (!empty($history_info)) {
                        foreach ($history_info as $key => $value) {
                            ?>
                            <tr>
                                <td>
                                    <?php echo isset($value['reply_date']) ? date('d-m-Y', strtotime($value['reply_date'])) . ' ' : ''; ?>
                                    <?php echo isset($value['reply_time']) ? date('H:i A', strtotime($value['reply_time'])) : ''; ?>
                                </td>
                                <td><?php echo isset($value['full_name']) ? ucwords($value['full_name']) : ''; ?></td>
                                <td><?php echo isset($value['allocated_by_full_name']) ? ucwords($value['allocated_by_full_name']) : ''; ?></td>
                                <td><?php echo isset($value['status_name']) ? $value['status_name'] : ''; ?></td>                                
                                <td><a href="javascript:void(0);" class="view-problem">View</a></td>
                                <td>
                                    <?php
                                    if ($value['document'] == '') {
                                        echo 'Not available';
                                    } else {
                                        $path = base_url() . 'uploads/request-document/' . $value['document'];
                                        echo '<a href="' . $path . '" target="_blank" download>Download & View</a>';
                                    }
                                    ?>
                                </td>
                            </tr>
                            <tr style="display:none;">
                                <td colspan="6">
                                    <p><b>Reply</b></p>
                                    <div class="problem">                                        
                                        <?php echo isset($value['personal_remark']) ? closetags($value['personal_remark']) : ''; ?>
                                    </div>
                                </td>
                            </tr>
                            <?php
                        }
                    } else {
                        echo '<tr><td colspan="6" align="center">Records not available</td></tr>';
                    }
                    ?>                    
                </tbody>
            </table>
        </div>
    </div>
</div>