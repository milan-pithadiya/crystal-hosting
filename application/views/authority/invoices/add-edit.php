<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>

<?php $this->view('authority/common/sidebar'); ?>
<!-- Invoices Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Invoices Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php echo site_url() . "authority/invoices/view"; ?>">Invoices</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $invoice_details !=null ? 'Edit' : 'Add'?> Invoices</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php $this->load->view('authority/common/messages');?>
                        <?php $action = $invoice_details !=null ? base_url('authority/invoices/update_invoice') : base_url('authority/invoices/insert_invoice'); ?>

                        <form id="form" method="post" action="<?= $action?>" enctype="multipart/form-data">
                            <div class="clone-section-main">   
                                <div class="clone-section-sub">   
                                    <div class="form-group">
                                        <input type="hidden" name="id" id="invoice_id" value="<?= ($invoice_details !=null) ? $invoice_details[0]['id'] : ""; ?>">
                                        <!-- <div class="col-md-3">
                                            <input type="hidden" name="id" id="invoice_id" value="<?= ($invoice_details !=null) ? $invoice_details[0]['id'] : ""; ?>">
                                            <label for="title">Invoices Title :<span class="required">*</span></label> 
                                            <?php
                                                $input_fields = array(
                                                    'name' => 'title',
                                                    'placeholder' => 'Invoice Title',
                                                    'class' => 'form-control title txtonly',
                                                    'id' => 'title',
                                                    'value' => (isset($invoice_details) && $invoice_details !=null ? $invoice_details[0]['title'] : ""),
                                                );
                                                echo form_input($input_fields);
                                                echo form_error("title", "<div class='error'>", "</div>");
                                            ?>
                                            <span class="error_title" style="color: #fc3a3a;"></span>
                                            <div class="clearfix" ></div>
                                            <hr>
                                        </div> -->
                                        <div class="form-group">
                                            <div class="col-md-3">
                                                <label for="invoice_date">Invoices Date :<span class="required">*</span></label> 
                                                <?php
                                                    $input_fields = array(
                                                        'name' => 'invoice_date',
                                                        'placeholder' => 'Invoice Date',
                                                        'class' => 'form-control date',
                                                        'id' => 'invoice_date',
                                                        'value' => (isset($invoice_details) && $invoice_details !=null ? $invoice_details[0]['invoice_date'] : ""),
                                                    );
                                                    echo form_input($input_fields);
                                                    echo form_error("invoice_date", "<div class='error'>", "</div>");
                                                ?>
                                                <span class="error_title" style="color: #fc3a3a;"></span>
                                                <div class="clearfix" ></div>
                                                <hr>  
                                            </div>
                                            <div class="col-md-3">
                                                <label for="due_date">Due Date :<span class="required">*</span></label> 
                                                <?php
                                                    $input_fields = array(
                                                        'name' => 'due_date',
                                                        'placeholder' => 'Due Date',
                                                        'class' => 'form-control date',
                                                        'id' => 'due_date',
                                                        'value' => (isset($invoice_details) && $invoice_details !=null ? $invoice_details[0]['due_date'] : ""),
                                                    );
                                                    echo form_input($input_fields);
                                                    echo form_error("due_date", "<div class='error'>", "</div>");
                                                ?>
                                                <span class="error_due_date" style="color: #fc3a3a;"></span>
                                                <div class="clearfix" ></div>
                                                <hr>  
                                            </div>
                                            <div class="col-md-4">
                                                <label for="user_id">Select User :<span class="required">*</span></label>
                                                <?php 
                                                    if ($user_details !=null) {
                                                        $option[null] = 'Select'; 
                                                        $data = array(
                                                            'class' => 'form-control user_id',
                                                            'id' => 'user_id',
                                                        );
                                                        foreach ($user_details['data'] as $key => $value) {
                                                            $option[$value['id']] = $value['first_name']." ".$value['last_name'];
                                                        }
                                                    }
                                                    echo form_dropdown('user_id',$option,isset($invoice_details[0]['user_id']) ? $invoice_details[0]['user_id'] : '',$data);
                                                ?>
                                                <span class="error_user_id" style="color: #fc3a3a;"></span>
                                                <?php echo form_error("user_id", "<div class='error'>", "</div>"); ?>
                                                <hr>
                                            </div>
                                            <div class="col-md-2">
                                                <label for="currency">Currency :<span class="required">*</span></label>
                                                <select class="form-control" id="currency_id" name="currency_id">
                                                    <option value="">Select</option>
                                                    <?php 
                                                        foreach ($currency_data['data'] as $key => $value) { ?>
                                                            <option value="<?=$value['id']?>" <?=(isset($invoice_details[0]['currency_id']) && $value['id']==$invoice_details[0]['currency_id'])?'selected':''?>><?=$value['currency_code']?> | <?=$value['currency_name']?>
                                                            </option>
                                                        <?php }
                                                    ?>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="line">
                                            <div class="col-md-12">
                                                <div class="container2">
                                                  <div class="row clearfix">
                                                    <div class="col-md-12">
                                                      <table class="table table-bordered table-hover" id="tab_logic">
                                                        <thead>
                                                          <tr>
                                                            <th class="text-center"> # </th>
                                                            <th class="text-center" style="width: 40%"> Description </th>
                                                            <th class="text-center"> Quantity </th>
                                                            <th class="text-center"> Price </th>
                                                            <th class="text-center"> Total </th>
                                                          </tr>
                                                        </thead>
                                                        <tbody>
                                                            <?php
                                                                if ($invoice_item_details !=null) {
                                                                    $invoice_count = 1;
                                                                    foreach ($invoice_item_details as $key => $invoice_item_row) {
                                                                        $id = $invoice_item_row['id'];
                                                                        ?>
                                                                          <tr>
                                                                            <td><?=$invoice_count?></td>
                                                                            <td>
                                                                                <input type="hidden" name="edit_item_id[]" value="<?=$id?>">
                                                                                <input type="text" name='description[]' placeholder='Enter description' class="form-control description" value="<?= $invoice_item_row['description']?>" />
                                                                            </td>
                                                                            <td><input type="number" name='quantity[]' placeholder='Enter Qty' class="form-control qty" value="<?=$invoice_item_row['quantity']?>" step="0" min="0"/></td>

                                                                            <td><input type="number" name='price[]' placeholder='Enter Unit Price' class="form-control price" value="<?=$invoice_item_row['price']?>" step="0.00" min="0"/></td>

                                                                            <td><input type="number" name='total[]' placeholder='0.00' class="form-control total" readonly/></td>
                                                                          </tr>
                                                                          <?php 
                                                                          $invoice_count++;
                                                                    }
                                                                }else{
                                                                    ?>
                                                                        <tr>
                                                                            <td>1</td>
                                                                            <td><input type="text" name='description[]' placeholder='Enter description' class="form-control description"/>
                                                                            </td>
                                                                            <td><input type="number" name='quantity[]' placeholder='Enter Qty' class="form-control qty" step="0" min="0"/></td>

                                                                            <td><input type="number" name='price[]' placeholder='Enter Unit Price' class="form-control price" step="0.00" min="0"/></td>

                                                                            <td><input type="number" name='total[]' placeholder='0.00' class="form-control total" readonly/></td>
                                                                        </tr>
                                                                    <?php
                                                                }
                                                            ?>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                  </div>
                                                  <div class="row clearfix">
                                                    <div class="col-md-12">
                                                      <button type="button" id="add_row" class="btn btn-success btn-xs pull-left">Add Row
                                                      </button>
                                                      <button type="button" id='delete_row' class="pull-right btn tn-danger btn-xs">Delete Row</button>
                                                    </div>
                                                  </div>
                                                  <div class="row clearfix" style="margin-top:20px">
                                                    <div class="pull-right col-md-4">
                                                      <table class="table table-bordered table-hover" id="tab_logic_total">
                                                        <tbody>
                                                          <tr>
                                                            <th class="text-center">Sub Total</th>
                                                            <td class="text-center"><input type="number" name='sub_total' placeholder='0.00' class="form-control" id="sub_total" readonly/></td>
                                                          </tr>
                                                          <tr>
                                                            <th class="text-center">GST </th>
                                                            <td class="text-center">
                                                                <div class="input-group mb-2 mb-sm-0">
                                                                <?php 
                                                                    $gst_rates= array("5"=>"5 %","12"=>"12 %","18"=>"18 %","28"=>"28 %");
                                                                    ?>
                                                                    <select class="form-control" id="gst" name="gst">
                                                                        <option value="">Select</option>
                                                                        <?php 
                                                                            foreach ($gst_rates as $key => $value) { ?>
                                                                                <option value="<?=$key?>" <?=(isset($invoice_details[0]['gst']) && $key==$invoice_details[0]['gst'])?'selected':''?>><?=$value?></option>
                                                                            <?php }
                                                                        ?>
                                                                    </select>
                                                                <div class="input-group-addon">%</div>
                                                              </div>
                                                            </td>
                                                          </tr>
                                                          <tr>
                                                            <th class="text-center">Tax Amount</th>
                                                            <td class="text-center"><input type="number" name='gst_amount' id="gst_amount" placeholder='0.00' class="form-control" readonly/></td>
                                                          </tr>
                                                          <tr>
                                                            <th class="text-center">Grand Total</th>
                                                            <td class="text-center"><input type="number" name='total_amount' id="total_amount" placeholder='0.00' class="form-control" readonly/></td>
                                                          </tr>
                                                        </tbody>
                                                      </table>
                                                    </div>
                                                  </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12">
                                    <input class="btn btn-success text-uppercase check" value="Submit" type="submit">
                                    <a href="<?php echo site_url() . 'authority/invoices'; ?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </section>
    </div>
    <?php $this->view('authority/common/copyright'); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
    <script>
        /*FORM VALIDATION*/
        
        var invoice_id = $("#invoice_id").val();
        // $("#form").validate({
        //     rules: {
        //         'invoice_date': {required: true},
        //         'price[]': {required: true},
        //     },
        //     messages: {
        //         'invoice_date': "Please enter invoice date",
        //         'price[]': "Please enter price",
        //     }

        // });
        // $("[name^=inp_text]").each(function () {
        //     $(this).rules("add", {
        //         required: true,
        //         checkValue: true
        //     });
        // });


        $('.check_excel').click(function(){
            if(isemptyfocus('xls_file'))
            {
                return false;
            }
        });

    </script>
    <!-- <script type="text/javascript">
        $(document).ready(function(){
    
        //option_list('addr0');

            var i=1;
            $("#add_row").click(function(){b=i-1;
                $('#addr'+i).html($('#addr'+b).html()).find('td:first-child').html(i+1);
                $('#tab_logic').append('<tr id="addr'+(i+1)+'"></tr>');
                option_list('addr'+i);
                i++; 
            });
            $("#delete_row").click(function(){
                if(i>1){
                $("#addr"+(i-1)).html('');
                i--;
                }
                calc();
            });
            
            $(".product").on('change',function(){
                option_checker(this)
            });
            
            
            $('#tab_logic tbody').on('keyup change',function(){
                calc();
            });
            $('#gst').on('keyup change',function(){
                calc_total();
            });

        });
        function option_checker(id)
        {
            var myOption=$(id).val();
            var s =0;
            $('#tab_logic tbody tr select').each(function(index, element){
                 var myselect = $(this).val();
                if(myselect==myOption){
                    s += 1;
                }
            });
            if(s>1){
                alert(myOption+' as been added already try new..')  
            }
        }

        /*function option_list(id)
        {
            el='#'+id;
            var myArray = ["Product 1", "Product 2", "Product 3", "Product 4"];
            var collect = '<option value="">Select Product</option>';
            for(var i = 0; i<myArray.length;i++){
                collect += '<option value="'+myArray[i]+'">'+myArray[i]+'</option>';
            }
            $(el+" select").html(collect);
        }*/

        function calc()
        {
            $('#tab_logic tbody tr').each(function(i, element) {
                var html = $(this).html();
                
                    var qty = $(this).find('.qty').val();
                    var price = $(this).find('.price').val();
                    // var discount = $(this).find('.discount').val();
                    // $(this).find('.total').val(qty*price-discount);
                    $(this).find('.total').val(qty*price);                    
                    calc_total();
            });
        }

        function calc_total()
        {
            total=0;
            $('.total').each(function() {
                total += parseInt($(this).val());
            });
            $('#sub_total').val(total.toFixed(2));
            gst_sum=total/100*$('#gst').val();
            $('#gst_amount').val(gst_sum.toFixed(2));
            $('#total_amount').val((gst_sum+total).toFixed(2));
        }

    </script> -->
    <script>
    $(document).ready(function(){
        var gst_sum = 0;
        $(document).on('click','#add_row',function(){

            $clone = $('#tab_logic').find('tbody').find('tr:first').clone();
            $clone1 = $clone.clone();
            $clone1.insertAfter($('#tab_logic').find('tbody').find('tr:last'));
            $('#tab_logic').closest('#tab_logic').find('tbody tr:last').find('input[type="text"]').val('');
            $('#tab_logic').closest('#tab_logic').find('tbody tr:last').find('input[type="number"]').val('');
            
            var i=1;
            $('#tab_logic').find('tbody').find('tr').each(function(){
                $(this).find('td:first').text(i++);
                $('#tab_logic').closest('#tab_logic').find('tbody tr:last').find('input[type="hidden"]').remove();
                $('#tab_logic').closest('#tab_logic').find('tbody tr:last').find('a[href]').attr('href','javascript:void(0);');
                $('#tab_logic').closest('#tab_logic').find('tbody tr:last').find('a[href]').find('button').addClass('delete_row').removeAttr('onclick');
            });       
        });

        $(document).on('click','#delete_row',function(){
            var count = $('#tab_logic').find('tbody').find('tr');
            // console.log(count.length);
            if(count.length > 1){
                $('#tab_logic').closest('#tab_logic').find('tr:last').remove();
            }else{
                alert('You can not remove the current section.');
            }

        });
        $('#tab_logic tbody').on('keyup change',function(){
            calc();
        });
        $('#gst').on('keyup change',function(){
            calc_total();
        });

    });

    function calc()
    {
        $('#tab_logic tbody tr').each(function(i, element) {
            
            var qty = $(this).find('.qty').val();
            var price = $(this).find('.price').val();
            // var discount = $(this).find('.discount').val();
            $(this).find('.total').val(qty*price);
            
            calc_total();
        });
    }

    function calc_total()
    {
        var total=0;
        $('.total').each(function() {
            total += parseInt($(this).val());
        });
        $('#sub_total').val(total.toFixed(2));
        gst_sum=total/100*$('#gst').val();
        total = gst_sum+total;
        $('#gst_amount').val(gst_sum.toFixed(2));
        $('#total_amount').val(total.toFixed(2));
    }
    $(document).ready(function() {
        calc();        
    });
</script>
    
<?php $this->view('authority/common/footer'); ?>
<script type="text/javascript">
    $(".date").datepicker({
        dateFormat: 'dd-mm-yy',
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        todayHighlight: true,
        autoclose: true,            
    });
</script>