<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Category Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Category Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php echo site_url() . "authority/testimonial/view"; ?>">Testimonial</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <div class="box-header with-border">
                        <h3 class="box-title"><?= $testimonial_details !=null ? 'Edit' : 'Add'?> Testimonial</h3>

                        <!-- <form method="post" action="<?//= base_url('authority/testimonial/save_excel_testimonial')?>" enctype="multipart/form-data"><br>                            
                            <div class="col-md-4"> 
                                <input type="file" name="xls_file" id="xls_file" class="form-control" accept=".xlsx, .xls">
                            </div>
                            <div class="col-md-1"> 
                                <input type="submit" class="btn btn-sm btn-success check_excel" value="submit">
                            </div>
                            <div class="col-md-4">                            
                                <a href="<?//= base_url('authority/testimonial/download_excel_testimonial');?>" class="btn btn-sm btn-warning">Excel Download</a>
                            </div>
                        </form>
                        <div class="col-md-12" style="color: red; font-size: 20px;"><center>(OR)</center></div> -->
                    </div>

                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php $this->load->view('authority/common/messages');?>
                        <?php $action = $testimonial_details !=null ? base_url('authority/testimonial/update_testimonial') : base_url('authority/testimonial/insert_testimonial'); ?>

                        <form id="form" method="post" action="<?= $action?>" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="hidden" name="id" id="testimonial_id" value="<?= ($testimonial_details !=null) ? $testimonial_details[0]['id'] : ""; ?>">
                                <div class="col-md-12">
                                    <label for="name">Name :<span class="required">*</span></label> 
                                    <?php
                                        $input_fields = array(
                                            'name' => 'name',
                                            'placeholder' => 'Name',
                                            'class' => 'form-control txtonly',
                                            'id' => 'name',
                                            'value' => (isset($testimonial_details) && $testimonial_details !=null ? $testimonial_details[0]['name'] : ""),
                                        );
                                        echo form_input($input_fields);
                                        echo form_error("name", "<div class='error'>", "</div>");
                                    ?>
                                    <span class="error_cat_name" style="color: #fc3a3a;"></span>
                                </div><br><br><br><br>

                                <div class="col-md-12">
                                    <label for="testimonial_image">Testimonial Image :<span class="required">*</span>(Upload by 100&#215;100)
                                    </label>

                                    <input type="file" name="testimonial_image" class="form-control testimonial_image" class="form-control testimonial_image" accept="image/*">
                                    <span class="error_file" style="color: #fc3a3a;"></span>
                                </div>

                                <?php
                                    if ($testimonial_details != null) {
                                        ?>
                                        <div class="form-group col-md-4">
                                            <label for="last_name">Current image</label><br>
                                            
                                            <img src="<?= base_url(TESTIMONIAL_IMAGE).$testimonial_details[0]['testimonial_image']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">
                                        </div>
                                    <?php }
                                ?><br><br><br><br>

                                <div class="col-md-12">
                                    <label for="description">Description :</label>  
                                    <?php
                                        $input_fields = array(
                                            'name' => 'description',
                                            'class' => 'form-control',
                                            'id' => 'editor1'
                                        );
                                        echo form_textarea($input_fields,isset($testimonial_details) && $testimonial_details !=null ? $testimonial_details[0]['description'] : "");
                                        echo form_error("description", "<div class='error'>", "</div>");
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-12" style="margin-top: 3%">
                                    <input class="btn btn-success text-uppercase check" value="Submit" type="submit">
                                    <a href="<?php echo site_url() . 'authority/testimonial'; ?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            <!-- /.box -->
        </section>
    </div>
    <?php $this->view('authority/common/copyright'); ?>
    <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
    <script>
        /*FORM VALIDATION*/
        var testimonial_id = $("#testimonial_id").val();
        $("#form").validate({
            rules: {
                name: {required: true},    
                // description: {required: true},       
                testimonial_image: { 
                    required: function(element) {
                        if (testimonial_id == '') {  
                            return true;
                        }
                        else {
                            return false;
                        }
                    }, 
                },                  
            },
            messages: {
                name: "Please enter name",
                testimonial_image: "Please select image",       
            }
        });

        // $('.check').click(function(){
        //     var testimonial_id = $("#testimonial_id").val();

        //     var cat_name = $(".name_english").val();
        //     var testimonial_image = $(".testimonial_image")[0].files.length;

        //     if (cat_name_english =='' && cat_name_arabic ==''){
        //         $('.error_cat_name').text('Please enter english OR arabic category-name.');
        //         $('.name_arabic').focus();
        //         return false;
        //     }  

        //     if (testimonial_id == ''){
        //         if(testimonial_image === 0){
        //             $('.error_file').text("Please select image.");
        //             return false;
        //         }
        //     } 
        // });

        // $('.check_excel').click(function(){
        //     if(isemptyfocus('xls_file'))
        //     {
        //         return false;
        //     }
        // });
    </script>
<?php $this->view('authority/common/footer'); ?>