<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/css/jquery-ui-timepicker-addon.css"/>
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/css/select2.min.css">
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>&nbsp;</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="<?php echo site_url() . "authority/home_slider/view"; ?>">Home slider</a></li>
            <!-- <li class="active">Add</li> -->
        </ol>
    </section>

    <section class="content">

        <!-- SELECT2 EXAMPLE -->
        <div class="row">
            <div class="col-lg-offset-2 col-md-offset-2  col-sm-offset-0 col-lg-8 col-md-8 col-sm-12 col-xs-12">
                <div class="box box-default">
                    <?php $action = ($slider_details == null) ? base_url('authority/home_slider/add_slider') : base_url('authority/home_slider/update_slider')?>

                    <form id="form" action="<?= $action ?>" method="post" enctype="multipart/form-data">
                        <input type="hidden" name="id" id="slider_id" value="<?= ($slider_details != null) ? $slider_details[0]['id'] : '';?>">

                        <div class="box-header with-border">
                            <h3 class="box-title"><?= $slider_details == null ? 'Add' : 'Edit';?> Slider image</h3>
                            <div class="box-tools pull-right">
                            </div>
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            <?php $this->load->view('authority/common/messages')?>
                            <!-- <div class="form-group">
                                <label for="full_name">Title :</label>
                                <input type="text" name="title" class="form-control title" placeholder="Title" value="<?= ($slider_details != null) ? $slider_details[0]['title'] : '';?>">
                                <span class="error_title" style="color: #fc3a3a;"></span>
                                <?= form_error('title', '<label class="error">', '</label>');?>
                            </div>

                            <div class="form-group">
                                <label for="last_name">Description :</label>
                                <textarea name="discription" class="form-control discription" placeholder="Description"><?= ($slider_details != null) ? $slider_details[0]['discription'] : '';?></textarea>
                                <?= form_error('discription', '<label class="error">', '</label>');?>
                                <span class="error_dis" style="color: #fc3a3a;"></span>
                            </div> -->

                            <div class="form-group">
                                <label for="last_name">Slide _position:</label>
                                <input type="text" name="slider_position" class="form-control slider_position only_digits" placeholder="Slider position" maxlength="5" value="<?= ($slider_details != null) ? $slider_details[0]['slider_position'] : '';?>">
                            </div>

                            <!-- <div class="form-group">
                                <label for="last_name">Slider link :</label>
                                <input type="text" name="slider_link" class="form-control slider_link" placeholder="Slider link" value="<?= ($slider_details != null) ? $slider_details[0]['slider_link'] : '';?>">
                            </div>   -->  

    						<div class="form-group">
                                <label for="last_name">Image :<span class="required">*</span>(Upload by 920&#xd7;385)</label>
    							<input type="file" id="slider_image" name="slider_image" class="form-control slider_image" accept="image/*">
                                <span class="error_file" style="color: #fc3a3a;"></span>
                            </div>   

                            <?php
                                if ($slider_details != null) {
                                ?>
                                    <div class="form-group">
                                        <label for="last_name">Current_image:</label><br>
                                        <img src="<?= base_url(HOME_SLIDER).$slider_details[0]['slider_image']?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="50px" width="50px">
                                    </div>
                                <?php }
                            ?>

                            <div class="form-group">
                                <input type="submit" class="btn btn-success text-uppercase check" value="Submit">
                                <a href="<?= base_url('authority/home_slider/view')?>" class="btn btn-danger text-uppercase pull-right">Back</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.box -->
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery.ui.touch-punch.min.js"></script>

<script>
	$(document).ready(function(){
        var slider_id = $("#slider_id").val();
		/*FORM VALIDATION*/
		$("#form").validate({
			rules: {
				// title: {required: true},            
				// discription: {required: true},
                slider_image: { 
                    required: function(element) {
                        if (slider_id == '') {  
                            return true;
                        }
                        else {
                            return false;
                        }
                    }, 
                },                  
			},
			messages: {
				// title: "Please enter title",
                // discription: "Please enter description",
                slider_image: "Please select image",       
			}
		});
	})
</script>
<?php $this->view('authority/common/footer'); ?>