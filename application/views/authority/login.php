<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title><?php echo SITE_TITLE; ?></title>
        <link rel="icon" href="<?php echo base_url(); ?>assets/admin/images/favicon.png" />
        <link rel="shortcut icon" href="<?= base_url(PROFILE_PHOTO.'logo1.png'); ?>" />
        <link rel="apple-touch-icon" href="<?= base_url(PROFILE_PHOTO.'logo1.png'); ?>" />
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/font-awesome/css/font-awesome.min.css">
        <!-- Ionicons -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/bower_components/Ionicons/css/ionicons.min.css">
        <!-- Theme style -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/dist/css/AdminLTE.min.css">
        <!-- iCheck -->
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/square/blue.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/css/style.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.css">


        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script type="text/javascript" src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script type="text/javascript" src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <style type="text/css">
            html, body {
                 height: auto; 
            }
        </style>
    </head>
    <body class="hold-transition skin-black-light login-page">
        <div class="login-box">
            <div class="login-box-body">
                <div class="login-logo">
                    <!-- <img src="<?php echo site_url(); ?>assets/uploads/profile_photo/footer-logo.png" alt="logo"/> -->
                    <?php
                        $profile_photo = $this->Production_model->get_all_with_where('user','','',array());
                    ?>
                    <div style="height: 60px">
                        <img src="<?= $profile_photo !=null ? base_url('assets/uploads/profile_photo/').$profile_photo[0]['profile_photo'] : base_url('assets/uploads/profile_photo/default-image.png')?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="90" width="200">
                    </div>
                </div>
                <br/>
                <br/>
                <form action="" method="POST" id="form">
                    <div class="form-group has-feedback">
                        <input type="text" name="email_address" id="email_address" class="form-control" placeholder="Email Address" value="<?php echo (isset($email_address) && $email_address != "") ? $email_address : ""; ?>">
                        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
                        <?php echo form_error('email_address', '<div class="error">', '</div>'); ?>
                    </div>
                    <div class="form-group has-feedback">
                        <input type="password" name="password" class="form-control" placeholder="Password" value="<?php echo (isset($password) && $password != "") ? $password : ""; ?>">
                        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                        <?php echo form_error('password', '<div class="error">', '</div>'); ?>
                        <?php
                        if (isset($message)):
                            echo '<div class="error">' . $message . "</div>";
                        endif;
                        ?>
                    </div>
                    <div class="loading-icon text-center"></div>
                    <div class="form-group has-feedback access_token <?=(isset($access_token_msg))?'show':'hide'?>">
                        <input type="text" name="access_token" id="access_token" class="form-control  " placeholder="Access token" value="<?php echo (isset($access_token) && $access_token != "") ? "" : ""; ?>">
                        <span class="glyphicon glyphicon-king form-control-feedback"></span>
                        <?php echo form_error('access_token', '<div class="error">', '</div>'); ?>
                        <?php
                        if (isset($access_token_msg)):
                            echo '<div class="error">' . $access_token_msg . "</div>";
                        endif;
                        ?>
                    </div>
                    <div class="row">
                        <div class="col-xs-8">
                            <div class="checkbox icheck">
                                <label>
                                    <input type="checkbox" name="remember_me" value="yes" <?php echo (isset($remember_me) && $remember_me == "yes") ? "checked='checked'" : ""; ?>> Remember Me
                                </label>
                            </div>
                        </div>
                        <!-- /.col -->
                        <div class="col-xs-12">
                            <button type="submit" class="btn btn-primary btn-block login getaccess">LOG IN</button>
                        </div>
                        <div class="col-xs-12">
                            <div class="checkbox icheck">
                                Forgot Your Password? <a href="<?php echo base_url(); ?>authority/login/forgot-password">Click Here</a>
                            </div>
                        </div>
                        <!-- /.col -->
                    </div>
                </form>
                <!-- /.social-auth-links -->

                <!--        <a href="#">I forgot my password</a><br>-->
                <!-- <a href="register.html" class="text-center">Register a new membership</a> -->

            </div>
            <!-- /.login-box-body -->
        </div>

        <!-- jQuery 3 -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/jquery/dist/jquery.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- iCheck -->
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/iCheck/icheck.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script> 
        <script>
            var BASE_URL = '<?php echo base_url(); ?>';
            $(function () {
                $('input').iCheck({
                    checkboxClass: 'icheckbox_square-blue',
                    radioClass: 'iradio_square-blue',
                    increaseArea: '20%' // optional
                });
            });

            /*FORM VALIDATION*/
            $("#form").validate({
                rules: {
                    email_address: {required: true},
                    password: {required: true},
                },
                messages: {
                    email_address: "Please enter email address",
                    password: {required: "Please enter password"},
                }
            });


            // var form = $( "#form" );
            // var access_token = $(".access_token");
            // form.validate();
            // console.log(access_token);
            // if(access_token.hasClass('hide')){
            //     $(document).on('click','.getaccess',function(e){
            //         //alert( "Valid: " + form.valid() );
            //         if(form.valid()){
            //             e.preventDefault();
            //             getAacessToken();
            //         }else{
            //             return false;
            //         }
             
            //     });
            // }
                
            //     function getAacessToken(){
            //         var email_address = $('#email_address').val();

            //         var uurl = BASE_URL+"api/user/getaccesstoken";
            //         $.ajax({
            //            url: uurl,
            //            type: 'POST',
            //            dataType: 'json',
            //            data: {email_address:email_address},
            //            //async: false,
            //            beforeSend: function(){
            //              $('.mask').show();
            //              $('.loading-icon').html('<i class="fa fa-refresh fa-spin"></i>');
            //            },
            //            success: function(response){
            //             if (response.result=="Success") {
            //                 $('.access_token').removeClass('hide');
            //                 $('.login').removeClass('getaccess');
            //                 $.alert({
            //                     type: 'green',
            //                     title: 'Login success',
            //                     content: response.message,
            //                 });
            //                 allowSubmit = true;
                            
            //             }else if(response.result=="Fail"){
            //                 $.alert({
            //                     type: 'red',
            //                     title: 'Login failed',
            //                     content: response.message,
            //                 });
            //                 $('.loading-icon').html('<i class="fa fa-refresh"></i>');
            //             }
            //             // console.log(response);
            //            },
            //            error: function(xhr) {
            //            //alert(xhr.responseText);
            //            },
            //            complete: function(){
            //              $('.mask').hide();
            //              $('.loading-icon').hide();
            //            },
            //         });
            //     }
        </script>
    </body>
</html>
