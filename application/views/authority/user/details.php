<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>User Details</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="<?php echo site_url() . "authority/user/view"; ?>"> User</a></li>
            <li class="active">Details</li>
        </ol>
    </section>
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body table-responsive">
                        <table id="mytable" class="table table-bordred">
                            <tbody>
                                <tr>
                                    <td><b>Name:</b></td>
                                    <td><?php echo $full_name; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Email Address:</b></td>
                                    <td><?php echo $email_address; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Mobile Number Primary:</b></td>
                                    <td><?php echo $mobile_number_1; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Gender:</b></td>
                                    <td><?php echo $gender; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Role:</b></td>
                                    <td><?php echo $role; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Profile Image:</b></td>
                                    <td><img width="200" src="<?=base_url(PROFILE_PHOTO.$profile_photo); ?>" class="img img-thumbnail" alt="<?php echo $full_name ?>"/></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>
<?php $this->view('authority/common/footer'); ?>