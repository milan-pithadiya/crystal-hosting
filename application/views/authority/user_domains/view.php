<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/css/footable.bootstrap.min.css">
<!-- iCheck for checkboxes and radio inputs -->
<link rel="stylesheet" href="<?php echo base_url(); ?>assets/admin/plugins/iCheck/all.css">
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>User Domains</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i>Home</a></li>
            <li class="active">User Domains</li>
        </ol>
    </section>
    <section class="content-header">
        <div class="form-group">
            <div class="input-group">
                <?php                            
                    $name = '';
                ?>
                 <span class="input-group-addon">Search</span>
                 <input type="search" name="name" id="name" placeholder="Search" aria-label="Search" class="form-control " style="line-height: 1.9;"  />
                 <span class="input-group-addon">
                    <button class="btn btn-xs btn-primary btn_filter" type="button"><i class="fa fa-search"></i></button>
                </span>
                 
            </div>
            <div class="row">
                <div class="col-md-3">
                    <label for="user_id">Select User</label>
                    <?php 
                        if (isset($user_details) && $user_details !=null) {
                            $option[null] = 'Select user'; 
                            $data = array(
                                'class' => 'form-control user_id ',
                                'id' => 'user_id',
                                'data-live-search'=>'true'
                            );
                            foreach ($user_details['data'] as $key => $value) {
                                $option[$value['id']] = $value['first_name']." ".$value['last_name'];
                            }
                        }
                        echo form_dropdown('user_id',$option,'',$data);
                    ?>
                </div>
                <div class="col-md-3">
                    <label for="month_no">Select Month</label>
                    <?php 
                        if (isset($user_details) && $user_details !=null) {
                            $option2[null] = 'Select month'; 
                            $data1 = array(
                                'class' => 'form-control month_no',
                                'id' => 'month_no',
                            );
                            for($m=1; $m<=12; ++$m)
                                $option2[$m] =  get_month_name($m);
                        }
                        echo form_dropdown('month_no',$option2,'',$data1);
                    ?> 
                </div>
                <!-- <div class="col-md-3">
                    <label for="month_no">Select xyz</label>
                    <?php 
                        if (isset($user_details) && $user_details !=null) {
                            $option2[null] = 'Select'; 
                            $data1 = array(
                                'class' => 'form-control month_no select2',
                                'id' => 'month_no',
                            );
                            for($m=1; $m<=12; ++$m)
                                $option2[$m] =  get_month_name($m);
                        }
                        echo form_dropdown('month_no',$option2,'',$data1);
                    ?> 
                </div>
                <div class="col-md-3">
                    <label for="month_no">Select xyz</label>
                    <?php 
                        if (isset($user_details) && $user_details !=null) {
                            $option2[null] = 'Select'; 
                            $data1 = array(
                                'class' => 'form-control month_no select2',
                                'id' => 'month_no',
                            );
                            for($m=1; $m<=12; ++$m)
                                $option2[$m] =  get_month_name($m);
                        }
                        echo form_dropdown('month_no',$option2,'',$data1);
                    ?> 
                </div> -->
                
            </div>
            <!-- <div class="input-group input-group-sm">
                <input class="form-control form-control-navbar" type="search" name="name" value="<?= $name;?>" placeholder="Search" aria-label="Search">
                <div class="input-group-append">
                    <button class="btn btn-primary btn_filter" type="button"><i class="fas fa-search"></i></button>
                </div>&nbsp;&nbsp;
            </div> -->
        </div>
    </section>
    <section class="content">
        <?php $this->load->view('authority/common/messages');?>
        <!-- /.row -->
        <div class="row">
            <form method="post">
                <div class="col-xs-12">
                    <?php
                        if ($domain_details !=null) {
                            ?>
                                <input type="submit" class="btn btn-md btn-danger chk_submit" value="Delete" formaction="<?= base_url('authority/user_domains/multiple_delete')?>">
                            <?php
                        }
                    ?>
                    <a href="<?= base_url('authority/user_domains/add')?>" class="btn btn-md btn-primary">Add</a> 

                    <div class="box">
                        <!-- /.box-header -->
                        <div class="box-body table-responsive ">
                            <table id="myTable" class="table table-bordred table-striped">
                                <thead>
                                    <tr>
                                        <th>
                                            <div class="checkbox" style="margin: -20px 0 0 30px;"><input type="checkbox" name="check_all" id="select_all"></div>
                                        </th>
                                        <th>No</th>
                                        <th>Domain Name</th>
                                        <th>User</th>
                                        <th>Provider Name</th>
                                        <th>Book Date</th>
                                        <th>Expiry Date</th>
                                        <th data-hide="phone,medium">Status<br/><small></small></th>
                                        <th data-hide="phone,medium" align="center">Action</th>
                                    </tr>
                                </thead>
                                <tbody class="data-response">
                                    <?php
                                    if ($domain_details > 0):
                                        

                                        foreach ($domain_details as $key=> $value) {
                                            $id = $value['id'];
                                            $user_id = $value['user_id'];
                                            
                                            ?>
                                            <tr data-expanded="true">
                                                <td>
                                                    <div class="checkbox" style="margin-left: 30px"><input type="checkbox" name="chk_multi_checkbox[]" class="chk_all" value="<?= $id?>"><div class="checkbox">
                                                </td>
                                                <td><?= $no+$key;?></td>
                                                <td><?= $value['domain_name'].$value['domain_title'];?>
                                                <td><?= $value['username'];?></td>
                                                <td><?= $value['provider_name'];?></td>
                                                <td>
                                                    <?= format_date_d_m_y($value['book_date']);?>
                                                </td>
                                                <td>
                                                    <?= format_date_d_m_y($value['expiry_date']);?>
                                                </td>
                                                <td>
                                                    <?php 
                                                        if($value['status'] == '1'){
                                                            echo '<span class="label label-success change-status" data-table="user_domain" data-id="'.$value['id'].'" data-current-status="1">'.'Active'.'</span>';
                                                            } else {
                                                            echo '<span class="label label-danger change-status" data-table="user_domain" data-id="'.$value['id'].'" data-current-status="0">'.'Deactive'.'</span>';
                                                        } 
                                                    ?>
                                                </td>

                                                <td align="center" class="action">
                                                    <p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo site_url(); ?>authority/user_domains/edit/<?php echo $value['id']; ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a></p>
                                                   
                                                    <p data-placement="top" data-toggle="tooltip" title="Delete"><button type="button" class="btn btn-danger btn-xs delete-btn" data-href="<?php echo site_url(); ?>authority/user_domains/delete_user_domain/<?php echo $value['id']; ?>" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    <?php else: ?>
                                        <tr data-expanded="true">
                                            <td colspan="5" align="center">Records not found</td>
                                        </tr>
                                    <?php endif; ?>
                                </tbody>
                            </table>
                        </div>
                        
                            
                        <div class="box-footer clearfix pagination_filter">
                            <?=(isset($pagination) && $pagination != "")?$pagination:''; ?>
                        </div>
                             
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
            </form>
        </div>
    </section>
</div>

<!-- DELETE POPUP -->
<div class="modal fade" id="delete" tabindex="-1" role="dialog" aria-labelledby="edit" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                <h4 class="modal-title custom_align" id="Heading">Delete this entry</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" value="" name="delete_link" id="delete_link"/>
                <div class="alert alert-danger"><span class="glyphicon glyphicon-warning-sign"></span> Are you sure you want to delete this Record?</div>

            </div>
            <div class="modal-footer ">
                <button type="button" class="btn btn-success btn-confirm-yes" ><span class="glyphicon glyphicon-ok-sign"></span> Yes</button>
                <button type="button" class="btn btn-default btn-confirm-no" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> No</button>
            </div>
        </div>
        <!-- /.modal-content --> 
    </div>
    <!-- /.modal-dialog --> 
</div>
<?php $this->view('authority/common/copyright'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/iCheck/icheck.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/footable-bootstrap/js/footable.min.js"></script>

<script type="text/javascript">
    $(document).ready(function () {
        $(document).on("click", ".delete-btn", function () {
            $("#delete_link").val($(this).data("href"));
        });

        $(".btn-confirm-yes").on("click", function () {
            window.location = $("#delete_link").val();
        });

        $(document).on('click','.change-status',function(){
            var current_element = jQuery(this);
            var id = jQuery(this).data('id');
            var table = jQuery(this).data('table');
            var current_status = jQuery(this).attr('data-current-status');
            // alert(current_status);

            var post_data = {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>',
                'action': 'change_status',
                'id': id,
                'table': table,
                'current_status': current_status,
            }
            $.ajax({
                type: "POST",
                url: BASE_URL + 'authority/ajax/change_status',
                data: post_data,
                async: false,
                success: function (response) {
                    var response = JSON.parse(response);
                    if (response.success) {
                        current_element.toggleClass('label-danger label-success');
                        if(current_element.hasClass('label-success')){
                            current_element.text('Active');
                            current_element.attr('data-current-status','1');
                            } else {
                            current_element.text('Deactive');
                            current_element.attr('data-current-status','0');
                        }
                        } else {
                        window.location = window.location.href;
                    }
                }
            });
        });
    });

    // multiple delete //
    $('.chk_submit').on('click', function() {
        var boxes = $('.chk_all:checkbox');
        if(boxes.length > 0) {
            if($('.chk_all:checkbox:checked').length < 1) {
                $.alert({
                    title: 'Confirm Delete',
                    content: 'Please select at least one checkbox',
                });
                return false;
            }
            else{
                confirm('Are you sure you want to delete this item?');
                return true;
            }
        }
    });
</script>
<?php $this->view('authority/common/footer'); ?>
<script type="text/javascript">
    var name;
    var user_id;
    var month_no;
    $(document).on('change','#name',function(e){            
        name = $('input[name="name"]').val();
        get_filtered_data('',name,user_id,month_no);
    });
    $(document).on('change','#user_id',function(e){            
        user_id = $('#user_id').val();
        get_filtered_data('',name,user_id,month_no);
    });
    $(document).on('change','#month_no',function(e){            
        month_no = $('#month_no').val();
        get_filtered_data('',name,user_id,month_no);
    });
    $(document).ready(function () { 
        $(document).on('click','.pagination_filter a',function(e){            
            var url = $(this).attr('href');
            var split_url = url.split('/');
            var page_no = split_url[7];
            e.preventDefault();
            
            get_filtered_data(page_no,name);
        });

        $(document).on('click','.btn_filter',function(){
            get_filtered_data('',name,user_id,month_no);
        });
    });
    function get_filtered_data(page_no='',name='',user_id='',month_no=''){     
        var post_data = {'page_no':page_no,'name':name,'user_id':user_id,'month_no':month_no};
        $.ajax({
            url: '<?= base_url('authority/user_domains/filter/')?>'+page_no,
            type: 'POST',
            dataType: 'json',
            data: post_data,
            beforeSend: function() {
                setTimeout(function() {}, 5000);
                $('.data-response').html('<tr><td colspan="10" align="center"><i class="fa fa-refresh fa-spin fa-4x"></i></td></tr>');
            },
            success: function(response){
                if (response.success){ 
                    setTimeout(function() {
                    $('.data-response').html(response.details); 
                    }, 900);  
                    
                    $('.pagination_filter').html(response.pagination);
                }
                else if (response.error && response.data_error){
                    $('.data-response').html(response.data_error);
                    $('.pagination_filter').html(response.pagination);
                }
            }, 
            complete: function() {
                setTimeout(function() {
                    $('.loading').css({'display':'none'});
                }, 1000);
                 
            },
        });
    }
</script>