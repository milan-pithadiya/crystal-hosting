<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
      <script src='https://www.google.com/recaptcha/api.js'></script>
    </head>
    <body>
      <div class="main aos-all" id="transcroller-body">
        <?php $this->load->view('include/header');?>
        <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
        <!-- about start -->
        <div class="allpage_banner_about allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'about.jpg')?>);">
            <h1 class="title_h1">Ticket</h1>
            <p><a href="<?=base_url()?>">Home </a> / Ticket</p>
        </div>
        <!-- domain_style start -->
        <div class="padding_all text-center domain_style">
          <div class="container">
            <h5></h5>
            <div class="row">
              <div class="col-md-3 pull-md-left sidebar">
                <div menuitemname="Client Details" class="panel panel-default">
                  <div class="panel-heading">
                      <h3 class="panel-title"> <i class="fa fa-user"></i>&nbsp; Your Info
                      </h3>
                  </div>
                  <div class="panel-body">
                    <p><strong><span id="first_name"></span> <span id="last_name"></span></strong></p><p></p>
                    <p>
                      <span id="address"></span>
                      <span id="country"></span>,<span id="state"></span>,<span id="city"></span>
                    </p>
                  </div>
                  <div class="panel-footer clearfix">
                    <a href="<?=base_url('profile')?>" class="btn btn-success btn-sm btn-block">
                      <i class="fa fa-pencil"></i> Update
                    </a>
                  </div>
                </div>
              </div>
              <div class="col-md-9 ">
              <h3 class="well">Generate Ticket</h3>
              <div class="well well-sm col-md-12" >
                <!-- Ticket Form Starts -->
                <div class=" col-md-12 col-xs-12 contact_box_input">
                  <form method="post" id="ticket" name="ticket" enctype="multipart/form-data">
                    <div class="col-md-12 text-left">
                      <div id="ticketerrors"></div>
                      <div class="col-md-6 ">
                        <label>Name:</label>
                        <input name="name" id="name" type="text" class="input_all" placeholder="Name" value="" readonly>
                        <p class="err_p" id="err_name"></p>
                      </div>
                        
                      <div class="col-md-6 ">
                          <label>Email:</label>
                          <input type="text" name="email" id="email" class="input_all" placeholder="Email" value="" readonly>
                          <p class="err_p" id="email_err"></p>
                      </div>
                      
                      <div class="col-md-10 ">
                            <label>Subject:</label>
                            <input name="subject" id="subject" type="text" class="input_all" placeholder="subject" value="">
                          <p class="err_p" id="subject_err"></p>
                      </div>
                      <div class="col-md-4 ">
                          <label for="sel1">Department:</label>
                          <select class="input_all" id="department" name="department">
                            <?php $dept= $this->uri->segment(3); $sel = 'selected="selected"';?>
                            <option value="" selected="selected">-- Select Department --</option>
                              <option value="1" <?php if($dept=='billing'){echo $sel;}?>>Billing</option>
                              <option value="2" <?php if($dept=='technical-support'){echo $sel;}?> >Technical Support</option>
                              <option value="3" <?php if($dept=='site-down'){echo $sel;}?> >Site Down</option>
                              <option value="4" <?php if($dept=='domain-support'){echo $sel;}?> >Domain Support</option>
                              <option value="5" <?php if($dept=='migration'){echo $sel;}?> >Migration</option>
                              <option value="6" <?php if($dept=='email-support'){echo $sel;}?> >Email Support</option>
                          </select>
                          <p class="err_p" id="department_err"></p>
                      </div>
                      
                      <!-- <div class="col-md-5 ">
                        <label for="sel1">Related Service:</label>
                          <select class="input_all" id="related_service" name="related_service" >
                            <option value="" selected="selected">-- Select Service --</option>
                            <option value="">None</option>
                                                
                          </select>
                          <p class="err_p" id="country_err"></p>
                      </div> -->
                      
                      <div class="col-md-3 ">
                          <label for="sel1">Priority:</label>
                          <select class="input_all" id="priority" name="priority" >
                            <option value="" selected="selected">-- Priority --</option>
                            <option value="1">High</option>
                            <option value="2">Medium</option>
                            <option value="3">Low</option>                                              
                          </select>
                          <p class="err_p" id="priority_err"></p>
                      </div>
                      
                      <div class="col-md-12 ">
                          <label for="sel1">Message:</label>
                          <textarea class="" id="message" name="message" rows="3"></textarea>
                          <p class="err_p" id="country_err"></p>
                      </div>
                      <div class="col-md-12 ">
                          <label for="sel1">Attachment:</label>
                          <input name="user_file" id="user_file" type="file" class="input_all" placeholder="Name" value="">
                          <p class="err_p" id="user_file_err"></p>
                      </div>
                      <div class="col-md-12">
                            <div class="g-recaptcha" data-sitekey="6Le-wvkSAAAAAPBMRTvw0Q4Muexq9bi0DJwx_mJ-" style="margin-top: 2%;"></div>
                            <span class="error_captcha" style="color: #fc3a3a;"></span>   
                        </div>
                    </div>
                    
                    <div class="text-center btn_margin" style="clear:both;">
                      <!-- <button type="button" class="btn btn-primary btn_margin check">Submit</button> -->
                      <input type="button" name="submit_ticket" value=" Submit " class="btn btn-primary btn_margin check" style="background-color: #ff802b;border-color: #ff802b;">
                    </div>
                  </form> 
                </div>
                
              </div>
            </div>
            </div>
          </div>
        </div>
        <!-- domain_style over -->
        <!-- help_line start -->
        <div class="padding_all help_line" style="background-image: url(<?=base_url(IMAGES.'mail.jpg')?>);">
          <div class="container text-center">
            <h1 class="h1_title">Need Help?</h1>
              <h4>Let us help you make the right decision!</h4>
            <div class="row margin_top">
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border">
                  <a href="javascript:;"><i class="fa fa-phone"></i></a>
                  <h3>Call Us</h3>
                  <p>Give us a call & ask all of your questions</p>
                </div>
              </div>
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border ">
                  <a href="javascript:;"><i class="fa fa-pencil"></i></a>
                  <h3>Email Us</h3>
                  <p>Send us an email with all of your questions</p>
                </div>
              </div>
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border margin_row">
                  <a href="javascript:;"><i class="fa fa-video-camera"></i></a>
                  <h3>Live Chat</h3>
                  <p>Chat with a member of our support team now</p>
                </div>
              </div>
              <div class="col-md-3 col-xs-12 margin_top" data-aos="zoom-in-up">
                <div class="help_border margin_row">
                  <a href="javascript:;"><i class="fa fa-certificate"></i></a>
                  <h3>Real Reviews</h3>
                  <p>Read what real customers have to say</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      <?php $this->load->view('include/footer');?>  
    </div>
    <?php $this->load->view('include/footer_js');?>
    <script type="text/javascript">
      $(document).ready(function () {
        get_user_profile('<?=$this->session->login_id?>');
      });
      function get_user_profile(user_id){
          var uurl = BASE_URL+"api/user/getProfile";
          // var post_data = {id:user_id};
           $.ajax({
               url: uurl,
               method: 'POST',
               dataType:'json',
               data: {id:user_id},
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){
                
                if (response.result=="Success") {
                  var userdata = response.data;
                  console.log(userdata);
                  $('#first_name').text(userdata.first_name);
                  $('#last_name').text(userdata.last_name);
                  $('#email').text(userdata.email);
                  $('#address').text(userdata.address);
                  $('#country').text(userdata.country_id_info);
                  $('#state').text(userdata.state_id_info);
                  $('#city').text(userdata.city_id_info);

                  $('#name').val(userdata.first_name+" "+userdata.last_name);
                  $('#email').val(userdata.email);
                }
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
           });
          
      }
    </script>
    <script type="text/javascript">

        $('#ticket').validate({
            rules: {
                name: {
                    required: true,
                },
                email: {
                    required: true,
                    email:true,
                },
                subject: {
                    required: true,
                },
                message: {
                    required: true,
                },
            },
            messages: {
                
                name: {
                  required: "Please enter your name",
                  
                },
                email: {
                  required: "Please enter your email address",
                  email: "Please Enter valid email address",
                  
                },
                subject: {
                  required: "Please enter your subject",
                },
                message: {
                  required: "Please enter your message",
                },
            },
        });

        var form = $("#ticket");
        form.validate();
        $(document).on('click','.check',function(){
          var formData = new FormData($('#ticket')[0]);
          if(form.valid()){
            add_ticket();
          }
        });
        function add_ticket(){          
          var message = CKEDITOR.instances.message.getData();      
          $("#message").val(message);
          var formData = new FormData($('#ticket')[0]);
          var uurl = BASE_URL+"api/plans/generate_ticket";
          var ticketURL = BASE_URL+"ticket";

           $.ajax({
               url: uurl,
               type: 'POST',
               data: formData,
               dataType:'json',
               //async: false,
               beforeSend: function(){
                 $('.mask').show();
                 $('#loader').show();
               },
               success: function(response){

                 if (response.result=="Success") {
                    $.alert({
                      title: 'Message',
                      type: 'green',
                      content: response.message,
                    });
                    setTimeout(function() { window.location.href = ticketURL; }, 2000);
                 }else{
                   $.alert({
                        title: 'Message',
                        type: 'red',
                        content: 'Ticket not generated. !!',
                    });
                 }
               },
               error: function(xhr) {
               //alert(xhr.responseText);
               },
               complete: function(){
                 $('.mask').hide();
                 $('#loader').hide();
               },
               cache: false,
               contentType: false,
               processData: false
           });
        }
    </script> 
    <script src="https://cdn.ckeditor.com/4.14.0/standard/ckeditor.js"></script>
    <script>
            CKEDITOR.replace( 'message' );
    </script>
   </body>
</html> 