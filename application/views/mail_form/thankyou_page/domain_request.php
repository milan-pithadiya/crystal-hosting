<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width,initial-scale=1.0"/>
        <title>Domain Request | <?=SITE_TITLE?></title>
        <link rel="icon" type="image/png" sizes="56x56" href="<?= base_url()?>assets/img/favicon.png">
        <style>
            .logo img,header{width:100%}.container,header{display:inline-block}body{text-align:center;font-family:sans-serif;margin:0;padding:0}*{box-sizing:border-box}a,a:hover{color:#fff;text-decoration:none}.logo{float:left;max-width:133px}.view-link{float:right;margin:21px 0}.view-link a{padding:8px 16px;font-size:18px;background-color:#e10a0a}.view-link a:hover{background-color:#272d32}.email-icon,.verify-btn{background-color:#e10a0a}header{padding:20px 0}.container{width:82%;margin:auto}.email-icon{padding:17px 0}.confirmation-box{box-shadow:0 0 10px 1px #848484;padding:0;margin:25px auto 31px;width:80%}.discription{padding:24px 10px}.verify-btn{padding:10px;color:#fff;display:block;width:43%;margin:auto}.verify-btn:hover{background-color:#272d32}.social a{font-size:24px;color:#fff;border-radius:6px;display:inline-block;width:40px;height:40px;margin:0 6px;text-align:center;}.fb{background-color:#3b5998}.twitter{background-color:#55acee}.email{background-color:#fbad1b}@media all and (max-width:736px){.verify-btn{font-size:13px!important}}@media all and (max-width:530px){.logo{display:inline-block;float:none}.view-link{display:block;float:none;margin:36px 0 9px}}
                .discription a{background-color: #ff802b} .register-link{background-color: #ff802b; color: #FFFFFF;padding: 10px;margin: 5px}table tr td:first-child{font-weight: bold} table tr td{
                    text-align: left;
                }

        </style>
    </head>
    <body>
        <header>
            <div class="container">
                <a href="<?= base_url()?>" class="logo">
                    <?php
	                    $profile_photo = $this->Production_model->get_all_with_where('user','','',array());
	                    if ($profile_photo != null) {
	                    ?>
	                        <img src="<?= $profile_photo !=null ? base_url('assets/uploads/profile_photo/').$profile_photo[0]['profile_photo'] : base_url('assets/uploads/profile_photo/default-image.png')?>">
	                    <?php }
	                ?>
                </a>
                <div class="view-link" style="background-color:#ff802b;color:#FFFFFF;">
                    <a href="<?= base_url()?>" style="background-color:#ff802b;color:#FFFFFF;">View Website</a>
                </div>
            </div>
        </header>
        <div style="clear:both">
        </div>
        <div class="container">
            <div class="confirmation-box col-md-8 col-md-offset-2">
                <div class="email-icon" style="text-align:center;background-color:#ff802b;">
                    <img src="<?= base_url()?>assets/img/mail.png" width="55px">
                </div>
                <div class="discription">
                    <h2>Domain Request</h2>
                    <h3>Hello , <?=(isset($name))?$name:''?></h3>
                    <br> 
                    <table align="center">
                        <tr>
                            <td>Requested Domain </td>
                            <td>: <?=$domain_name?></td>
                        </tr>
                    </table>
                    <br>
                    <h4 style="color: dimgrey;">Your domain request is submitted to Crystal Hosting. </h4>
                    <h4 style="color: dimgrey;">Our executive will contact you soon. </h4>
                </div>
            </div>
        </div>
        <div class="container">
            <footer style="text-align:center;">
                <div class="col-md-8 col-md-offset-2">
                    <h3>
                        Stay in Touch
                    </h3>
                    <?php
                        $social_media_link = $this->Production_model->get_all_with_where('social_links','','',array());
                        extract($social_media_link[0]);
                    ?>
                    <div class="social">
                        <a href="<?=$facebook_link?>" class="fb">
                            <img src="<?= base_url()?>assets/img/fb.png" width="10px">
                        </a>
                        <a href="<?=$twiter_link?>" class="twitter">
                            <img src="<?= base_url()?>assets/img/tw.png" width="20px">
                        </a>
                        <a href="<?= base_url()?>" class="email">
                            <img src="<?= base_url()?>assets/img/mail.png" width="20px">
                        </a>
                    </div>
                    <h4 style="margin: 32px 0">Copyright &copy; <?= date('Y')?>.All rights are reserved.</h4>
                </div>
            </footer>
        </div>
    </body>
</html>