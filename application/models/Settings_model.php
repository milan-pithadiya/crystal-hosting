<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Settings_model extends CI_model {

    public function __construct() {
        $conditions = array(
            "select" => "*",
        );
        $info = $this->common_model->select_data("site_settings", $conditions);
        if ($info['row_count'] > 0) {
            foreach ($info['data'] as $value) {
                define($value['setting_key'], $value['setting_value']);
            }
        }
    }

}
