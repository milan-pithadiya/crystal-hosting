<?php

defined('BASEPATH') OR exit('No direct script access allowed');

Class Common_model extends CI_Model {

    /**
     * 
     * @param type $table_name
     * @param type $data
     * @return boolean
     */
    public function insert_data($table_name, $records = array()) {
        try {
            if (is_array($records) && !empty($records) && $table_name != '') {
                $this->db->insert($table_name, $records);
                return $this->db->insert_id();
            } else {
                return false;
            }
        } catch (Exception $e) {
            exit("There is an error in the insert query");
        }
    }

    /**
     * 
     * @param type $table_name
     * @param type $conditions
     * @return string
     */
    public function select_data($table_name, $conditions = array()) {
        $column_names = array_key_exists("select", $conditions) ? $conditions['select'] : '*';
        $this->db->select($column_names);
        /* For where conditions */
        if (array_key_exists("where", $conditions)) {
            foreach ($conditions['where'] as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        /* JOIN */
        if (array_key_exists("join", $conditions)) {

            /* Getting join table name */
            if (array_key_exists("table", $conditions['join'])) {
                $join_table = $conditions['join']['table'];
            } else {
                exit("Please specify join table");
            }

            /* getting type of join */
            if (array_key_exists("join_type", $conditions['join']) && $conditions['join']['join_type'] != "") {
                $join_type = $conditions['join']['join_type'];
                /* $join_type_arr = array("INNER JOIN", "LEFT JOIN", "RIGHT JOIN"); */
                $join_type_arr = array("INNER", "LEFT", "RIGHT");
                if ($join_type != "" && !in_array($join_type, $join_type_arr)) {
                    exit("Incorrect join method");
                }
            } else {
                $join_type = "";
            }


            if (array_key_exists("join_conditions", $conditions['join'])) {
                $join_conditions = $conditions['join']['join_conditions'];
            } else {
                exit("Please specify join conditions");
            }

            /* getting join conditions */

            if ($join_type == "") {
                $this->db->join($join_table, $join_conditions);
            } else {
                $this->db->join($join_table, $join_conditions, $join_type);
            }
        }
        /* IN */
        if (array_key_exists("IN", $conditions)) {
            foreach ($conditions['IN'] as $key => $value) {
                $this->db->where_in($key, $value);
            }
        }
        /* Not in */
        if (array_key_exists("NOT IN", $conditions)) {
            foreach ($conditions['NOT IN'] as $key => $value) {
                $this->db->where_not_in($key, $value);
            }
        }
        /* LIKE */
        if (array_key_exists("LIKE", $conditions)) {
            foreach ($conditions['LIKE'] as $key => $value) {
                $this->db->like($key, $value);
            }
        }
        /* ORDER BY */
        if (array_key_exists("ORDER BY", $conditions)) {
            //$this->db->order_by($conditions["ORDER BY"][0], $conditions["ORDER BY"][1]);

            foreach ($conditions["ORDER BY"] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        
        if (array_key_exists("GROUP BY", $conditions)) {
            /* Array Required for group by */
            $this->db->group_by($conditions["GROUP BY"]);
        }
        /* SET LIMITS */
        if (array_key_exists("start", $conditions) && array_key_exists("limit", $conditions)) {
            $this->db->limit($conditions['limit'], $conditions['start']);
        } elseif (!array_key_exists("start", $conditions) && array_key_exists("limit", $conditions)) {
            $this->db->limit($conditions['limit']);
        }
        $query_result = $this->db->get($table_name);

        /* FOR RETURN ONLY TOTAL RECORDS */
        if (array_key_exists("countall", $conditions)) {
            return $query_result->num_rows();
        } else {
            /* FOR GENERATING RESULT */
            $result = array();
            if ($query_result->num_rows() > 0) {
                $result['row_count'] = $query_result->num_rows();
                $result['data'] = $query_result->result_array();
            } else {
                $result['row_count'] = 0;
                $result['data'] = "";
            }
            return $result;
        }
    }

    /**
     * 
     * @param type $table_name
     * @param type $conditions
     * @return string
     */
    public function select_data_with_join($table_name, $conditions = array()) {

        $column_names = array_key_exists("select", $conditions) ? $conditions['select'] : '*';
        $this->db->select($column_names);
        $this->db->from($table_name);
        if (array_key_exists("join", $conditions)) {

            /* Getting join table name */
            if (array_key_exists("table", $conditions['join'])) {
                $join_table = $conditions['join']['table'];
            } else {
                exit("Please specify join table");
            }

            /* getting type of join */
            if (array_key_exists("join_type", $conditions['join']) && $conditions['join']['join_type'] != "") {
                $join_type = $conditions['join']['join_type'];
                /* $join_type_arr = array("INNER JOIN", "LEFT JOIN", "RIGHT JOIN"); */
                $join_type_arr = array("INNER", "LEFT", "RIGHT");
                if ($join_type != "" && !in_array($join_type, $join_type_arr)) {
                    exit("Incorrect join method");
                }
            } else {
                $join_type = "";
            }


            if (array_key_exists("join_conditions", $conditions['join'])) {
                $join_conditions = $conditions['join']['join_conditions'];
            } else {
                exit("Please specify join conditions");
            }

            /* getting join conditions */

            if ($join_type == "") {
                $this->db->join($join_table, $join_conditions);
            } else {
                $this->db->join($join_table, $join_conditions, $join_type);
            }
        }

        /* For other conditionss */
        if (array_key_exists("where", $conditions)) {
            foreach ($conditions['where'] as $key => $value) {
                $this->db->where($key, $value);
            }
        }
        /* Not in */
        if (array_key_exists("NOT IN", $conditions)) {
            foreach ($conditions['NOT IN'] as $key => $value) {
                $this->db->where_not_in($key, $value);
            }
        }
        /* ORDER BY */
        if (array_key_exists("ORDER BY", $conditions)) {
            //$this->db->order_by($conditions["ORDER BY"][0], $conditions["ORDER BY"][1]);
            foreach ($conditions["ORDER BY"] as $key => $value) {
                $this->db->order_by($key, $value);
            }
        }
        $query_result = $this->db->get();
        $result = array();
        if ($query_result->num_rows() > 0) {
            $result['row_count'] = $query_result->num_rows();
            $result['data'] = $query_result->result_array();
        } else {
            $result['row_count'] = 0;
            $result['data'] = "";
        }
        return $result;
    }

    /**
     * 
     * @param type $table_name
     * @param type $records
     * @param type $conditions
     */
    public function update_data($table_name, $records = array(), $conditions = array()) {
        try {
            if (array_key_exists("where", $conditions)) {
                foreach ($conditions['where'] as $key => $value) {
                    $this->db->where($key, $value);
                }
            }
            $this->db->update($table_name, $records);
            if ($this->db->affected_rows() > 0) {
				return true;
			} else {
				return true;
			}
        } catch (Exception $e) {
            exit("There is an error in the update query");
        }
    }

    /**
     * 
     * @param type $table_name
     * @param type $conditions
     */
    public function delete_data($table_name, $conditions = array()) {
        try {
            if (array_key_exists("where", $conditions)) {
                foreach ($conditions['where'] as $key => $value) {
                    $this->db->where($key, $value);
                }
            }
            $this->db->delete($table_name);
            if ($this->db->affected_rows() > 0) {
                return true;
            } else {
                return false;
            }
        } catch (Exception $e) {
            exit("There is an error in the delete query");
        }
    }

    /**
     * 
     * @param type $sql
     * @return int
     */
    public function get_data_with_sql($sql) {
        try {
            $result = array();
            $query_result = $this->db->query($sql);
            if ($query_result->num_rows() > 0) {
                $result['row_count'] = $query_result->num_rows();
                $result['data'] = $query_result->result_array();
            } else {
                $result['row_count'] = 0;
            }
            return $result;
        } catch (Exception $e) {
            exit("There is an error in the query");
        }
    }

    /**
     * 
     * @param type $sql
     * @return int
     */
    public function simple_query($sql) {
        try {
            if ($this->db->simple_query($sql)) {
                return true;
            } else {
                //$error = $this->db->error();
                return false;
            }
        } catch (Exception $e) {
            exit("There is an error in the query");
        }
    }

    public function only_pagination($settings = array()) {

        /* $default_settings = array(
          "total_record" => "",
          "url" => "",
          "per_page" => "",
          ); */
        $response = array();

        $this->load->library('pagination');
        $config_pagination = array();
        $config_pagination['base_url'] = isset($settings['url']) ? $settings['url'] : site_url();
        $config_pagination['total_rows'] = $settings['total_record'];
        $config_pagination['per_page'] = isset($settings['per_page']) ? $settings['per_page'] : RECORDS_PER_PAGE;
        $config_pagination["uri_segment"] = 4;
        // $config_pagination['num_links'] = 4;
        $config_pagination['use_page_numbers'] = TRUE;
        $config_pagination['display_pages'] = TRUE;
        $config_pagination['full_tag_open'] = '<ul class="pagination pull-right">';
        $config_pagination['full_tag_close'] = '</ul>';
        $config_pagination['first_link'] = 'First';
        $config_pagination['first_tag_open'] = '<li>';
        $config_pagination['first_tag_close'] = '</li>';
        $config_pagination['last_link'] = 'Last';
        $config_pagination['last_tag_open'] = '<li>';
        $config_pagination['last_tag_close'] = '</li>';
        $config_pagination['next_link'] = '<span class="glyphicon glyphicon-chevron-right"></span>';
        $config_pagination['next_tag_open'] = '<li>';
        $config_pagination['next_tag_close'] = '</li>';
        $config_pagination['prev_link'] = '<span class="glyphicon glyphicon-chevron-left"></span>';
        $config_pagination['prev_tag_open'] = '<li>';
        $config_pagination['prev_tag_close'] = '</li>';
        $config_pagination['cur_tag_open'] = '<li><a class="active" href="">';
        $config_pagination['cur_tag_close'] = '</a></li>';
        $config_pagination['num_tag_open'] = '<li>';
        $config_pagination['num_tag_close'] = '</li>';
        $this->pagination->initialize($config_pagination);
        $response['pagination'] = $this->pagination->create_links();
        // echo"<pre>"; print_r($response); exit;

        // $page_number = ($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
        $page_number = (isset($settings['page_number']) && $settings['page_number'] != '') ? $settings['page_number'] : (($this->uri->segment(4)) ? $this->uri->segment(4) : 0);        
        $config_pagination['cur_page'] = $page_number;

        if ($page_number > ceil(($config_pagination['total_rows'] / $config_pagination['per_page']))) {
            /* Redirect when page limit exceeded */
            redirect($config_pagination['base_url']);
        }

        /* GETTING A OFFSET */
        $offset = ($page_number == 0) ? 0 : ($page_number * $config_pagination['per_page']) - $config_pagination['per_page'];

        /* FOR SELECTING DATA WITH START AND END LIMIT */
        $response['start'] = $offset;
        $response['limit'] = $settings['per_page'];
        $response['no'] =  $response['start']+1;
        return $response;
    }

    public function get_pagination($table_name = "", $conditions = array(), $settings = array()) {

        $total_record = $this->select_data($table_name, array_merge($conditions, array("countall" => "countall")));
        $settings = array_merge($settings, array("total_record" => $total_record));

        $response = array();
        $response = $this->only_pagination($settings);
        /* FOR SELECTING DATA WITH START AND END LIMIT */
        $conditions = array_merge($conditions, array("start" => $response['start'], "limit" => $response['limit']));
        $conditions = $this->select_data($table_name, $conditions);
        $response = array_merge($response, $conditions);
        unset($conditions, $settings, $total_record);
        return $response;
    }

    public function print_query() {
        echo $this->db->last_query();
    }

}
