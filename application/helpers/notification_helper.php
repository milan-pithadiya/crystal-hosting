<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (!function_exists('get_notifications')) {
	function get_notifications($user_id){
	   $CI =& get_instance();

       $sql = "SELECT notification.*,notification_status.read_status,notification_status.remove_status,post_management.add_type,post_management.i_want,post_management.post_image,post_management.post_description,post_management.price,post_management.order_id,post_management.package_id,post_management.payment_details,post_management.payment_details,chat_invitation.seller_status,
       IF(notification.product_id=chat_invitation.product_id AND chat_invitation.seller_id=notification.user_id , 'Show', 'Hide') as buttons

        FROM notification
        LEFT JOIN notification_status ON notification_status.notification_id = notification.notification_id
        LEFT JOIN post_management ON post_management.post_id = notification.product_id
        LEFT JOIN chat_invitation ON chat_invitation.product_id = notification.product_id
        WHERE notification.user_id = '".$user_id."' ORDER BY notification.notification_id DESC";

        $query = $CI->db->query($sql);
        $get_user = $query->result_array();

        // echo"<pre>"; echo $CI->db->last_query(); print_r($get_user); exit; 

        if ($get_user !=null) { 
            $rec = array(); 
            $chat_data = array();
            $count = 0; 
            foreach ($get_user as $key => $value) {
                if ($value['remove_status'] == null || empty($value['remove_status']) || $value['remove_status'] == 0) {
                    if ($value['type'] == 2) {
                        $owner_name = $CI->Production_model->get_all_with_where('user_register','','',array("user_id"=>$value['buyer_id']));
                    }
                    if ($value['seller_status'] == 0) {
                        $status = 'Pending';
                    }
                    if ($value['seller_status'] == 1) {
                        $status = 'Accept';
                    }
                    if ($value['seller_status'] == 2) {
                        $status = 'Reject';
                    }
                    $rec[$count]['notification_id'] = $value['notification_id'];
                    $rec[$count]['title'] = $value['title'];
                    $rec[$count]['message'] = $value['description'];
                    $rec[$count]['image'] = $value['image'];
                    $rec[$count]['type'] = $value['type'];

                    $rec[$count]['read_status'] = $value['read_status'];
                    $rec[$count]['remove_status'] = $value['remove_status'];
                    $rec[$count]['product_id'] = $value['product_id'];
                    $rec[$count]['user_id'] = $value['user_id'];
                    $rec[$count]['buyer_id'] = $value['buyer_id'];
                    $rec[$count]['add_type'] = $value['add_type'];
                    $rec[$count]['i_want'] = $value['i_want'];
                    $rec[$count]['post_description'] = $value['post_description'];
                    $rec[$count]['post_status'] = $status;
                    $rec[$count]['buttons'] = $value['buttons'];
                    $rec[$count]['created_date'] = date('d-m-Y h:i:s',strtotime($value['create_date']));

                    if (isset($owner_name) && $owner_name !=null) {                        
                        $chat_data = array(
                            'post_owner_name' => $owner_name[0]['name'],
                            'profile_picture' => $owner_name[0]['profile_picture'],
                            'post_title' => $value['i_want'],
                            'post_description' => $value['post_description'],
                            'status' => $status,
                        );
                    }
                    $rec[$count]['chat_data'] = isset($chat_data) && $chat_data !=null ? $chat_data : null;
                    $count++;           
                }
            }   
            
            $return['result'] = "success";
            $return['message'] = "Notification Found Successfully...!";
            $return['data'] = $rec;
            return json_encode($return); 
        }
        else{
            $return['result'] = "fail";
            $return['message'] = "You don't have any notification(s)...!";
            return json_encode($return);
        }
    } 
}

if (!function_exists('update_notification')) {
	function update_notification($user_id,$notification_id,$type,$status){
		$CI =& get_instance();
        $temp_noti_id = explode(',', $notification_id);
        foreach ($temp_noti_id as $value) {
            $result = $CI->Production_model->get_all_with_where('notification_status', '', '', array('user_id' => $user_id, 'notification_id' => $value));

            $responce = count($result) > 0 ? $CI->Production_model->update_record('notification_status', $type == 0 ? array('read_status' => $status) :
                            array('remove_status' => $status), array('user_id' => $user_id, 'notification_id' => $value)) :
                    $CI->Production_model->insert_record('notification_status', $type == 0 ? array('read_status' => $status, 'user_id' => $user_id, 'notification_id' => $value) :
                            array('remove_status' => $status, 'user_id' => $user_id, 'notification_id' => $value));
        }
    }
}
?>