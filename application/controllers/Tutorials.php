<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	class Tutorials extends CI_Controller {
		public function __construct()
		{
			parent::__construct();			
		}
		function index()
		{
			$data['tutorial_faq_details'] = $this->Production_model->get_all_with_where('tutorial_faq','','',array('status'=>'1'));
			$this->load->view('tutorials',$data);
		}
		function video_tutorials(){
			$data['tutorial_details'] = $this->Production_model->get_all_with_where('tutorial','','',array('status'=>'1'));
			$this->load->view('video_tutorials',$data);	
		}
		function video_tutorial_links($id){
			$data['video_tutorial_links'] = $this->Production_model->get_all_with_where('tutorial','','',array());
			$this->load->view('video_tutorial_links',$data);	
		}
		function view_video_tutorial($id){
			$data['tutorial_details'] = $this->Production_model->get_all_with_where('tutorial','','',array('status'=>'1','seo_slug'=>$id));
			$this->load->view('view_video_tutorial',$data);	
		}
	}
?>