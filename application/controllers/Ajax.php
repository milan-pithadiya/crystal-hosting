<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {

    public function __construct() {
        parent::__construct();
        /* For to check user is logged in or not */
        //$this->common_functions->is_front_user_loggedin();
        $this->load->library('form_validation');
        $this->load->helper('address_helper');
    }

    public function get_states() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $error = false;
        $response_array = array('success' => false);
        $id_country = $this->input->post('id_country');
        if ($id_country != '') {
            $state_info = get_state($id_country);
            $data = '';
            if (count($state_info) > 0) {
                $data = '<option value="">Select State</option>';
                foreach ($state_info as $key => $value) {
                    $data .= '<option value="' . $value['id'] . '">' . $value['state_name'] . '</option>';
                }
            }
            $response_array = array('success' => true, 'data' => $data);
        }
        echo json_encode($response_array);
        exit;
    }

    public function get_cities() {
        if (!$this->input->is_ajax_request()) {
            exit('No direct script access allowed');
        }
        $error = false;
        $response_array = array('success' => false);
        $id_state = $this->input->post('id_state');
        if ($id_state != '') {
            $city_info = get_city($id_state);
            $data = '';
            if (count($city_info) > 0) {
                $data = '<option value="">Select City</option>';
                foreach ($city_info as $key => $value) {
                    $data .= '<option value="' . $value['id'] . '">' . $value['city_name'] . '</option>';
                }
            }
            $response_array = array('success' => true, 'data' => $data);
        }
        echo json_encode($response_array);
        exit;
    }

}
