<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Privacy_policy extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();			
		}

		function index()
		{
			$data['privacy_data'] = $this->Production_model->get_all_with_where('web_settings','','',array('id'=>2));
			$this->load->view('privacy_policy',$data);	
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>