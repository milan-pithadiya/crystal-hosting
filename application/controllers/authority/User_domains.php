<?php defined('BASEPATH') OR exit('No direct script access allowed');
class User_domains extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index($page_number='') {
        $settings = array(
            "url" => site_url() . "authority/user_domains/index/",
            "per_page" => RECORDS_PER_PAGE,
        );
        if ($this->input->get('clear-search') == 1) {
            $this->session->user_domain_info = array();
            redirect(base_url('authority/user_domains'));
        }
        if(isset($first_name) && $first_name != ""){
            $where['user_register.first_name'] = $first_name;
        }else{
            $where = array();
        }

        $join[0]['table_name'] = 'user_register';
        $join[0]['column_name'] = 'user_register.id = user_domain.user_id';
        $join[0]['type'] = 'left';

        $join[1]['table_name'] = 'domain';
        $join[1]['column_name'] = 'domain.id = user_domain.domain_type_id';
        $join[1]['type'] = 'left';
        
        $tmp_data = $this->Production_model->jointable_descending(array('user_domain.*','user_register.id as user_id','concat(user_register.first_name," ",user_register.last_name) as username','domain.title as domain_title'),'user_domain','',$join,'user_domain.id','desc',$where,array());

        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/user_domains/index/');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;

        $record = $this->Production_model->only_pagination($tmp_array);


        $join[0]['table_name'] = 'user_register';
        $join[0]['column_name'] = 'user_register.id = user_domain.user_id';
        $join[0]['type'] = 'left';

        $join[1]['table_name'] = 'domain';
        $join[1]['column_name'] = 'domain.id = user_domain.domain_type_id';
        $join[1]['type'] = 'left';

        $data['domain_details'] = $this->Production_model->jointable_descending(array('user_domain.*','user_register.id as user_id','concat(user_register.first_name," ",user_register.last_name) as username','domain.title as domain_title'),'user_domain','',$join,'user_domain.id','desc',$where,array(),'',$record['limit'],$record['start']);

        $data['pagination'] = $record['pagination'];

        $data['no'] = $record['no']; 
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data['user_details'] = $this->common_model->select_data("user_register", $conditions);
        // echo "<pre>";print_r($data);exit;
        unset($settings, $conditions);        
        $this->load->view('authority/user_domains/view', $data);
    }
    function add()
    {
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data['user_details'] = $this->common_model->select_data("user_register", $conditions);
        if (isset($this->session->domain_msg) && $this->session->domain_msg != '') {
            $data = array_merge($data, array("success" => $this->session->domain_msg));
            $this->session->domain_msg = '';
        }
        $conditions1 = array("select" => "*",'ORDER BY'=>array('id'=>'ASC'),"where"=>array('status'=>'1'));
        $data['currency_data'] = $this->common_model->select_data("currency", $conditions1);

        $conditions2 = array("select" => "*",'ORDER BY'=>array('id'=>'ASC'),"where"=>array('status'=>'1'));
        $data['domain_type_data'] = $this->common_model->select_data("domain", $conditions2);

        $conditions4 = array("select" => "*",'ORDER BY'=>array('id'=>'ASC'),"where"=>array('status'=>'1'));
        $hosting_plan_data = $this->common_model->select_data("hosting_price", $conditions4);
        $data['hosting_plan_data'] = (!empty($hosting_plan_data['data']))?$hosting_plan_data['data']:array();
        
        $data['user_domains_details'] = array();
        // echo "<pre>";print_r($data);exit;
        unset($settings, $conditions);
        $this->load->view('authority/user_domains/add-edit',$data);
    }    
    function insert_domain()
    {
        if($this->input->post()){    
            $data = $this->input->post();
            $create_date = date('Y-m-d H:i:s');            
            $this->validate_it();
            if ($this->form_validation->run()) {
                $domain_data = array(
                    'domain_name' => $data['domain_name'],
                    'provider_name' => $data['provider_name'],
                    'username' => $data['username'],
                    'password' => $this->encryption->encrypt($data['password']),
                    'book_date' => format_date_ymd($data['book_date']),
                    'expiry_date' => add_year_date_ymd($data['book_date'],1),
                    'user_id' => $data['user_id'],
                    'domain_type_id' => $data['domain_type_id'],
                    'price' => $data['price'],
                    'currency_id' => $data['currency_id'],
                    'costing_price' => $data['costing_price'],
                    'costing_currency_id' => $data['costing_currency_id'],
                );
                    
                $record = $this->Production_model->insert_record('user_domain',$domain_data);   
                if($record !='') {
                    if($data['hosting'] !=0){
                        $hosting_data = array(
                            'provider_name' => $data['host_provider_name'],
                            'host_username' => $data['host_username'],
                            'host_password' => $this->encryption->encrypt($data['host_password']),
                            'hosting_plan_id' => $data['hosting_plan_id'],
                            'book_date' => format_date_ymd($data['book_date']),
                            'expiry_date' => add_year_date_ymd($data['book_date'],1),
                            'user_id' => $data['user_id'],
                            'domain_id' => $record,

                        );  
                        $record2 = $this->Production_model->insert_record('user_hosting',$hosting_data);
                    }

                    $this->session->set_flashdata('success', 'User Domain Add Successfully....!');
                    redirect(base_url('authority/user_domains'));
                }else{
                    $this->session->set_flashdata('error', 'User Domain Not Added....!');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
        }
    }
    function edit($id)
    {
        $data['user_domains_details'] = $this->Production_model->get_all_with_where('user_domain','','',array('id'=>$id));
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data['user_details'] = $this->common_model->select_data("user_register", $conditions);
        if (isset($this->session->domain_msg) && $this->session->domain_msg != '') {
            $data = array_merge($data, array("success" => $this->session->domain_msg));
            $this->session->domain_msg = '';
        }
        $conditions1 = array("select" => "*",'ORDER BY'=>array('id'=>'ASC'),"where"=>array('status'=>'1'));
        $data['currency_data'] = $this->common_model->select_data("currency", $conditions1); 

        $conditions2 = array("select" => "*",'ORDER BY'=>array('id'=>'ASC'),"where"=>array('status'=>'1'));
        $data['domain_type_data'] = $this->common_model->select_data("domain", $conditions2);

        $conditions3 = array("select" => "*","where"=>array('domain_id'=>$id));
        $user_hosting = $this->common_model->select_data("user_hosting", $conditions3);
        $data['user_hosting'] = (!empty($user_hosting['data'][0]))?$user_hosting['data'][0]:array();

        $conditions4 = array("select" => "*",'ORDER BY'=>array('id'=>'ASC'),"where"=>array('status'=>'1'));
        $hosting_plan_data = $this->common_model->select_data("hosting_price", $conditions4);
        $data['hosting_plan_data'] = (!empty($hosting_plan_data['data']))?$hosting_plan_data['data']:array();

        $this->load->view('authority/user_domains/add-edit',$data);
    }
    function update_domain()
    {
        $data = $this->input->post();
    
        $id = $data['id'];
        $modified_date = date('Y-m-d H:i:s');
        $domain_data = array(
            'domain_name' => $data['domain_name'],
            'provider_name' => $data['provider_name'],
            'username' => $data['username'],
            'password' => $this->encryption->encrypt($data['password']),
            'book_date' => format_date_ymd($data['book_date']),
            'expiry_date' => add_year_date_ymd($data['book_date'],1),
            'user_id' => $data['user_id'],
            'domain_type_id' => $data['domain_type_id'],
            'price' => $data['price'],
            'currency_id' => $data['currency_id'],
            'costing_price' => $data['costing_price'],
            'costing_currency_id' => $data['costing_currency_id'],
        );
        $record = $this->Production_model->update_record('user_domain',$domain_data,array('id'=>$id));
        if ($record) {
            if($data['hosting'] !=0){
                $hosting_data = array(
                    'provider_name' => $data['host_provider_name'],
                    'host_username' => $data['host_username'],
                    'host_password' => $this->encryption->encrypt($data['host_password']),
                    'hosting_plan_id' => $data['hosting_plan_id'],
                    'book_date' => format_date_ymd($data['book_date']),
                    'expiry_date' => add_year_date_ymd($data['book_date'],1),
                    'user_id' => $data['user_id'],
                ); 
                $record2 = $this->Production_model->update_record('user_hosting',$hosting_data,array('domain_id'=>$id));
            }
            $this->session->set_flashdata('success', 'User Domain Update Successfully....');
            redirect(base_url('authority/user_domains'));
        }else{
            $this->session->set_flashdata('error', 'User Domain Not Updated....');
            redirect($_SERVER['HTTP_REFERER']);
        }        
    }
    function delete_user_domain($id)
    {        
        $record = $this->Production_model->delete_record('user_domain',array('id'=>$id));
        if ($record != 0) {
            $this->session->set_flashdata('success', 'User Domain Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'User Domain Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $record = $this->Production_model->delete_record('user_domain',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'User Domain Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'User Domain Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
    function validate_it(){
        $this->form_validation->set_rules('domain_name', 'domain_name', 'trim|required', array("required" => "Please enter domain_name"));
        $this->form_validation->set_rules('book_date', 'book_date', 'trim|required', array("required" => "Please enter book_date"));
        $this->form_validation->set_rules('user_id', 'user_id', 'trim|required', array("required" => "Please select user_id"));
        $this->form_validation->set_rules('domain_type_id', 'domain_type_id', 'trim|required', array("required" => "Please select domain_type_id"));
        $this->form_validation->set_rules('currency_id', 'currency_id', 'trim|required', array("required" => "Please select currency_id"));
        $this->form_validation->set_rules('user_id', 'user_id', 'trim|required', array("required" => "Please select user_id"));
    }
    function filter()
    {
        // $this->session->user_domain_info = $_POST;

        $data[] = $this->input->post();

        $where = [];
        
        $name = $_POST['name'];
        $user_id = $_POST['user_id'];
        $month_no = $_POST['month_no'];
        $or_like_array =array();
        
        if(isset($user_id) && $user_id != ""){
            $where['user_register.id'] = $user_id;
        }
        $join[0]['table_name'] = 'user_register';
        $join[0]['column_name'] = 'user_register.id = user_domain.user_id';
        $join[0]['type'] = 'left';

        $join[1]['table_name'] = 'domain';
        $join[1]['column_name'] = 'domain.id = user_domain.domain_type_id';
        $join[1]['type'] = 'left';
        if (isset($name) && $name !=null) {
            // $this->db->group_start();
            // $this->db->like('title', $name);
            // $this->db->group_end();
            $or_like_array['user_domain.domain_name']=$name;
            $or_like_array['user_register.first_name']= $name;
            $or_like_array['user_register.last_name']= $name;
            $or_like_array['user_domain.provider_name']= $name;
        }
        
        $tmp_data = $this->Production_model->jointable_descending(array('user_domain.*','user_register.id as user_id','concat(user_register.first_name," ",user_register.last_name) as username','domain.title as domain_title','EXTRACT(MONTH FROM (user_domain.expiry_date)) as expiry_month'),'user_domain','',$join,'user_domain.id','desc',$where,array(),'','','',$or_like_array);
        $filteredData=[];
        $tmp_array['total_record'] = count($tmp_data);
        $tmp_array['url'] = base_url('authority/bonds/index');
        $tmp_array['per_page'] = RECORDS_PER_PAGE;
        $record = $this->Production_model->only_pagination($tmp_array);

        // if (isset($name) && $name !=null) {
        //     $this->db->group_start();
        //     $this->db->like('title', $name);
        //     $this->db->group_end();
        // }
        if (isset($name) && $name !=null) {
            
            $or_like_array['user_domain.domain_name']=$name;
            $or_like_array['user_register.first_name']= $name;
            $or_like_array['user_register.last_name']= $name;
            $or_like_array['user_domain.provider_name']= $name;
        }
        

        $filteredData1 = $this->Production_model->jointable_descending(array('user_domain.*','user_register.id as user_id','concat(user_register.first_name," ",user_register.last_name) as username','domain.title as domain_title','EXTRACT(MONTH FROM (user_domain.expiry_date)) as expiry_month'),'user_domain','',$join,'user_domain.id','desc',$where,array(),array(),$record['limit'],$record['start'],$or_like_array); 
        $data['pagination'] = $record['pagination']; 
        $data['no'] = $record['no']; 

        if(isset($month_no) && $month_no!=''){
            foreach ($filteredData1 as $key => $value) {
                if($value['expiry_month'] == $month_no ){
                    $filteredData[] = $value;
                }
                
            }
            // echo "<pre>";print_r($filteredData);exit;
        }else{
            $filteredData=$filteredData1;
        }

        ob_start();
        if (isset($filteredData) && !empty($filteredData) ) { 
            foreach ($filteredData as $key => $value) {
                $id = $value['id'];
                ?>
                    <tr data-expanded="true">
                        <td>
                            <div class="checkbox" style="margin-left: 30px"><input type="checkbox" name="chk_multi_checkbox[]" class="chk_all" value="<?= $id?>"><div class="checkbox">
                        </td>
                        <td><?= $key+$record['no'];?></td>
                        <td><?= $value['domain_name'].$value['domain_title'];?>
                        <td><?= $value['username'];?>
                        <td><?= $value['provider_name'];?>
                        </td>
                        <td>
                            <?= format_date_d_m_y($value['book_date']);?>
                        </td>
                        <td>
                            <?= format_date_d_m_y($value['expiry_date']);?>
                        </td>
                        <td>
                            <?php 
                                if($value['status'] == '1'){
                                    echo '<span class="label label-success change-status" data-table="user_domain" data-id="'.$value['id'].'" data-current-status="1">'.'Active'.'</span>';
                                    } else {
                                    echo '<span class="label label-danger change-status" data-table="user_domain" data-id="'.$value['id'].'" data-current-status="0">'.'Deactive'.'</span>';
                                } 
                            ?>
                        </td>

                        <td align="center" class="action">
                            <p data-placement="top" data-toggle="tooltip" title="Edit"><a href="<?php echo site_url(); ?>authority/user_domains/edit/<?php echo $value['id']; ?>" class="btn btn-primary btn-xs"><span class="glyphicon glyphicon-pencil"></span></a></p>
                           
                            <p data-placement="top" data-toggle="tooltip" title="Delete"><button type="button" class="btn btn-danger btn-xs delete-btn" data-href="<?php echo site_url(); ?>authority/user_domains/delete_user_domain/<?php echo $value['id']; ?>" data-title="Delete" data-toggle="modal" data-target="#delete" ><span class="glyphicon glyphicon-trash"></span></button></p>
                        </td>
                    </tr>
                <?php
            }   
            $response_array['success'] = true;
            $response_array['details'] = ob_get_clean();                
            $response_array['pagination'] = $data['pagination'];                
        }else{
            $response_array['error'] = true;
            $response_array['data_error'] = '<tr data-expanded="true">
                                                <td colspan="10" align="center">Records not found</td>
                                            </tr>'; 
            $response_array['pagination'] = '';                     
        }           
        echo json_encode($response_array); exit;
    } 
}
?>