<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Hosting_price extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index($page_number='') {
        $settings = array(
            "url" => site_url() . "authority/hosting_price/index/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data = $this->common_model->get_pagination("hosting_price", $conditions, $settings);
        if (isset($this->session->hosting_price_msg) && $this->session->hosting_price_msg != '') {
            $data = array_merge($data, array("success" => $this->session->hosting_price_msg));
            $this->session->plan_msg = '';
        }
        unset($settings, $conditions);        
        $this->load->view('authority/hosting_price/view', $data);
    }
    function add()
    {
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data['features_details'] = $this->common_model->select_data("common_feature", $conditions);
        if (isset($this->session->hosting_price_msg) && $this->session->hosting_price_msg != '') {
            $data = array_merge($data, array("success" => $this->session->hosting_price_msg));
            $this->session->plan_msg = '';
        }

        $conditions1 = array("select" => "*",'ORDER BY'=>array('id'=>'ASC'),"where"=>array('status'=>'1'));
        $data['currency_data'] = $this->common_model->select_data("currency", $conditions1);
        $data['hosting_price_details'] = array();
        // echo "<pre>";print_r($data);exit;
        unset($settings, $conditions);
        $this->load->view('authority/hosting_price/add-edit',$data);
    }    
    function insert_hosting_price()
    {
        if($this->input->post()){    
            $data = $this->input->post();
            $create_date = date('Y-m-d H:i:s');
            $resultSet = Array();                 
            $get_sub_title = $this->Production_model->get_all_with_where('hosting_price','','',array('title'=> $title));
            $data = array(
                'title' => $data['title'],
                'price' => $data['price'],
                'price' => $data['price'],
                'create_date' => $create_date
            );
            
            $record = $this->Production_model->insert_record('hosting_price',$data);
            $chkbox_id = $this->input->post('feature_id');

            foreach ($chkbox_id as $key => $value) {
                $data = array(
                    "hosting_id"=>$record,
                    "feature_id"=>$value
                );
                $record1 = $this->Production_model->insert_record('hosting_related_feature',$data);
            }
               
            if($record !='') {
                $this->session->set_flashdata('success', 'Hosting price Add Successfully....!');
                redirect(base_url('authority/hosting_price'));
            }else{
                $this->session->set_flashdata('error', 'Hosting price Not Added....!');
                redirect($_SERVER['HTTP_REFERER']);
            }
        }
    }

    function edit($id)
    {
        $data['hosting_price_details'] = $this->Production_model->get_all_with_where('hosting_price','','',array('id'=>$id));
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data['features_details'] = $this->common_model->select_data("common_feature", $conditions);
        if (isset($this->session->hosting_price_msg) && $this->session->hosting_price_msg != '') {
            $data = array_merge($data, array("success" => $this->session->hosting_price_msg));
            $this->session->plan_msg = '';
        }
        // echo "<pre>";print_r($data);exit;
        $conditions1 = array("select" => "*",'ORDER BY'=>array('id'=>'ASC'),"where"=>array('status'=>'1'));
        $data['currency_data'] = $this->common_model->select_data("currency", $conditions1);

        $data['feature_related_details'] = $this->Production_model->get_all_with_where('hosting_related_feature','','',array('status'=> '1')); 
        $this->load->view('authority/hosting_price/add-edit',$data);
    }
    function update_hosting_price()
    {
        $data = $this->input->post();
        $id = $data['id'];
        $modified_date = date('Y-m-d H:i:s');
        $id = $this->input->post('id');        
        $get_image = $this->Production_model->get_all_with_where('hosting_price','','',array('id'=>$id));
        $data = array(
            'title' => $data['title'],
            'price' => $data['price'],
            'currency_id' => $data['currency_id'],
            'modified_date' => $modified_date
        );
        $record = $this->Production_model->update_record('hosting_price',$data,array('id'=>$id));

        if ($record == 1) {
            $chkbox_id = $this->input->post('feature_id');
            foreach ($chkbox_id as $key => $value) {
                $alreadyt_exists = $this->Production_model->get_all_with_where('hosting_related_feature','','',array('hosting_id'=>$id,'feature_id'=>$value));

                // echo $id." ".$value."<br>";
                if(empty($alreadyt_exists)){
                    $data = array(
                        "hosting_id"=>$id,
                        "feature_id"=>$value
                    );
                    $record1 = $this->Production_model->insert_record('hosting_related_feature',$data);
                }
            }
            $this->session->set_flashdata('success', 'Hosting price Update Successfully....');
            redirect(base_url('authority/hosting_price'));
        }else{
            $this->session->set_flashdata('error', 'Hosting price Not Updated....');
            redirect($_SERVER['HTTP_REFERER']);
        }        
    }
    function delete_hosting_price($id)
    {        
        $record = $this->Production_model->delete_record('hosting_price',array('id'=>$id));
      
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Hosting price Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Hosting price Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $record = $this->Production_model->delete_record('hosting_price',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Hosting price Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Hosting price Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>