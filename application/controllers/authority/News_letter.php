<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class News_letter extends CI_Controller {

	private $_search_array = array('full_name'=>'Full name','user_name'=>'User name');

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function view() {
      
        // ==================== pagination start ======================== //

        $data = $this->Production_model->pagination_create(base_url('authority/news_letter/view/'),'news_letter','','id','asc',array());

        // ==================== pagination end ======================== //
        
        $this->load->view('authority/news_letter/view',$data);
    }

    function add()
    {
        $data['news_letter_data'] = array();
        $this->load->view('authority/news_letter/add-edit',$data);
    }

    function add_news_letter()
    {
        $data = $this->input->post();
        // echo"<pre>"; print_r($data); exit;
        
        if($_FILES['news_letter_image']['name']==""){ $img_name='';}
        else{
            $imagePath = NEWS_LETTER_IMAGE;
            if(!is_dir($imagePath)){
                mkdir($imagePath);
                @chmod($imagePath,0777);
            }
            $img_name = 'NEWSLETTER-'.rand(111,999)."-".str_replace(' ', '_',$_FILES['news_letter_image']['name']);

            $destFile = $imagePath . $img_name; 
            $filename = $_FILES["news_letter_image"]["tmp_name"];       
            move_uploaded_file($filename,  $destFile);

            // create Thumbnail -- IMAGE_SIZES end //
            $data['image'] = $img_name;
        }
        // echo "<pre>"; print_r($data); exit;

        $record = $this->Production_model->insert_record('news_letter',$data);
        if ($record !='') {
            $this->session->set_flashdata('success', $this->lang->line("added"));
            redirect(base_url('authority/news_letter/view')); 
        }
        else
        {
            $this->session->set_flashdata('error', $this->lang->line("not_added"));
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function edit($id)
    {
        $data['news_letter_data'] = $this->Production_model->get_all_with_where('news_letter','','',array('id'=>$id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
        $this->load->view('authority/news_letter/add-edit',$data);
    }

    function update_news_letter()
    {
        $news_letter_id = $this->input->post('id');
        $data = $this->input->post();
        // echo"<pre>"; print_r($data); exit;

        if($_FILES['news_letter_image']['name'] !='')
        {
            $imagePath = NEWS_LETTER_IMAGE;
            if(!is_dir($imagePath)){
                mkdir($imagePath);
                @chmod($imagePath,0777);
            }            
            $get_image = $this->Production_model->get_all_with_where('news_letter','','',array('id'=>$news_letter_id));
            // echo"<pre>"; print_r($get_image); exit;
            if ($get_image !=null && $get_image[0]['image'] !=null && !empty($get_image[0]['image']))
            {
                @unlink(NEWS_LETTER_IMAGE.$get_image[0]['image']);
            }
            
            $img_name = 'NEWSLETTER-'.rand(111,999)."-".$_FILES['news_letter_image']['name'];

            $destFile = $imagePath . $img_name; 
            $filename = $_FILES["news_letter_image"]["tmp_name"];       
            move_uploaded_file($filename,  $destFile);

            $data['image'] = $img_name;
        }
        // echo "<pre>"; print_r($data); exit;

        $record = $this->Production_model->update_record('news_letter',$data,array('id'=>$news_letter_id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;
        if ($record == 1) {
            $this->session->set_flashdata('success', $this->lang->line("updated"));
            redirect(base_url('authority/news_letter/view'));
        }
        else
        {
            $this->session->set_flashdata('error', $this->lang->line("not_updated"));
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function delete($id)
    {
        $get_image = $this->Production_model->get_all_with_where('news_letter','','',array('id'=>$id));
        
        if ($get_image !=null && $get_image[0]['image'] !=null && !empty($get_image[0]['image']))
        {
            @unlink(NEWS_LETTER_IMAGE.$get_image[0]['image']);
        }
        $record = $this->Production_model->delete_record('news_letter',array('id'=>$id));

        if ($record == 1) {
            $this->session->set_flashdata('success', $this->lang->line("deleted"));
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', $this->lang->line("not_deleted"));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $get_image = $this->Production_model->get_all_with_where('news_letter','','',array('id'=>$value));
  
            if ($get_image !=null && !empty($get_image[0]['image']))
            {
                @unlink(NEWS_LETTER_IMAGE.$get_image[0]['image']);
            }
            $record = $this->Production_model->delete_record('news_letter',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', $this->lang->line("deleted"));
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', $this->lang->line("not_deleted"));
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>