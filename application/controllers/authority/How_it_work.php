<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class How_it_work extends CI_Controller {

	private $_search_array = array('full_name'=>'Full name','user_name'=>'User name');

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function view() {

        // ==================== pagination start ======================== //

        $data = $this->Production_model->pagination_create(base_url('authority/how-it-work/view/'),'how_it_work','','id','asc',array());

        // ==================== pagination end ======================== //
        
        $this->load->view('authority/how-it-work/view',$data);
    }

    function add()
    {
        $data['work_data'] = array();
        $this->load->view('authority/how-it-work/add-edit',$data);
    }

    function add_work()
    {
        $data = $this->input->post();
        // echo"<pre>"; print_r($data); exit;
        
        if($_FILES['work_image']['name']==""){ $img_name='';}
        else{
            $imagePath = WORK_IMAGE;
            if(!is_dir($imagePath)){
                mkdir($imagePath);
                @chmod($imagePath, 0777);
            }
            $img_name = 'WORK-'.rand(111,999)."-".str_replace(' ', '_',$_FILES['work_image']['name']);

            $destFile = $imagePath . $img_name; 
            $filename = $_FILES["work_image"]["tmp_name"];       
            move_uploaded_file($filename,  $destFile);

            $data['work_image'] = $img_name;
        }
        // echo "<pre>"; print_r($data); exit;

        $record = $this->Production_model->insert_record('how_it_work',$data);
        if ($record !='') {
            $this->session->set_flashdata('success', 'Work Add Successfully....!');
            redirect(base_url('authority/how-it-work/view')); 
        }
        else
        {
            $this->session->set_flashdata('error', 'Work Not Added....!');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function edit($id)
    {
        $data['work_data'] = $this->Production_model->get_all_with_where('how_it_work','','',array('id'=>$id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;
        $this->load->view('authority/how-it-work/add-edit',$data);
    }

    function update_work()
    {
        $about_id = $this->input->post('id');
        $data = $this->input->post();
        // echo"<pre>"; print_r($data); exit;

        if($_FILES['work_image']['name'] !='')
        {
            $get_image = $this->Production_model->get_all_with_where('how_it_work','','',array('id'=>$about_id));
            // echo"<pre>"; print_r($get_image); exit;
            if ($get_image !=null && $get_image[0]['work_image'] !=null && !empty($get_image[0]['work_image']))
            {
                @unlink(WORK_IMAGE.$get_image[0]['work_image']);
            }

            $imagePath = WORK_IMAGE;
            if(!is_dir($imagePath)){
                mkdir($imagePath);
                @chmod($imagePath, 0777);
            }
            $img_name = 'WORK-'.rand(111,999)."-".$_FILES['work_image']['name'];

            $destFile = $imagePath . $img_name; 
            $filename = $_FILES["work_image"]["tmp_name"];       
            move_uploaded_file($filename,  $destFile);
           
            $data['work_image'] = $img_name;
        }
        // echo "<pre>"; print_r($data); exit;

        $record = $this->Production_model->update_record('how_it_work',$data,array('id'=>$about_id));
        // echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;
        if ($record == 1) {
            $this->session->set_flashdata('success', 'Work Update Successfully....!');
            redirect(base_url('authority/how-it-work/view'));
        }
        else
        {
            $this->session->set_flashdata('error', 'Work Not Updated....!');
            redirect($_SERVER['HTTP_REFERER']);
        }   
    }

    function delete($id)
    {
        $get_image = $this->Production_model->get_all_with_where('how_it_work','','',array('id'=>$id));
        
        if ($get_image !=null && $get_image[0]['work_image'] !=null && !empty($get_image[0]['work_image']))
        {
            @unlink(WORK_IMAGE.$get_image[0]['work_image']);
            // @unlink(WORK_IMAGE.'thumbnail/medium/'.$get_image[0]['work_image']);
            // @unlink(WORK_IMAGE.'thumbnail/thumb/'.$get_image[0]['work_image']);
        }
        $record = $this->Production_model->delete_record('how_it_work',array('id'=>$id));

        if ($record == 1) {
            $this->session->set_flashdata('success', 'Work Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Work Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $get_image = $this->Production_model->get_all_with_where('how_it_work','','',array('id'=>$value));
  
            if ($get_image !=null && !empty($get_image[0]['work_image']))
            {
                @unlink(WORK_IMAGE.$get_image[0]['work_image']);
                // @unlink(BLOG_MEDIUM_IMAGE.$get_image[0]['work_image']);
                // @unlink(BLOG_THUMB_IMAGE.$get_image[0]['work_image']);
            }
            $record = $this->Production_model->delete_record('how_it_work',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Work Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Work Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>