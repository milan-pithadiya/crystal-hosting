<?php
defined('BASEPATH') OR exit('No direct script access allowed');

	class Register extends CI_Controller {

		public function __construct()
		{
			parent::__construct();
		}
		
		public function index()
		{
			$this->load->view('register');
		}

		function add_register()
		{
			$data = $this->input->post();
			// echo"<pre>"; print_r($data); exit;

			$this->form_validation->set_rules('name', 'name', 'required');
			$this->form_validation->set_rules('mobile_no', 'mobile_no', 'required');
			// $this->form_validation->set_rules('id_country', 'country', 'required');
			// $this->form_validation->set_rules('id_state', 'state', 'required');
			// $this->form_validation->set_rules('id_city', 'city', 'required');
			// $this->form_validation->set_rules('address', 'address', 'required');
			$this->form_validation->set_rules('user_email', 'user_email', 'required');
			// $this->form_validation->set_rules('gender', 'gender', 'required');
			$this->form_validation->set_rules('password', 'password', 'required');
			$this->form_validation->set_rules('confirm_password', 'confirm_password', 'required|matches[password]');

			if ($this->form_validation->run() == FALSE)
	        {
	        	$this->load->view('register');
	            // $this->session->set_flashdata('error','Please All The Fields Required Data...!');
				// redirect($_SERVER['HTTP_REFERER']);
	        }
	        else
	        {
	        	$data = $this->input->post();
	        	unset($data['confirm_password']);
	        	
	        	$data['type'] = 'web';
	        	$data['password'] = $this->encryption->encrypt($data['password']);
	          	// echo "<pre>"; print_r($data); exit;

	          	$get_user_email = $this->Production_model->get_all_with_where('user_register','','',array('user_email'=>$data['user_email']));
				if (count($get_user_email) > 0) {
					$this->session->set_flashdata('error', 'Email id allredy exist....!');
					redirect($_SERVER['HTTP_REFERER']);
				}

				$get_user_mobile = $this->Production_model->get_all_with_where('user_register','','',array('mobile_no'=>$data['mobile_no']));
				if (count($get_user_mobile) > 0) {
					$this->session->set_flashdata('error', 'Mobile no allredy exist....!');
					redirect($_SERVER['HTTP_REFERER']);
				}
				else
				{		
					// echo"<pre>"; print_r($data); exit;		
					// $this->load->view('mail_form/admin_send_mail/registration', $data);		
					$record = $this->Production_model->insert_record('user_register',$data);
					if ($record !='') {
						$send_mail = $this->Production_model->mail_send('Crystal Hositing Account',$data['user_email'],'','mail_form/thankyou_page/registration','',''); // user send email thank-you page

						$admin_email = $this->Production_model->get_all_with_where('user','','',array()); 
						$send_mail = $this->Production_model->mail_send('Crystal Hositing Account',$admin_email[0]['email_address'],'','mail_form/admin_send_mail/registration',$data,''); // admin send mail

						// if ($send_mail == 1) {
						// }
						$this->session->set_flashdata('success', 'Thank you for registration');
						redirect($_SERVER['HTTP_REFERER']);
					}
					else
					{
						$this->session->set_flashdata('error', 'Not Registered....!');
						redirect($_SERVER['HTTP_REFERER']);
					}
				}	
			}	
		}
	}
?>