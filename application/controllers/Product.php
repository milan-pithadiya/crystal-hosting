<?php 
	defined('BASEPATH') OR exit('No direct script access allowed');
	
	class Product extends CI_Controller {
	
		public function __construct()
		{
			parent::__construct();			
		}

		function index()
		{	
			$data['category_details'] = $this->Production_model->get_all_with_where('category','category_id','desc',array('status'=>'1'));

			$where['product_manage.status'] = '1';				
			$join[0]['table_name'] = 'category';
			$join[0]['column_name'] = 'category.category_id = product_manage.category_id';
			$join[0]['type'] = 'left';

			$join[1]['table_name'] = 'sub_category';
			$join[1]['column_name'] = 'sub_category.sub_category_id = product_manage.sub_category_id';
			$join[1]['type'] = 'left';
			
			$tmp_data = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage','',$join,'','desc',$where);

			// echo"<pre>"; print_r($data); exit;
			//================= create pagination start ===================//

			$tmp_array['total_record'] = count($tmp_data);
			$tmp_array['url'] = base_url('product/index/page/');
			$tmp_array['per_page'] = RECORDS_PER_PAGE_FRONT;

			$record = $this->Production_model->only_pagination($tmp_array);
			// echo"<pre>"; print_r($record); exit;

			$data['all_pro_details'] = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage','',$join,'','desc',$where,'','',$record['limit'],$record['start']);			

			$data['pagination'] = $record['pagination'];
			// echo"<pre>"; print_r($data['all_pro_details']); exit;			

			/*Best seller*/ 
			$where1['product_manage.best_seller'] = '1';
			
			$join1[0]['table_name'] = 'category';
			$join1[0]['column_name'] = 'category.category_id = product_manage.category_id';
			$join1[0]['type'] = 'left';

			$join1[1]['table_name'] = 'sub_category';
			$join1[1]['column_name'] = 'sub_category.sub_category_id = product_manage.sub_category_id';
			$join1[1]['type'] = 'left';

			$data['best_seller_details'] = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage','',$join1,'','desc',$where1);	
			// echo"<pre>"; print_r($data['best_seller_details']); exit;
			$data['cat_pro_details'] = array();	
			$data['sub_cat_pro_details'] = array();	
			$data['pro_name_wise_filter'] = array();	

			$this->load->view('product',$data);	
		}

		function category($catid){

			$data['category_details'] = $this->Production_model->get_all_with_where('category','category_id','asc',array('status'=>'1'));

			$where['product_manage.category_id'] = $catid;
			$where['product_manage.status'] = '1';

			$join[0]['table_name'] = 'category';
			$join[0]['column_name'] = 'category.category_id = product_manage.category_id';
			$join[0]['type'] = 'left';

			$join[1]['table_name'] = 'sub_category';
			$join[1]['column_name'] = 'sub_category.sub_category_id = product_manage.sub_category_id';
			$join[1]['type'] = 'left';
			
			$tmp_data = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage','',$join,'','desc',$where,'','');

			//================= create pagination start ===================//

			$tmp_array['total_record'] = count($tmp_data);
			$tmp_array['url'] = base_url('product/category/'.$catid);
			$tmp_array['per_page'] = RECORDS_PER_PAGE_FRONT;

			$record = $this->Production_model->only_pagination($tmp_array);
			// echo"<pre>"; print_r($record); exit;

			$data['cat_pro_details'] = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage','',$join,'','desc',$where,'','',$record['limit'],$record['start']);			

			$data['pagination'] = $record['pagination'];
			// echo"<pre>"; echo $this->db->last_query(); print_r($data); exit;

			//================= create pagination end ===================//

			/*Best seller*/ 
			$where1['product_manage.best_seller'] = '1';
			
			$join1[0]['table_name'] = 'category';
			$join1[0]['column_name'] = 'category.category_id = product_manage.category_id';
			$join1[0]['type'] = 'left';

			$join1[1]['table_name'] = 'sub_category';
			$join1[1]['column_name'] = 'sub_category.sub_category_id = product_manage.sub_category_id';
			$join1[1]['type'] = 'left';

			$data['best_seller_details'] = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage','',$join1,'','desc',$where1);	

			$data['sub_cat_pro_details'] = array();
			$data['all_pro_details'] = array();
			$data['hot_deal_details'] = array();
			$data['pro_name_wise_filter'] = array();
			
			$this->load->view('product',$data);	
		}

		function sub_category($subcat_id){
			$data['category_details'] = $this->Production_model->get_all_with_where('category','category_id','asc',array('status'=>'1'));

			$where['product_manage.sub_category_id'] = $subcat_id;
			$where['product_manage.status'] = '1';

			$join[1]['table_name'] = 'category';
			$join[1]['column_name'] = 'category.category_id = product_manage.category_id';
			$join[1]['type'] = 'left';

			$join[2]['table_name'] = 'sub_category';
			$join[2]['column_name'] = 'sub_category.sub_category_id = product_manage.sub_category_id';
			$join[2]['type'] = 'left';
			
			$tmp_data = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage','',$join,'','desc',$where,'','');

			//================= create pagination start ===================//

			$tmp_array['total_record'] = count($tmp_data);
			$tmp_array['url'] = base_url('product/sub_category/'.$subcat_id);
			$tmp_array['per_page'] = RECORDS_PER_PAGE_FRONT;

			$record = $this->Production_model->only_pagination($tmp_array);
			// echo"<pre>"; print_r($record); exit;

			$data['sub_cat_pro_details'] = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage','',$join,'','desc',$where,'','',$record['limit'],$record['start']);		

			$data['pagination'] = $record['pagination'];
			// echo"<pre>"; echo $this->db->last_query(); print_r($data['sub_cat_pro_details']); exit;
			
			//================= create pagination end ===================//

			/*Best seller*/  
			$where1['product_manage.best_seller'] = '1';
			
			$join1[0]['table_name'] = 'category';
			$join1[0]['column_name'] = 'category.category_id = product_manage.category_id';
			$join1[0]['type'] = 'left';

			$join1[1]['table_name'] = 'sub_category';
			$join1[1]['column_name'] = 'sub_category.sub_category_id = product_manage.sub_category_id';
			$join1[1]['type'] = 'left';

			$data['best_seller_details'] = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage','',$join1,'','desc',$where1);	

			$data['cat_pro_details'] = array();
			$data['all_pro_details'] = array();
			$data['hot_deal_details'] = array();
			$data['pro_name_wise_filter'] = array();
			// echo"<pre>"; echo $this->db->last_query(); print_r($data['sub_cat_pro_details']); exit;

			$this->load->view('product',$data);	
		}
		
		function product_details($id){

			$data['similar_image'] = $this->Production_model->get_all_with_where('product_similar_image','product_id','desc',array('product_id'=>$id)); // multiple image upload...			
			
			$where['product_manage.product_id'] = $id;
			$where['product_manage.status'] = '1';			
			
			$join[0]['table_name'] = 'category';
			$join[0]['column_name'] = 'category.category_id = product_manage.category_id';
			$join[0]['type'] = 'left';

			$join[1]['table_name'] = 'sub_category';
			$join[1]['column_name'] = 'sub_category.sub_category_id = product_manage.sub_category_id';
			$join[1]['type'] = 'left';

			$data['product_details'] = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage','',$join,'product_id','desc',$where);

			// echo"<pre>"; print_r($data['product_details']); exit;
			//======================== get similer product image start ========================//

			$where2['product_similar_image.product_id'] = $id;
			
			$join2[0]['table_name'] = 'category';
			$join2[0]['column_name'] = 'category.category_id = product_manage.category_id';
			$join2[0]['type'] = 'left';

			$join2[1]['table_name'] = 'sub_category';
			$join2[1]['column_name'] = 'sub_category.sub_category_id = product_manage.sub_category_id';
			$join2[1]['type'] = 'left';

			$join2[2]['table_name'] = 'product_similar_image';
			$join2[2]['column_name'] = 'product_similar_image.product_id = product_manage.product_id';
			$join2[2]['type'] = 'left';
			
			$data['similer_image'] = $this->Production_model->jointable_descending(array('product_manage.product_id','category.category_name','sub_category.sub_category_name','product_similar_image.product_similar_image','product_similar_image.image_id'),'product_manage','',$join2,'product_id','desc',$where2);
			
			//======================== get similer product image end ========================//

			//======================== related product start ========================//

			$where3['product_manage.related_product'] = '1';
			$where3['product_manage.status'] = '1';
			
			$join3[0]['table_name'] = 'category';
			$join3[0]['column_name'] = 'category.category_id = product_manage.category_id';
			$join3[0]['type'] = 'left';

			$join3[1]['table_name'] = 'sub_category';
			$join3[1]['column_name'] = 'sub_category.sub_category_id = product_manage.sub_category_id';
			$join3[1]['type'] = 'left';
			
			$data['relate_prouct'] = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage','',$join3,'product_id','desc',$where3);
			
			//======================== related product end ========================//

			// echo"<pre>"; print_r($data['relate_prouct']); exit;

			$this->load->view('product_details',$data);	
		}

		function product_search(){
			$data['category_details'] = $this->Production_model->get_all_with_where('category','category_id','desc',array('status'=>'1'));

			$pro_name_search = $this->input->post('pro_name_search'); 

		 	$search['product_manage.product_name'] = $pro_name_search;
			$where['product_manage.status'] = '1';
			
			$join[1]['table_name'] = 'category';
			$join[1]['column_name'] = 'category.category_id = product_manage.category_id';
			$join[1]['type'] = 'left';

			$join[2]['table_name'] = 'sub_category';
			$join[2]['column_name'] = 'sub_category.sub_category_id = product_manage.sub_category_id';
			$join[2]['type'] = 'left';
			
			$tmp_data = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage',$search,$join,'','desc',$where,'','');

			//================= create pagination start ===================//

			$tmp_array['total_record'] = count($tmp_data);
			$tmp_array['url'] = base_url('product/product_search');
			$tmp_array['per_page'] = RECORDS_PER_PAGE_FRONT;

			$record = $this->Production_model->only_pagination($tmp_array);
			// echo"<pre>"; print_r($record); exit;

			$data['pro_name_wise_filter'] = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage',$search,$join,'','desc',$where,'','',$record['limit'],$record['start']);			

			$data['pagination'] = $record['pagination'];
			// echo"<pre>"; echo $this->db->last_query(); print_r($data['pro_name_wise_filter']); exit;

			//================= create pagination end ===================//

			/*Best seller*/ 
			$where1['product_manage.best_seller'] = '1';
			
			$join1[0]['table_name'] = 'category';
			$join1[0]['column_name'] = 'category.category_id = product_manage.category_id';
			$join1[0]['type'] = 'left';

			$join1[1]['table_name'] = 'sub_category';
			$join1[1]['column_name'] = 'sub_category.sub_category_id = product_manage.sub_category_id';
			$join1[1]['type'] = 'left';

			$data['best_seller_details'] = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage','',$join1,'','desc',$where1);	

			$data['cat_pro_details'] = array();	
			$data['sub_cat_pro_details'] = array();
			$data['all_pro_details'] = array();
			$data['hot_deal_details'] = array();			
			$this->load->view('product',$data);	
		}

		// function write_review(){
			
		// 	$product_id = $this->input->post('product_id');
		// 	$user_name = $this->input->post('user_name');
		// 	$user_comment = $this->input->post('user_comment');
		// 	$ratting_star = $this->input->post('ratting_star');
		// 	$data = array(
		// 		'product_id' => $product_id,
		// 		'login_id' => $this->session->userdata('login_id'),
		// 		'user_name' => $user_name,
		// 		'ratting_star' => $ratting_star,
		// 		'user_comment' => $user_comment,
		// 		'create_date' => date('Y-m-d H:i:s')
		// 	); 
  //           // echo "<pre>"; print_r($data); exit;

		// 	$get_record = $this->Production_model->get_all_with_where('product_ratting','','',array('product_id'=>$product_id,'login_id'=>$this->session->userdata('login_id')));
  //       	// echo"<pre>"; print_r($get_record); exit;
        	
		// 	if (count($get_record) > 0) {
		// 		$response_array['allredy'] = false;
		// 	}
		// 	else{
		// 		if ($this->session->userdata('login_id') !=null){
		// 			$record = $this->Production_model->insert_record('product_ratting', $data);
		// 			if ($record !=null) {
		// 				$response_array['success'] = true;
		// 			}
		// 		}
		// 		else{
		// 			$response_array['error'] = false;
		// 		}
		// 	}	
		// 	echo json_encode($response_array);
		// }

		// function delete_review($id){
		// 	$record = $this->Production_model->delete_record('product_ratting',array('ratting_id'=>$id));
		// 	if ($record == 1) {
		// 		redirect($_SERVER['HTTP_REFERER']);
		// 	}
		// 	else{
		// 		redirect($_SERVER['HTTP_REFERER']);
		// 	}
		// }

		// function pro_quick_view($id){
		// 	$data['similar_image'] = $this->Production_model->get_all_with_where('product_similar_image','product_id','desc',array('product_id'=>$id)); // multiple image upload...

		// 	$data['review_details'] = $this->Production_model->get_all_with_where('product_ratting','ratting_id','desc',array('product_id'=>$id,'status'=>'1')); // get all review in product...
			
		// 	// echo"<pre>"; print_r($data); exit;
		// 	$where['product_manage.product_id'] = $id;
		// 	$where['product_manage.status'] = '1';
		//  $where['product_manage_attribute.currency_id'] = CURRENCY;
			
		// 	$join[0]['table_name'] = 'product_manage_attribute';
		// 	$join[0]['column_name'] = 'product_manage_attribute.product_id = product_manage.product_id';
		// 	$join[0]['type'] = 'left';

		// 	$join[1]['table_name'] = 'category';
		// 	$join[1]['column_name'] = 'category.category_id = product_manage.category_id';
		// 	$join[1]['type'] = 'left';

		// 	$join[2]['table_name'] = 'sub_category';
		// 	$join[2]['column_name'] = 'sub_category.sub_category_id = product_manage.sub_category_id';
		// 	$join[2]['type'] = 'left';
			
		// 	$data['product_details'] = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage','',$join,'product_id','desc',$where,'','','');

		// 	//======================== get related_product product start ========================//

		// 	$where2['product_manage.related_product'] = '1';
			
		// 	$join2[0]['table_name'] = 'product_manage_attribute';
		// 	$join2[0]['column_name'] = 'product_manage_attribute.product_id = product_manage.product_id';
		// 	$join2[0]['type'] = 'left';

		// 	$join2[1]['table_name'] = 'category';
		// 	$join2[1]['column_name'] = 'category.category_id = product_manage.category_id';
		// 	$join2[1]['type'] = 'left';

		// 	$join2[2]['table_name'] = 'sub_category';
		// 	$join2[2]['column_name'] = 'sub_category.sub_category_id = product_manage.sub_category_id';
		// 	$join2[2]['type'] = 'left';
			
		// 	$data['related_product'] = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage','',$join2,'product_id','desc',$where2,'','product_id','');
			
		// 	//======================== get related_product product end ========================//

		// 	// ===================== get product-color && pro-attribute start ================= //

		// 	$where3['product_color.product_id'] = $id;
		// 	$where3['product_color.status'] = '1';
			
		// 	$join3[0]['table_name'] = 'color';
		// 	$join3[0]['column_name'] = 'color.color_id = product_color.color_id';
		// 	$join3[0]['type'] = 'left';
			
		// 	$data['pro_color_details'] = $this->Production_model->jointable_descending(array('product_color.*','color.color_name'),'product_color','',$join3,'id','desc',$where3,'','','');
			
		// 	$data['pro_attr_details'] = $this->Production_model->get_all_with_where('product_manage_attribute','','',array('product_id'=>$id));
			
		// 	// ===================== get product-color && pro-attribute end ================== //

		// 	// echo"<pre>"; print_r($data['pro_attr_details']); exit;
		// 	$this->load->view('product_quick_view',$data);
		// }

		// function best_seller(){

		// 	$data['pro_slider_details'] = $this->Production_model->get_all_with_where('product_slider','status','asc',array('status'=>'1'));
				
		// 	$where['product_manage.best_seller'] = '1';
			
		// 	$join[0]['table_name'] = 'product_manage_attribute';
		// 	$join[0]['column_name'] = 'product_manage_attribute.product_id = product_manage.product_id';
		// 	$join[0]['type'] = 'left';

		// 	$join[1]['table_name'] = 'category';
		// 	$join[1]['column_name'] = 'category.category_id = product_manage.category_id';
		// 	$join[1]['type'] = 'left';

		// 	$join[2]['table_name'] = 'sub_category';
		// 	$join[2]['column_name'] = 'sub_category.sub_category_id = product_manage.sub_category_id';
		// 	$join[2]['type'] = 'left';
			
		// 	$tmp_data = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage','',$join,'','desc',$where,'','');

		// 	//================= create pagination start ===================//

		// 	$tmp_array['total_record'] = count($tmp_data);
		// 	$tmp_array['url'] = base_url('product/index/page/');
		// 	$tmp_array['per_page'] = RECORDS_PER_PAGE_FRONT;

		// 	$record = $this->Production_model->only_pagination($tmp_array);
		// 	// echo"<pre>"; print_r($record); exit;

		// 	$data['best_seller_details'] = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage','',$join,'','desc',$where,'','',$record['limit'],$record['start']);			
		// 	$data['pagination'] = $record['pagination'];

		// 	// echo"<pre>"; echo $this->db->last_query(); print_r($data['best_seller_details']); exit;

		// 	$data['cat_pro_details'] = array();	
		// 	$data['sub_cat_pro_details'] = array();	
	
		// 	$data['hot_deal_details'] = array();	

		// 	$this->load->view('product',$data);
		// }

		// function check_product_available(){

		// 	$product_id = $this->input->post('product_id'); 			
		// 	$get_pro_id = $this->Production_model->get_where_user('SUM(product_quantity) as pro_qty','tbl_order_mapping','','',array('product_id'=>$product_id));
			
		// 	$get_pro_qty = $this->Production_model->get_where_user('product_quantity','product_manage','','',array('product_id'=>$product_id)); // get all quantity for this product...
		// 	// echo"<pre>"; echo $this->db->last_query(); print_r($get_pro_id); exit;
			
		// 	$product_available = $get_pro_qty[0]['product_quantity']-$get_pro_id[0]['pro_qty']; // check product is available or not. 
		// 	if ($product_available >0) {
		// 		$response_array['success'] = true;
		// 		$response_array['stock'] = $product_available;
		// 	}
		// 	else{
		// 		$response_array['error'] = false;	
		// 		$response_array['stock'] = $product_available;				
		// 	}
		// 	echo json_encode($response_array);
		// }

		function filter()
		{	
			if($this->input->post()){
				$data [] = $this->input->post();

				$order_b='';
				$order_in='';
				if(!empty($data[0]['category_ids'])){
					$category_id = implode(',', $data[0]['category_ids']);
				}
				
				// $where = 'product_manage_attribute.final_price between '.$data[0]['min_price'].' and '.$data[0]['max_price']. ' and '.' product_manage.status= '."'0'". ' and ' .'product_manage_attribute.currency_id= '.CURRENCY;
				$where = 'product_manage.status= '."'1'";

				if(!empty($data[0]['category_ids'])){
					$where .=' and category`.`category_id in('.$category_id.')';
				}
				if($data[0]['sort_by']== 1 && !empty($data[0]['sort_in']) && $data[0]['sort_in']== 1){ // Name filter
					$order_in='asc';
				}
				if($data[0]['sort_by']== 1 && !empty($data[0]['sort_in']) && $data[0]['sort_in']== 2){ // Name filter
					$order_in='desc';
				}
				if($data[0]['sort_by']== 2 && !empty($data[0]['sort_in']) && $data[0]['sort_in']== 3){ // Price filter
					$order_in='asc';
				}
				if($data[0]['sort_by']== 2 && !empty($data[0]['sort_in']) && $data[0]['sort_in']== 4){ // Price filter
					$order_in='desc';
				}

				/*Name filter*/
				if($data[0]['sort_by']== 1 && !empty($data[0]['sort_by'])){
					$order_b='product_manage.product_name';
				}
				/*Price filter*/				
				// if($data[0]['sort_by']== 2 && !empty($data[0]['sort_by'])){
				// 	$order_b='product_manage_attribute.final_price';
				// }
			}
			// echo"<pre>"; print_r($data); exit;
			$pro_name_search = $this->input->post('pro_name_search');
			if (isset($pro_name_search) && $pro_name_search !=null) {
		 		$search['product_manage.product_name'] = $pro_name_search;
			}
			else{
				$search = '';
			}

			$join[0]['table_name'] = 'category';
			$join[0]['column_name'] = 'category.category_id = product_manage.category_id';
			$join[0]['type'] = 'left';

			$join[1]['table_name'] = 'sub_category';
			$join[1]['column_name'] = 'sub_category.sub_category_id = product_manage.sub_category_id';
			$join[1]['type'] = 'left';
			
			$tmp_data = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage',$search,$join,$order_b,$order_in,$where,'','');

			//================= create pagination start ===================//

			$tmp_array['total_record'] = count($tmp_data);
			$tmp_array['url'] = base_url('product/index/page/');			
			$tmp_array['per_page'] = RECORDS_PER_PAGE_FRONT;

			if(isset($data[0]['page_no']) && $data[0]['page_no'] != ''){
				$tmp_array['page_no'] = isset($data[0]['page_no']) ? $data[0]['page_no'] : '';
			}

			$record = $this->Production_model->only_pagination($tmp_array);
			// echo"<pre>"; print_r($record); exit;

			$filteredData = $this->Production_model->jointable_descending(array('product_manage.*','category.category_name','sub_category.sub_category_name'),'product_manage',$search,$join,$order_b,$order_in,$where,'','',$record['limit'],$record['start']);			

			$pagination= $record['pagination'];
			// echo"<pre>"; echo $this->db->last_query(); print_r($pagination); exit;
			
			ob_start();
			if (isset($filteredData) && !empty($filteredData) ) { 
				//print_r($filteredData);exit;
				
	    		foreach ($filteredData as $key => $value) {
		        	$id = $value['product_id'];
					$filteredProducts="";
					?>
						<ul class="products">
							<li class="product">
	                            <div class="product-container">
	                                <figure>
	                                    <div class="product-wrap">
	                                        <div class="product-images">
	                                            <div class="shop-loop-thumbnail shop-loop-front-thumbnail">
	                                                <a href="<?= base_url('product/product_details/'.$id)?>">
	                                                <img width="400" height="450" src="<?= base_url(PRO_MEDIUM_IMAGE.$value['product_image'])?>" alt="Product-1" />
	                                                </a>
	                                            </div>
	                                        </div>
	                                    </div>
	                                    <figcaption>
	                                        <div class="shop-loop-product-info">
	                                            <div class="info-title">
	                                                <h3 class="product_title">
	                                                    <a href="<?= base_url('product/product_details/'.$id)?>">
	                                                    <?= $value['product_name']?>
	                                                    </a>
	                                                </h3>
	                                            </div>
	                                            <div class="info-excerpt">
	                                                <?= substr($value['product_desc'],0,150)?>
	                                            </div>
	                                            <div class="list-info-meta clearfix">
	                                                <div class="list-action clearfix">
	                                                    <a href="<?= base_url('product/product_details/'.$id)?>"><button type="button" class="read-more btn btn-sm">Read More</button></a>
	                                                    <div class="clear"></div>
	                                                </div>
	                                            </div>
	                                        </div>
	                                    </figcaption>
	                                </figure>
	                            </div>
	                        </li>
                    	</ul>
		        	<?php
		        }		        
	    	}else{
                echo"<center><h4>Product Not Available...!</h4></center>";
	    	}
	    	?>
	    	<!-- <div class="col-12">
	            <div class="pagination-bar"> -->
	                <?php /*
	                    if(isset($pagination) && $pagination !=null){
	                        echo $pagination;
	                    } */
	                ?>
	            <!-- </div>
            </div> -->
            <?php
	    	echo ob_get_clean(); 
			//print_r($data);
		}

		function get_sub_category(){
			$id = $this->input->post('category_id');

			$sub_category_details = $this->Production_model->get_all_with_where('sub_category','sub_category_id','desc',array('category_id'=>$id));

			if ($sub_category_details !=null) {
                foreach ($sub_category_details as $key => $sub_cat_row) {
                    $subcat_id = $sub_cat_row['sub_category_id'];           
                ?>
                    <li>
                        <a href="<?= base_url('sub-category/'.$subcat_id)?>"><?= $sub_cat_row['sub_category_name']?></a>
                    </li>
                <?php }
            }
			// echo"<pre>"; print_r($data); exit;
		}
	}
	/* End of file Category.php */
	/* Location: ./application/controllers/Category.php */
?>