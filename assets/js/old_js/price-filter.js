/*Price range filter*/
jQuery('.slider-range-price').each(function() {
    var min = jQuery(this).data('min');
    var max = jQuery(this).data('max');
    var unit = jQuery(this).data('unit');
    var value_min = jQuery(this).data('value-min');
    var value_max = jQuery(this).data('value-max');
    var label_reasult = jQuery(this).attr('data-label-reasult');
    var t = jQuery(this);
    var result = label_reasult + " " + unit + value_min + ' - ' + unit +value_max
    jQuery(this).slider({
      range: true,
      min: min,
      max: max,
      values: [value_min, value_max],
      slide: function(event, ui) {
        var result = label_reasult + " " + unit + ui.values[0] + ' - ' + unit + ui.values[1];
        // console.log( ui.values[0] + ' - ' +ui.values[1] );        
        //t.closest('.slider-range').find('.amount-range-price').html(result);
        $('#amount').val(result);
      },
      stop: function( event, ui ) {
        // console.log( ui.values[0] + ' - ' +ui.values[1] );
        $('[name="slider-min"]').val(ui.values[0]);
        $('[name="slider-max"]').val(ui.values[1]);
        get_filtered_data();
      },
    });
    $('[name="slider-min"]').val(min);
    $('[name="slider-max"]').val(max);
    $('#amount').val(result);
});