function isemptyfocus(name)
{
    if ($('#' + name).val().trim().length <= 0)
    {
        $('#' + name).focus().css({
            'border-color': ' red',
            '-webkit-border-radius': '4px',
            '-moz-border-radius': '4px',
            'border-radius': '4px',
            '-webkit-box-shadow': '0px 0px 4px red',
            '-moz-box-shadow': '0px 0px 4px red',
            'box-shadow': '0px 0px 4px red'
        });
        return true;
    } else {
        return false;
    }
}

//empty value focus border become red
function requireandfocus(name)
{
    if ($('#' + name).val().trim().length <= 0) {
        $('#' + name).focus().addClass('text-error');
        return true;
    } else {
        $('#' + name).focus().removeClass('text-error');
        return false;
    }
}
function requireandmessage(name, message = 'Valid Value')
{
    if ($('#' + name).val().trim().length <= 0) {
        $('#' + name).focus();
        $('#' + name + '-error').html('Please Enter ' + message);
        return true;
    } else {
        $('#' + name + '-error').html('');
        return false;
}
}
//select empty
function isemptydropdown(name)
{
    if ($('#' + name).val() == 'Select' || $('#' + name).val() == '') {
        $('#' + name).focus().addClass('text-error');
        return true;
    } else {
        $('#' + name).focus().removeClass('text-error');
        return false;
    }
}
function isemptyselect(name, message = 'Option')
{
    if ($('#' + name).val() == 'Select' || $('#' + name).val() == '')
    {
        $('#' + name).focus();
        $('#' + name + '-error').html('Please Select Atleast One ' + message + '!');
        /*swal({title:"<strong style='color:#d9534f;'>Please Select Atleast One"+message+"!</strong>",
         html:true
         });*/
        return true;
    } else {
        $('#' + name + '-error').html('');
        return false;
}
}
//email
function isvalidemail(name, message = "Valid Email")
{
    if (!($('#' + name).val()).match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
        $('#' + name).focus();
        $('#' + name + '-error').html('Please Enter ' + message + '!');
        return true;
    } else {
        $('#' + name + '-error').html('');
        return false;
}
}
///////////////

function isvalidemailss(name)
{
    if (!($('#' + name).val()).match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/)) {
        $('#' + name).focus().css({
            'border-color': ' red',
            '-webkit-border-radius': '4px',
            '-moz-border-radius': '4px',
            'border-radius': '4px',
            '-webkit-box-shadow': '0px 0px 4px red',
            '-moz-box-shadow': '0px 0px 4px red',
            'box-shadow': '0px 0px 4px red'
        });
        return true;
    } else {
        return false;
    }
}

/////////////////

function isconfirmpassword(password, confirmpassword)
{
    if (($('#' + password).val()) != ($('#' + confirmpassword).val()))
    {
        $('#' + confirmpassword).focus();
        $('#' + name + '-error').html('Password Does Not Match Please Re-Enter Password!');
        return true;
    } else {
        $('#' + name + '-error').html('');
        return false;
    }
}
//Mobile Number with country code and 10 digits
function ismobile(name, message = "Valid Mobile Number")
{
    if (!($('#' + name).val()).match(/^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/)) {
        $('#' + name).focus();
        $('#' + name + '-error').html('Please Enter ' + message + '!');
        return true;
    } else {
        $('#' + name + '-error').html('');
        return false;
}
}
//Mobile Number only 10 digits 
function ismobile10(name, message = "Only 10 Digit")
{
    if (!($('#' + name).val()).match(/^([7-9][0-9]{9})$/)) {
        $('#' + name).focus();
        $('#' + name + '-error').html('Please Enter ' + message + '!');
        return true;
    } else {
        $('#' + name + '-error').html('');
        return false;
}
}
//Validate stdcode
function isstdno(name, message = 'Valid Std Code')
{
    if (!($('#' + name).val()).match(/^[0-9]\d{3,4}$/)) {
        $('#' + name).focus();
        $('#' + name + '-error').html('Please Enter ' + message + '!');
        return true;
    } else {
        $('#' + name + '-error').html('');
        return false;
}
}
//Validate Landline Number
function islandlineno(name, message = 'Valid Landline Number')
{
    if (!($('#' + name).val()).match(/^[0-9]\d{5,6}$/)) {
        $('#' + name).focus();
        $('#' + name + '-error').html('Please Enter ' + message + '!');
        return true;
    } else {
        $('#' + name + '-error').html('');
        return false;
}
}
// Validation for ip address
function isipaddress(name, message = 'Valid Ip')
{
    if (!($('#' + name).val()).match(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/)) {
        $('#' + name).focus();
        $('#' + name + '-error').html('Please Enter ' + message + '!');
        return true;
    } else {
        $('#' + name + '-error').html('');
        return false;
}
}
//****************************** On Key Press Validation ******************************************************************************
// it allows to press only numeric key
function onlynumberpress(name)
{
    $(document).on('keypress', '#' + name, function (e) {
        if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
            return false;
        }
    });
}
//Only Numberpress with dot,delete and backspace and left right arrow allowed
function onlynumberpressdot(name)
{
    $(document).on('keypress', '#' + name, function (evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode == 46 || charCode == 8 || charCode == 37 || charCode == 39 || charCode == 46) {//For Delete ,BackSpace & Dot
            return true;
        } else if (charCode > 31 && (charCode < 48 || charCode > 57)) {
            return false;
        }
        return true;
    });
}

// it allows to press only alphabat with special character key

function alfabet_with_special_char(name)
{
    $(document).on('keypress', '#' + name, function (event) {
        var inputValue = event.which;
        if (!(inputValue >= 58 && inputValue <= 127) && (inputValue < 10 || inputValue > 47) && inputValue != 8 && inputValue != 0) {
            event.preventDefault();
        }
    });
}


// it allows to press only numeric key
function onlyalfabetpress(name)
{
    $(document).on('keypress', '#' + name, function (event) {
        var inputValue = event.which;
        if (!(inputValue >= 65 && inputValue <= 122) && (inputValue != 32 && inputValue != 0) && inputValue != 8) {
            event.preventDefault();
        }
    });
}
//Only alfabet press with dot,delete and backspace and left right arrow allowed
function onlyalfabetpressarrow(name)
{
    $(document).on('keypress', '#' + name, function (evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if ((charCode > 64 && charCode < 91) || (charCode > 96 && charCode < 123) || charCode == 8 || charCode == 37 || charCode == 39 || charCode == 46)
            return true;
        else
            return false;
    });
}
// No Special Character Allowed On Keypress accept backspace
function nospecialchar(name)
{
    $(document).on('keypress', '#' + name, function (e) {
        if (e.which < 8 || (e.which > 10 && e.which < 48) || (e.which > 57 && e.which < 65) || (e.which > 90 && e.which < 97) || e.which > 122) {
            return false;
        }
    });
}
//on keypress uppercase
function converttouppercase(name)
{
    $(document).on('input', '#' + name, function (evt) {
        $(this).val(function (_, val) {
            return val.toUpperCase();
        });
    });
}
//on keypress lowercase
function converttolowercase(name)
{
    $(document).on('input', '#' + name, function (evt) {
        $(this).val(function (_, val) {
            return val.toLowerCase();
        });
    });
}
///File
function singlefile(name)
{
    var ext = $('#' + name).val().split('.').pop().toLowerCase();
    if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
        swal("Cancelled", "Invalid Extension !", "error");
        $(this).val('').clone();
        return false;
    }
}
function multiplefile(name)
{
    $(document).on('change', '#' + name, function () {
        for (var i = 0; i < $(this).get(0).files.length; ++i) {
            var ext = $(this).get(0).files[i].name.split('.').pop().toLowerCase();
            if ($.inArray(ext, ['gif', 'png', 'jpg', 'jpeg']) == -1) {
                swal("Cancelled", "Invalid Extension! :: " + ext, "error");
                $(this).val('').clone();
                return false;
            }
        }
    });
}
// auto remove bs-alert wich affected on alert calass
function autoremovealert()
{
    $(document).ready(function () {
        window.setTimeout(function () {
            $(".alert").fadeTo(1000, 0).slideUp(1000, function () {
                $(this).remove();
            });
        }, 6000);
    });
}
//*********************** AJAX Loader ********************************//
/* AJAX Loader Function */
/* 1st arguments:-
 var loader_image='<?php echo base_url('/assets/img/ajax-loader.gif'); ?>';
 2nd arguments:-
 if you want to put images then----- "var loader_text='<img src="<?= base_url('/assets/img/loding-text.gif') ?>">';
 if you want to put text then------- var loader_text="Loading...";
 */
function loadajexloader(loader_image, loader_text)
{
    function ajaxindicatorstart(text)
    {
        if ($('body').find('#resultLoading').attr('id') != 'resultLoading') {
            $('body').append('<div id="resultLoading" style="display:none"><div><img src="' + loader_image + '"><div>' + text + '</div></div><div class="bg"></div></div>');
        }
        $('#resultLoading').css({'width': '100%', 'height': '100%', 'position': 'fixed', 'z-index': '10000000', 'top': '0', 'left': '0', 'right': '0', 'bottom': '0', 'margin': 'auto'
        });
        $('#resultLoading .bg').css({'background': '#000000', 'opacity': '0.7', 'width': '100%', 'height': '100%', 'position': 'absolute', 'top': '0'
        });
        $('#resultLoading>div:first').css({'width': '250px', 'height': '75px', 'text-align': 'center', 'position': 'fixed', 'top': '0', 'left': '0', 'right': '0', 'bottom': '0', 'margin': 'auto', 'font-size': '16px', 'z-index': '10', 'color': '#ffffff'
        });
        $('#resultLoading .bg').height('100%');
        $('#resultLoading').fadeIn(300);
        $('body').css('cursor', 'wait');
    }
    function ajaxindicatorstop()
    {
        $('#resultLoading .bg').height('100%');
        $('#resultLoading').fadeOut(300);
        $('body').css('cursor', 'default');
    }
    $(document).ajaxStart(function () {
        //show ajax indicator
        ajaxindicatorstart(loader_text);
    }).ajaxStop(function () {
        //hide ajax indicator
        ajaxindicatorstop();
    });
    $.ajax({
        global: false,
        // ajax stuff
    });
}



// include new file //

$(".digits").keypress(function (e) { // accept (.)point value
    if (e.which != 8 && e.which != 0 && (e.which < 46 || e.which > 57)) {
        return false;
    }
});

$(".only_digits").keypress(function (e) { // not accept (.)point value
    if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        return false;
    }
});

$('.txtonly').bind('keypress', function (event) {
    var inputValue = event.which;
    if (!(inputValue >= 58 && inputValue <= 127) && (inputValue < 10 || inputValue > 47) && inputValue != 8 && inputValue != 0) {
        event.preventDefault();
    }
});

$("#user_email").keyup(function () {
    var email = $("#user_email").val();
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email)) {
        $("#valid").text(email + " is not a valid email");
        email.focus();
        return false;
    } else {
        $("#valid").text("");
    }
});

$("#subscribe_email").keyup(function () {
    var email = $("#subscribe_email").val();
    var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;

    if (!filter.test(email)) {
        $("#valid_subscribe").text(email + " is not a valid email");
        email.focus();
        return false;
    } else {
        $("#valid_subscribe").text("");
    }
});

/*Mobile number formate*/
$('.number_formate').keyup(function () {
    $(this).val($(this).val().replace(/(\d{3})(\d{4})(\d{3})/, "$1-$2-$3"))
});

/*Debit card number formate*/
$('.debit_number').keyup(function () {
    $(this).val($(this).val().replace(/(\d{4})(\d{4})(\d{4})(\d{4})/, "$1 $2 $3 $4"))
});

$('.view-problem').click(function () {
    $(this).closest('tr').next('tr').fadeToggle();
});

$('.view-reply').click(function () {
    $(this).closest('tr').next('tr').toggle('slow');
});

$('.username').keypress(function (e) {
    if (e.which === 32) {
        return false;
    }
});

$('.refresh-page').click(function (e) {
    window.location = window.location.href;
});