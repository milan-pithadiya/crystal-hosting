<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Subcategory extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function index($page_number = "") {
        $settings = array(
            "url" => site_url() . "authority/subcategory/index/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "*",'ORDER BY'=>array('sub_category_id'=>'DESC'));
        $data = $this->common_model->get_pagination("sub_category", $conditions, $settings);
        if (isset($this->session->category_msg) && $this->session->category_msg != '') {
            $data = array_merge($data, array("success" => $this->session->category_msg));
            $this->session->category_msg = '';
        }
        // echo"<pre>"; print_r($data); exit;
        unset($settings, $conditions);
        $this->load->view('authority/sub_category/view', $data);
    }

    function add()
    {
        $data['category_details'] = $this->Production_model->get_all_with_where('category','','',array('status'=>'1'));
        $data['sub_category_details'] = [];
        $this->load->view('authority/sub_category/add-edit',$data);
    }

    // ==================================================================== //
    // ================= excel formate download && insert ================= //
    // ==================================================================== //

    // public function download_excel_category(){

    //     // At the beggining...
    //     ob_start();    
    //     $content="";
    //     $normalout=true;

    //     // ... do some stuff ...

    //     // i guess if some condition is true...
    //     $file_name = 'category.xls';
    //     $content=ob_get_clean();
    //     $normalout=false;
    //     header( "Content-Type: application/vnd.ms-excel" );
    //     header( "Content-disposition: attachment; filename=".$file_name);
    //     echo 'sub category name';
    //     // echo 'First Name' . "\t" . 'Last Name' . "\t" . 'Phone' . "\n";
    //     // echo 'John' . "\t" . 'Doe' . "\t" . '555-5555' . "\n";


    //     // Here you could die() or continue...
    //     ob_start();
    //     // ... rest of execution ...

    //     $content.=ob_get_clean();
    //     if($normalout)
    //     {
    //         echo($content);
    //     } else {
    //           // Excel provided no output.
    //     }
    // }

    // public function save_excel_category()
    // {  
    //     $this->load->library('excel');
    //     if ($_FILES['xls_file']['error'] == 0) {

    //         $ext = pathinfo($_FILES['xls_file']['name'], PATHINFO_EXTENSION);
           
    //         $imagePath = SUB_CATEGORY_EXCEL;
    //         $img_name = 'category_excel-'.rand(111,999)."-".str_replace(' ', '_',$_FILES['xls_file']['name']);

    //         $destFile = $imagePath . $img_name; 
    //         $filename = $_FILES["xls_file"]["tmp_name"];       
    //         move_uploaded_file($filename, $destFile);
            
    //         if(is_array($img_name)){
    //             $this->session->set_flashdata('phperror','File Not Uploaded.!!!'.$img_name['error']);
    //             redirect($_SERVER['HTTP_REFERER']);
    //         }

    //         $path = SUB_CATEGORY_EXCEL;
    //         $data['FileName'] = $img_name;
    //         $data['FieldsList'] =array("sub_category_name");

    //         $excel_data = $this->excel->import_excel($path,$data);

    //         // echo '<pre>'; print_r($excel_data); exit;
    //         $resultSet = Array();
            
    //         //======== check duplicate value get in excel sheet data start ========//

    //         $tmp_resultSet = Array(); 
    //         $duplicates = Array(); 
    //         foreach ($excel_data as $key => $dt) {

    //             if (in_array($dt, $tmp_resultSet)) {
    //                 $duplicates[] = $dt;
    //             }
    //             $tmp_resultSet[] = $dt;
    //         }
    //         $tmp = $duplicates;
    //         $duplicate_cat_name = [];
    //         if ($tmp !=null) {
    //             foreach ($tmp as $key => $duplicate_row){
    //                 $tmp = $duplicate_row['sub_category_name'];
    //                 array_push($duplicate_cat_name,$tmp);
    //             }
    //             if (!empty($duplicate_cat_name)){
    //                 $error_cat_name = implode(', ', array_unique($duplicate_cat_name));

    //                 // echo"<pre>"; print_r(array_unique($duplicate_cat_name)); exit;
    //                 $this->session->set_flashdata('error',"$error_cat_name Duplicate category name...!");
    //                 redirect(base_url('authority/subcategory/add'));
    //             }
    //         }
               
    //         //========= check duplicate value get in excel sheet data end =========//

    //         foreach ($excel_data as $key => $dt) {
    //             $get_cat_name = $this->Production_model->get_all_with_where('sub_category','','',array('category_id'=>$this->input->post('category_id'),'sub_category_name'=>$dt['sub_category_name']));

    //             if(!empty($get_cat_name)) 
    //             {
    //                 $resultSet[] = $dt['sub_category_name'];
    //                 // echo"<pre>"; print_r($resultSet);
    //             }
    //         }

    //         if (!empty($resultSet)) {
    //             // echo"<pre>"; print_r($resultSet); exit;

    //             $error = implode(', ', $resultSet);

    //             $this->session->set_flashdata('error',"$error Sub category name is allredy exist...!");
    //             redirect(base_url('authority/subcategory/add'));
    //         }
    //         else
    //         {
    //             foreach ($excel_data as $key => $dt) {
    //                 $ins = array(
    //                     'category_id' => $this->input->post('category_id'),
    //                     'sub_category_name'=>$dt['sub_category_name'],
    //                     'create_date' => date('Y-m-d H:i:s')
    //                 ); //insert Array

    //                 $this->Production_model->insert_record('sub_category',$ins);//Insert Table Code
    //             }
    //         }

    //         $this->session->set_flashdata('success','Excel Imported.!!!');
    //         redirect(base_url('authority/subcategory/view'));

    //         // echo '<pre>'; print_r($excel_data); exit;
    //     } else {
    //         $this->session->set_flashdata('error','File Not Uploaded.!!!');
    //         redirect($_SERVER['HTTP_REFERER']);
    //     }
    // }

    // ==================================================================== //
    // =========================== End of this ============================ //
    // ==================================================================== //

    function insert_sub_category()
    {
        // $data = $this->input->post();
        // $data['create_date'] = date('Y-m-d H:i:s');
        // echo"<pre>"; print_r($data); exit;

        $this->form_validation->set_rules('category_id', 'category name', 'required');
        // $this->form_validation->set_rules('sub_category_name', 'sub category name', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER['HTTP_REFERER']); 
        }
        else
        {
            $category_id = $this->input->post('category_id');
            $sub_cat_name = $this->input->post('sub_category_name');
            $count_sub_category = count($this->input->post('sub_category_name')); 
            $create_date = date('Y-m-d H:i:s');

            // $get_cat_name = $this->Production_model->get_all_with_where('sub_category','','',array('category_id'=>$category_id,'sub_category_name'=>$sub_category_name));
            // echo "<pre>"; print_r($sub_category_name); exit;
            
            // if (count($get_cat_name) > 0) {
            //     $this->session->set_flashdata('error', 'Category name Allredy Exicuted....!');
            //     redirect($_SERVER['HTTP_REFERER']);
            // }
            // else
            // {
                $resultSet = Array();                 
                if (isset($count_sub_category)) {     
                    for ($i = 0; $i < $count_sub_category; $i++) {

                        $get_sub_cat_name = $this->Production_model->get_all_with_where('sub_category','','',array('category_id'=>$category_id,'sub_category_name'=> $sub_cat_name[$i]));

                        if(!empty($get_sub_cat_name)) 
                        {
                            $resultSet[] = $sub_cat_name[$i];
                            // echo"<pre>"; print_r($resultSet);
                        }
                    }
                    if (!empty($resultSet)) {
                        // echo"<pre>"; print_r($resultSet); exit;

                        $error = implode(', ', $resultSet);

                        $this->session->set_flashdata('error',"$error Category name is allredy exist...!");
                        redirect(base_url('authority/subcategory/add'));
                    }
                    // echo"<pre>"; print_r($resultSet); exit;
                    else
                    {                           
                        for ($i = 0; $i < $count_sub_category; $i++) {
                            // if(!is_dir(SUB_CAT_ICON)){
                            //     mkdir(SUB_CAT_ICON);                
                            //     @chmod(SUB_CAT_ICON,0777);
                            // }
                            /*category icon*/
                            // if($_FILES['sub_category_icon']['name'][$i]==""){ $icon_name='';}
                            // else{
                            //     $iconPath = SUB_CAT_ICON;                               
                            //     $icon_name = 'CAT-ICON'.rand(111,999)."-".str_replace(' ', '_',$_FILES['sub_category_icon']['name'][$i]);
                            //     $destFile = $iconPath . $icon_name; 
                            //     $filename = $_FILES["sub_category_icon"]["tmp_name"][$i];       
                            //     move_uploaded_file($filename,  $destFile);                                
                            //     // $data['sub_category_icon'] = $icon_name;
                            // }

                            $data = array(
                                'category_id' => $category_id,
                                'sub_category_name' => $sub_cat_name[$i],
                                // 'sub_category_icon' => $icon_name,
                                'create_date' => $create_date
                            );

                            if ($sub_cat_name[$i] !=null) {
                                // echo"<pre> insert"; print_r($data); 
                                $record = $this->Production_model->insert_record('sub_category',$data);
                            }
                        }
                    }
                   // exit;
                }
                if ($record !='') {
                    $this->session->set_flashdata('success', 'Category Add Successfully....!');
                    redirect(base_url('authority/subcategory'));
                }
                else
                {
                    $this->session->set_flashdata('error', 'Category Not Added....!');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            // }   
        }   
    }

    function edit($id)
    {
        $data['category_details'] = $this->Production_model->get_all_with_where('category','','',array('status'=>'1'));
        $data['sub_category_details'] = $this->Production_model->get_all_with_where('sub_category','','',array('status'=>'1','sub_category_id'=>$id));
        // echo"<pre>"; print_r($data); exit;
        $this->load->view('authority/sub_category/add-edit',$data);
    }

    function update_sub_category()
    {
        $sub_category_id = $this->input->post('sub_category_id');
        
        $category_id = $this->input->post('category_id');
        $sub_cat_name = $this->input->post('sub_category_name');
        $modified_date = date('Y-m-d H:i:s');

        // $data = $this->input->post();
        // echo"<pre>"; print_r($data); exit;

        $this->form_validation->set_rules('category_id', 'category name', 'required');
        // $this->form_validation->set_rules('sub_category_name', 'sub category name', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER['HTTP_REFERER']); 
        }
        else
        {
            $data = array(
                'category_id' => $category_id,
                'sub_category_name' => $sub_cat_name[0],
                'modified_date' => $modified_date
            );

            $get_image = $this->Production_model->get_all_with_where('sub_category','','',array('sub_category_id'=>$sub_category_id));
            // echo"<pre>"; print_r($get_image); exit;

            /*category icon */
            // if($_FILES['sub_category_icon']['name'] !='')
            // {$iconPath = SUB_CAT_ICON;
              
            //       if(!is_dir($iconPath)){
            //         mkdir($iconPath);                
            //         @chmod($iconPath,0777);
            //     }
            //     /*Old image delete*/
            //     if ($get_image !=null && $get_image[0]['sub_category_icon'] !=null && !empty($get_image[0]['sub_category_icon']))
            //     {
            //         @unlink(SUB_CAT_ICON.$get_image[0]['sub_category_icon']);
            //     }
            //     $icon_name = 'CAT-ICON'.rand(111,999)."-".str_replace(' ', '_',$_FILES['sub_category_icon']['name'][0]);
            //     $destFile = $iconPath . $icon_name; 
            //     $filename = $_FILES["sub_category_icon"]["tmp_name"][0];       
            //     move_uploaded_file($filename,  $destFile);

            //     $data['sub_category_icon'] = $icon_name;
            // }
            // echo"<pre>"; print_r($data); exit;
            $record = $this->Production_model->update_record('sub_category',$data,array('sub_category_id'=>$sub_category_id));
            // echo"<pre>"; echo $this->db->last_query(); print_r($record); exit;
            if ($record == 1) {
                $this->session->set_flashdata('success', 'Category Update Successfully....!');
                redirect(base_url('authority/subcategory'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Category Not Updated....!');
                redirect($_SERVER['HTTP_REFERER']);
            }   
        }
    }

    function delete_category($id)
    {
        /*Old image delete*/
        // $get_image = $this->Production_model->get_all_with_where('sub_category','','',array('sub_category_id'=>$id));
        
        // if ($get_image !=null && $get_image[0]['sub_category_icon'] !=null && !empty($get_image[0]['sub_category_icon']))
        // {
        //    @unlink(SUB_CAT_ICON.$get_image[0]['sub_category_icon']);
        // }
        $record = $this->Production_model->delete_record('sub_category',array('sub_category_id'=>$id));
      
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Category Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Category Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            // $get_image = $this->Production_model->get_all_with_where('sub_category','','',array('sub_category_id'=>$value));
  
            // if ($get_image !=null && !empty($get_image[0]['sub_category_icon']))
            // {
            //     @unlink(SUB_CAT_ICON.$get_image[0]['sub_category_icon']);
            // }
            $record = $this->Production_model->delete_record('sub_category',array('sub_category_id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Category Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Category Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>