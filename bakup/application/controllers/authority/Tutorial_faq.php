<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Tutorial_faq extends CI_Controller {
    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }
    public function index($page_number='') {
        $settings = array(
            "url" => site_url() . "authority/tutorial_faq/index/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "*",'ORDER BY'=>array('id'=>'DESC'));
        $data = $this->common_model->get_pagination("tutorial_faq", $conditions, $settings);
        if (isset($this->session->tutorial_msg) && $this->session->tutorial_msg != '') {
            $data = array_merge($data, array("success" => $this->session->tutorial_msg));
            $this->session->tutorial_msg = '';
        }
        unset($settings, $conditions);        
        $this->load->view('authority/tutorial_faq/view', $data);
    }
    function add()
    {
        $data['tutorial_faq_details'] = array();       
        $this->load->view('authority/tutorial_faq/add-edit',$data);
    }    
    function insert_tutorial_faq()
    {
        $data = $this->input->post();
        $title = $data['title']; 
        $description = $data['description'];
        $count_tutorial_faq = count($data['title']); 
        $create_date = date('Y-m-d H:i:s');
        $resultSet = Array();                 
        if(isset($count_tutorial_faq)) {     
            for($i = 0; $i < $count_tutorial_faq; $i++){
                $get_sub_title = $this->Production_model->get_all_with_where('tutorial_faq','','',array('title'=> $title[$i]));

                if(!empty($get_sub_title)) 
                {
                    $resultSet[] = $title[$i];
                }
            }
            if(!empty($resultSet)) {
                $error = implode(', ', $resultSet);
                $this->session->set_flashdata('error',"$error Tutorial faq title is allredy exist...!");
                redirect(base_url('authority/tutorial_faq/add'));
            }else{                           
                for($i = 0; $i < $count_tutorial_faq; $i++) {
                    $data = array(
                        'title' => $title[$i],
                        'description' => $description[$i],
                        'create_date' => $create_date
                    );
                    if($title[$i] !=null) {
                        $record = $this->Production_model->insert_record('tutorial_faq',$data);
                    }
                }
                if($record !='') {
                    $this->session->set_flashdata('success', 'Tutorial Add Successfully....!');
                    redirect(base_url('authority/tutorial_faq'));
                }else{
                    $this->session->set_flashdata('error', 'Tutorial Not Added....!');
                    redirect($_SERVER['HTTP_REFERER']);
                }
            }
        }
    }

    function edit($id)
    {
        $data['tutorial_faq_details'] = $this->Production_model->get_all_with_where('tutorial_faq','','',array('id'=>$id));
        $this->load->view('authority/tutorial_faq/add-edit',$data);
    }
    function update_tutorial_faq()
    {
        $data = $this->input->post();
        $title = $data['title']; 
        $description = $data['description']; 
        $modified_date = date('Y-m-d H:i:s');
        $id = $this->input->post('id');        
        $get_image = $this->Production_model->get_all_with_where('tutorial_faq','','',array('id'=>$id));
        $data = array(
            'title' => $title[0],
            'description' => $description[0],
            'modified_date' => $modified_date
        );
        $record = $this->Production_model->update_record('tutorial_faq',$data,array('id'=>$id));
        if ($record == 1) {
            $this->session->set_flashdata('success', 'Tutorial faq Update Successfully....');
            redirect(base_url('authority/tutorial_faq'));
        }else{
            $this->session->set_flashdata('error', 'Tutorial faq Not Updated....');
            redirect($_SERVER['HTTP_REFERER']);
        }        
    }
    function delete_tutorial_faq($id)
    {        
        $record = $this->Production_model->delete_record('tutorial_faq',array('id'=>$id));
      
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Tutorial faq Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Tutorial faq Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }
    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            $record = $this->Production_model->delete_record('tutorial_faq',array('id'=>$value));
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Tutorial faq Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            $this->session->set_flashdata('error', 'Tutorial faq Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  
}
?>