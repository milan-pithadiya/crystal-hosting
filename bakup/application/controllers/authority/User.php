<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
        $this->load->library('form_validation');
        $this->load->library('common_functions');
    }

    public function view($page_number = "") {
        $settings = array(
            "url" => site_url() . "authority/user/view/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "id,full_name,email_address,mobile_number_1");
        $data = $this->common_model->get_pagination("user", $conditions, $settings);
        if (isset($this->session->user_msg)) {
            $data = array_merge($data, array("success" => $this->session->user_msg));
            $this->session->unset_userdata('user_msg');
        }
        unset($settings, $conditions);
        $this->load->view('authority/user/view', $data);
    }

    public function edit($id = "") {
        $this->load->helper("form");
        $data = array("form_title" => "Edit Profile");
        if ($id != "") {
            $conditions = array("where" => array("id" => $id));
            $user_info = $this->common_model->select_data("user", $conditions);
            if ($user_info['row_count'] > 0) {
                $data = array_merge($data, $user_info['data'][0]);
            } else {
                redirect("authority/user/view");
            }
        } else {
            redirect("authority/user/view");
        }

        /* Form Validation */
        $this->form_validation->set_rules('full_name', 'full name', 'required', array('required' => 'Please enter user name'));
        $this->form_validation->set_rules('email_address', 'email addres', 'required|valid_email|is_unique_with_except_record[user.email_address.id.' . $data['id'] . ']', array('required' => 'Please enter email address', "valid_email" => "Please enter valid email address", "is_unique_with_except_record" => "This email is already available"));
        $this->form_validation->set_rules('mobile_number_1', 'mobile number', 'required|min_length[10]', array('required' => 'Please enter mobile number'));
        $this->form_validation->set_rules('mobile_number_2', 'mobile number', 'min_length[10]');

        if ($this->form_validation->run() === FALSE) {
            $data = array_merge($data, $_POST);
        } else {
            $error = false;
            $records = $_POST;

            if (isset($_FILES['profile_photo']) && $_FILES['profile_photo']['name'] != "") {
                $config['upload_path'] = './assets/uploads/profile_photo/';
                $config['allowed_types'] = 'gif|jpg|png|jpeg|"';                
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('profile_photo')) {
                    $error_msg = array('error' => $this->upload->display_errors());
                    $error = true;
                } else {
                    /* delete old photo */
                    if (!is_null($data['profile_photo'])) {
                        $path = $config['upload_path'] . $data['profile_photo'];
                        if (is_file($path)) {
                            @unlink($path);
                        }
                    }
                    $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
                    $file_name = $upload_data['file_name'];
                    chmod($config['upload_path'] . $file_name, 0777);
                    $records["profile_photo"] = $file_name;
                }
            } else {
                $records["profile_photo"] = $data['profile_photo'];
            }
            /*footer logo*/
            // if (isset($_FILES['footer_logo']) && $_FILES['footer_logo']['name'] != "") {
            //     $config['upload_path'] = './assets/uploads/profile_photo/';
            //     $config['allowed_types'] = 'gif|jpg|png|jpeg|"';                
            //     $this->load->library('upload', $config);
            //     if (!$this->upload->do_upload('footer_logo')) {
            //         $error_msg = array('error' => $this->upload->display_errors());
            //         $error = true;
            //     } else {
            //         /* delete old photo */
            //         if (!is_null($data['footer_logo'])) {
            //             $path = $config['upload_path'] . $data['footer_logo'];
            //             if (is_file($path)) {
            //                 @unlink($path);
            //             }
            //         }
            //         $upload_data = $this->upload->data(); //Returns array of containing all of the data related to the file you uploaded.
            //         $file_name = $upload_data['file_name'];
            //         chmod($config['upload_path'] . $file_name, 0777);
            //         $records["footer_logo"] = $file_name;
            //     }
            // } else {
            //     $records["footer_logo"] = $data['footer_logo'];
            // }
            // echo"<pre>"; print_r($records); exit;
            if (!$error) {
                $conditions = array(
                    "where" => array("id" => $data['id']),
                );
                $this->common_model->update_data("user", $records, $conditions);
                if ($data['id'] == $this->session->user_info['id']) {
                    $this->common_functions->updatesession($data['id']);
                }
                $data = array_merge($data, $_POST);
                $data = array_merge($data, array("success" => "Record updated successfully"));
                $_POST = array();
                $data = array_merge($data, $_POST);
            }
        }
        // echo"<pre>"; print_r($data); exit;
        $this->load->view('authority/user/add-edit', $data);
    }

    public function delete($id = "") {
        if ($id != "") {
            $conditions = array("select" => "id,profile_photo", "where" => array("id" => intval($id)));
            $user = $this->common_model->select_data("user", $conditions);
            if ($user['row_count'] > 0) {
                $photo = $user['data'][0]['profile_photo'];
                if (!is_null($photo)) {
                    $path = "./uploads/profile_photo/" . $photo;
                    if (is_file($path)) {
                        @unlink($path);
                    }
                }
            }
            $conditions = array(
                "where" => array("id" => $id),
            );
            $this->common_model->delete_data("user", $conditions);
            $this->session->user_msg = "Record deleted successfully";
            redirect("authority/user/view");
        } else {
            redirect("authority/user/view");
        }
    }

    public function details($id = "") {
        $data = array();
        if ($id != "") {
            $conditions = array("where" => array("id" => $id));
            $user_info = $this->common_model->select_data("user", $conditions);
            if ($user_info['row_count'] > 0) {
                $data = array_merge($data, $user_info['data'][0]);
            } else {
                redirect("authority/user/view");
            }
        } else {
            redirect("authority/user/view");
        }
        $this->load->view('authority/user/details', $data);
    }

}
