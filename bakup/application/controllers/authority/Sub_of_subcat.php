<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Sub_of_subcat extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('login_check_model');
    }

    public function index($page_number = "") {
        $settings = array(
            "url" => site_url() . "authority/sub_of_subcat/view/",
            "per_page" => RECORDS_PER_PAGE,
        );
        $conditions = array("select" => "sub_of_subcat_id,sub_category_id,category_id,sub_of_subcat_name,create_date,modified_date,status",'ORDER BY'=>array('sub_of_subcat_id'=>'DESC'));

        $data = $this->common_model->get_pagination("sub_of_sub_category", $conditions, $settings);
        if (isset($this->session->sub_of_cat_msg) && $this->session->sub_of_cat_msg != '') {
            $data = array_merge($data, array("success" => $this->session->sub_of_cat_msg));
            $this->session->sub_of_cat_msg = '';
        }
        // echo"<pre>"; print_r($data); exit;
        unset($settings, $conditions);
        $this->load->view('authority/sub_of_subcat/view', $data);
    }

    function add()
    {
        $data['category_details'] = $this->Production_model->get_all_with_where('category','','',array('status'=>'1'));

        $data['sub_of_subcat_details'] = array();
        $this->load->view('authority/sub_of_subcat/add-edit',$data);
    }

    // ==================================================================== //
    // ================= excel formate download && insert ================= //
    // ==================================================================== //

    // public function download_excel_sub_of_subcat(){

    //     // At the beggining...
    //     ob_start();    
    //     $content="";
    //     $normalout=true;

    //     // ... do some stuff ...

    //     // i guess if some condition is true...
    //     $file_name = 'sub_of_sub_category.xls';
    //     $content=ob_get_clean();
    //     $normalout=false;
    //     header( "Content-Type: application/vnd.ms-excel" );
    //     header( "Content-disposition: attachment; filename=".$file_name);
    //     echo 'sub_of_sub_category name';
    //     // echo 'First Name' . "\t" . 'Last Name' . "\t" . 'Phone' . "\n";
    //     // echo 'John' . "\t" . 'Doe' . "\t" . '555-5555' . "\n";


    //     // Here you could die() or continue...
    //     ob_start();
    //     // ... rest of execution ...

    //     $content.=ob_get_clean();
    //     if($normalout)
    //     {
    //         echo($content);
    //     } else {
    //           // Excel provided no output.
    //     }
    // }

    // public function save_excel_sub_of_subcat()
    // {  
    //     $this->load->library('excel');
    //     if ($_FILES['xls_file']['error'] == 0) {

    //         $ext = pathinfo($_FILES['xls_file']['name'], PATHINFO_EXTENSION);
           
    //         $imagePath = TOPIC_EXCEL;
    //         $img_name = 'sub_of_sub_category_excel-'.rand(111,999)."-".str_replace(' ', '_',$_FILES['xls_file']['name']);

    //         $destFile = $imagePath . $img_name; 
    //         $filename = $_FILES["xls_file"]["tmp_name"];       
    //         move_uploaded_file($filename, $destFile);
            
    //         if(is_array($img_name)){
    //             $this->session->set_flashdata('phperror','File Not Uploaded.!!!'.$img_name['error']);
    //             redirect($_SERVER['HTTP_REFERER']);
    //         }

    //         $path = TOPIC_EXCEL;
    //         $data['FileName'] = $img_name;
    //         $data['FieldsList'] =array("sub_of_sub_category_name");

    //         $excel_data = $this->excel->import_excel($path,$data);

    //         // echo '<pre>'; print_r($excel_data); exit;
    //         $resultSet = Array();
            
    //         //======== check duplicate value get in excel sheet data start ========//

    //         $tmp_resultSet = Array(); 
    //         $duplicates = Array(); 
    //         foreach ($excel_data as $key => $dt) {

    //             if (in_array($dt, $tmp_resultSet)) {
    //                 $duplicates[] = $dt;
    //             }
    //             $tmp_resultSet[] = $dt;
    //         }
    //         $tmp = $duplicates;
    //         $duplicate_cat_name = [];
    //         if ($tmp !=null) {
    //             foreach ($tmp as $key => $duplicate_row){
    //                 $tmp = $duplicate_row['sub_of_subcat_name'];
    //                 array_push($duplicate_cat_name,$tmp);
    //             }
    //             if (!empty($duplicate_cat_name)){
    //                 $error_cat_name = implode(', ', array_unique($duplicate_cat_name));

    //                 // echo"<pre>"; print_r(array_unique($duplicate_cat_name)); exit;
    //                 $this->session->set_flashdata('error',"$error_cat_name Duplicate category name...!");
    //                 redirect(base_url('authority/sub_of_subcat/add'));
    //             }
    //         }
               
    //         //========= check duplicate value get in excel sheet data end =========//
            
    //         foreach ($excel_data as $key => $dt) {
    //             $get_cat_name = $this->Production_model->get_all_with_where('sub_of_sub_category','','',array('category_id'=>$this->input->post('category_id'),'sub_category_id'=>$this->input->post('sub_category_id'),'sub_of_subcat_name'=>$dt['sub_of_subcat_name']));

    //             if(!empty($get_cat_name)) 
    //             {
    //                 $resultSet[] = $dt['sub_of_subcat_name'];
    //                 // echo"<pre>"; print_r($resultSet);
    //             }
    //         }

    //         if (!empty($resultSet)) {
    //             // echo"<pre>"; print_r($resultSet); exit;

    //             $error = implode(', ', $resultSet);

    //             $this->session->set_flashdata('error',"$error Name is allredy exist...!");
    //             redirect(base_url('authority/sub_of_subcat/add'));
    //         }
    //         else
    //         {
    //             foreach ($excel_data as $key => $dt) {
    //                 $ins = array(
    //                     'category_id' => $this->input->post('category_id'),
    //                     'sub_category_id'=> $this->input->post('sub_category_id'),
    //                     'sub_of_subcat_name'=> $dt['sub_of_subcat_name'],
    //                     'create_date' => date('Y-m-d H:i:s')
    //                 ); //insert Array

    //                 $this->Production_model->insert_record('sub_of_sub_category',$ins);//Insert Table Code
    //             }
    //         }

    //         $this->session->set_flashdata('success','Excel Imported.!!!');
    //         redirect(base_url('authority/sub_of_subcat/view'));

    //         // echo '<pre>'; print_r($excel_data); exit;
    //     } else {
    //         $this->session->set_flashdata('error','File Not Uploaded.!!!');
    //         redirect($_SERVER['HTTP_REFERER']);
    //     }
    // }

    // ==================================================================== //
    // =========================== End of this ============================ //
    // ==================================================================== //

    function insert_sub_of_subcat()
    {
        $this->form_validation->set_rules('category_id', 'category name', 'required');
        // $this->form_validation->set_rules('sub_category_id', 'sub category name', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER['HTTP_REFERER']); 
        }
        else
        {
            $category_id = $this->input->post('category_id');
            $sub_category_id = $this->input->post('sub_category_id');
            $subcat_name = $this->input->post('sub_of_subcat_name');
            $count_sub_of_subcat = count($this->input->post('sub_of_subcat_name')); 
            $create_date = date('Y-m-d H:i:s');
            
            $resultSet = Array();                 
            if (isset($count_sub_of_subcat)) {     
                for ($i = 0; $i < $count_sub_of_subcat; $i++) {

                    $get_sub_of_subcat_name = $this->Production_model->get_all_with_where('sub_of_sub_category','','',array('category_id'=>$category_id,'sub_category_id'=>$sub_category_id,'sub_of_subcat_name'=> $subcat_name[$i]));

                    if(!empty($get_sub_of_subcat_name)) 
                    {
                        $resultSet[] = $subcat_name[$i];
                        // echo"<pre>"; print_r($resultSet);
                    }
                }
                if (!empty($resultSet)) {
                    // echo"<pre>"; print_r($resultSet); exit;

                    $error = implode(', ', $resultSet);

                    $this->session->set_flashdata('error',"$error Name is allredy exist...!");
                    redirect(base_url('authority/sub_category/add'));
                }
                // echo"<pre>"; print_r($resultSet); exit;
                else
                {
                    for ($i = 0; $i < $count_sub_of_subcat; $i++) {
                        $data = array(
                            'category_id' => $category_id,
                            'sub_category_id' => $sub_category_id,
                            'sub_of_subcat_name' => $subcat_name[$i],
                            'create_date' => $create_date
                        );
                        if ($subcat_name[$i] !=null) {
                            // echo"<pre> insert"; print_r($data); 
                            $record = $this->Production_model->insert_record('sub_of_sub_category',$data);
                        }
                    }
                }
               // exit;
            }
            if ($record !='') {
                $this->session->set_flashdata('success', 'Category Add Successfully....!');
                redirect(base_url('authority/sub_of_subcat'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Category Not Added....!');
                redirect($_SERVER['HTTP_REFERER']);
            } 
        }   
    }

    function edit($id)
    {
        $data['category_details'] = $this->Production_model->get_all_with_where('category','','',array('status'=>'1'));
        $data['sub_of_subcat_details'] = $this->Production_model->get_all_with_where('sub_of_sub_category','','',array('sub_of_subcat_id'=>$id));

        $this->load->view('authority/sub_of_subcat/add-edit',$data);
    }

    function update_sub_of_subcat()
    {
        $category_id = $this->input->post('category_id');
        $sub_category_id = $this->input->post('sub_category_id');
        $sub_of_subcat_id = $this->input->post('sub_of_subcat_id');

        $subcat_name = $this->input->post('sub_of_subcat_name');

        $modified_date = date('Y-m-d H:i:s');
        // exit;
        // $data = $this->input->post();
        // echo"<pre>"; print_r($data); exit;

        $this->form_validation->set_rules('category_id', 'category name', 'required');
        $this->form_validation->set_rules('sub_category_id', 'sub category name', 'required');
        // $this->form_validation->set_rules('sub_category_name', 'sub category name', 'required');

        if ($this->form_validation->run() == FALSE)
        {
            $this->session->set_flashdata('error', validation_errors());
            redirect($_SERVER['HTTP_REFERER']); 
        }
        else
        {
            $data = array(
                'category_id' => $category_id,
                'sub_category_id' => $sub_category_id,
                'sub_of_subcat_name' => $subcat_name[0],
                'modified_date' => $modified_date
            );

            $record = $this->Production_model->update_record('sub_of_sub_category',$data,array('sub_of_subcat_id'=>$sub_of_subcat_id));
            // echo"<pre>"; print_r($record); exit;
            if ($record == 1) {
                $this->session->set_flashdata('success', 'Category Updated Successfully....!');
                redirect(base_url('authority/sub_of_subcat'));
            }
            else
            {
                $this->session->set_flashdata('error', 'Category Not Updated....!');
                redirect($_SERVER['HTTP_REFERER']);
            }   
        }
    }

    function delete_sub_of_subcat($id)
    {
        $record = $this->Production_model->delete_record('sub_of_sub_category',array('sub_of_subcat_id'=>$id));
        // $record = $this->Production_model->delete_record('product_manage',array('sub_of_subcat_id'=>$id)); 

        if ($record != 0) {
            $this->session->set_flashdata('success', 'Category Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Category Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function multiple_delete()
    {
        $chkbox_id = $this->input->post('chk_multi_checkbox');
        foreach ($chkbox_id as $key => $value) {
            // $get_image = $this->Production_model->get_all_with_where('home_slider','','',array('id'=>$value));
  
            // if ($get_image !=null && !empty($get_image[0]['slider_image']))
            // {
            //     unlink(HOME_SLIDER.$get_image[0]['slider_image']);
            //     unlink(HOME_SLIDER.'thumbnail/medium/'.$get_image[0]['slider_image']);
            //     unlink(HOME_SLIDER.'thumbnail/thumb/'.$get_image[0]['slider_image']);
            // }
            $record = $this->Production_model->delete_record('sub_of_sub_category',array('sub_of_subcat_id'=>$value));
            // $record = $this->Production_model->delete_record('product_manage',array('sub_of_subcat_id'=>$value)); 
        }
        if ($record != 0) {
            $this->session->set_flashdata('success', 'Category Deleted Successfully....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
        else
        {
            $this->session->set_flashdata('error', 'Category Not Deleted....!');
            redirect($_SERVER['HTTP_REFERER']);
        }
    }  

    // ajax call category wise all subcategory list start // 
    
    function get_sub_cat_name(){
        $cat_id = $this->input->post('category_id');
        $sub_cat_id = $this->input->post('sub_cat_id');

        $get_sub_cat = $this->Production_model->get_all_with_where('sub_category','','',array('category_id'=>$cat_id));
        // echo"<pre>"; print_r($get_sub_cat); exit;
        ?><option value=''>Select subcategory</option><?php
        if ($get_sub_cat !=null) {
            foreach ($get_sub_cat as $key => $sub_cat_row) {
                ?><option value="<?= $sub_cat_row['sub_category_id']?>" <?= ($sub_cat_row['sub_category_id'] == $sub_cat_id) ? 'selected' : '';?>>
                    <?php   
                        echo $sub_cat_row['sub_category_name'];
                    ?>
                </option><?php
            }
        }
    }
    // ajax call category wise all subcategory list end // 
}
?>