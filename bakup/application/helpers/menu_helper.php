<?php

defined('BASEPATH') OR exit('No direct script access allowed');
if (!function_exists('get_active_class')) {

    function get_active_class($key) {

        $ci = & get_instance();
        $link = $ci->router->fetch_class() . "/" . $ci->router->fetch_method();

        $menu = array(
            "dashboard/index" => array("dashboard"),

            "user/add" => array("user", "users", "user-add"),
            "user/view" => array("user", "users", "user-view"),
            "user/edit" => array("user", "users", "user-add"),
            "user/details" => array("user", "users", "user-view"),
            "user/document" => array("user", "users", "user-view"),
            "payment_type/add" => array("payment-type", "payment-type-add"),
            "payment_type/edit" => array("payment-type", "payment-type-edit"),
            "payment_type/view" => array("payment-type", "payment-type-view"),           
            "account/changepassword" => array("my-account", "change-password","master-admin"),

            "userlist/view" => array("userlist","view","master-admin"),
            "userlist/add" => array("userlist","add","master-admin"),
            "userlist/edit" => array("userlist","master-admin"),
            "userlist/details" => array("userlist","view","details","master-admin"),

            "users_registered/index" => array("report","users-registered"),
            "add_posted/index" => array("report","add-posted"),

            "feedback/view" => array("feedback","view","website"),
            "feedback/details" => array("feedback","view","details","website"),
            
            "about/view" => array("about","view","website"),
            "about/details" => array("about","view","details","website"),

            "how_it_work/view" => array("how-it-work","view"),
            "how_it_work/add" => array("how-it-work","view","add"),
            "how_it_work/edit" => array("how-it-work","view","edit"),

            "faqcategory/index" => array("faq","faqcategory"),
            "faqcategory/add" => array("faq","faq-category"),
            "faqcategory/edit" => array("faq","faq-category"),

            "faq/view" => array("faq","view"),
            "faq/add" => array("faq","view","add"),
            "faq/edit" => array("faq","view","edit"),

            "social_links/add" => array("social-links","add","master-admin"),
            "social_links/edit" => array("social-links","edit","master-admin"),
            "social_links/view" => array("social-links","view","master-admin"),
            "social_links/details" => array("social-links","view","details","master-admin"),
            
            "blog/view" => array("blog", "view"),
            "blog/add" => array("blog", "add"),
            "blog/edit" => array("blog", "edit"),

            "seo_page/view" => array("seo_page", "view"),
            "seo_page/add" => array("seo_page", "add"),
            "seo_page/edit" => array("seo_page", "edit"),

            "contact_us/view" => array("contact_us", "view","website"),
            "contact_us/details" => array("contact_us", "view", "details","website"),

            "subscribe/view" => array("subscribe", "view"),
            "product_ratting/view" => array("product_ratting", "view"),
            
            "home_slider/view" => array("home_slider", "view","website"),
            "home_slider/add" => array("home_slider", "add","website"),
            "home_slider/edit" => array("home_slider", "edit","website"),
            
            "service/view" => array("service", "view"),
            "service/add" => array("service", "add"),
            "service/edit" => array("service", "edit"),

            "testimonial/index" => array("testimonial"),
            "testimonial/add" => array("testimonial"),
            "testimonial/edit" => array("testimonial"),

            "tutorials/index" => array("tutorials","website"),
            "tutorials/add" => array("tutorials","website"),
            "tutorials/edit" => array("tutorials","website"),

            "tutorial_faq/index" => array("tutorial_faq","website"),
            "tutorial_faq/add" => array("tutorial_faq","website"),
            "tutorial_faq/edit" => array("tutorial_faq","website"),

            "hostings/index" => array("hostings","master-admin"),
            "hostings/add" => array("hostings","master-admin"),
            "hostings/edit" => array("hostings","master-admin"),

            "hosting_price/index" => array("hosting_price","master-admin"),
            "hosting_price/add" => array("hosting_price","master-admin"),
            "hosting_price/edit" => array("hosting_price","master-admin"),

            "user_hostings/index" => array("user_hostings","master-admin"),
            "user_hostings/add" => array("user_hostings","master-admin"),
            "user_hostings/edit" => array("user_hostings","master-admin"),

            "domains/index" => array("domains","master-admin"),
            "domains/add" => array("domains","master-admin"),
            "domains/edit" => array("domains","master-admin"),

            "domain_requests/index" => array("domain_requests","master-admin"),
            
            "user_domains/index" => array("user_domains","master-admin"),
            "user_domains/add" => array("user_domains","master-admin"),
            "user_domains/edit" => array("user_domains","master-admin"),

            "common_features/index" => array("common_features","master-admin"),
            "common_features/add" => array("common_features","master-admin"),
            "common_features/edit" => array("common_features","master-admin"),

            "notification_days/index" => array("notification_days","master-admin"),
            "notification_days/add" => array("notification_days","master-admin"),
            "notification_days/edit" => array("notification_days","master-admin"),

            "security/index" => array("securities","security","master-admin"),
            "security/add" => array("securities","security","master-admin"),
            "security/edit" => array("securities","security","master-admin"),

            "invoices/index" => array("invoices","invoices"),
            "invoices/add" => array("invoices","invoices"),
            "invoices/edit" => array("invoices","invoices"),

            "tickets/index" => array("tickets","tickets"),
            "tickets/add" => array("tickets","tickets"),
            "tickets/edit" => array("tickets","tickets"),

            "security_features/index" => array("securities","security_features","master-admin"),
            "security_features/add" => array("securities","security_features","master-admin"),
            "security_features/edit" => array("securities","security_features","master-admin"),

            "package_management/view" => array("package-management","view"),
            "package_management/add" => array("package-management"),
            "package_management/edit" => array("package-management"),


            "news_letter/view" => array("news_letter", "view"),
            "news_letter/add" => array("news_letter", "add"),
            "news_letter/edit" => array("news_letter", "edit"),

            "post_management/index" => array("post-management"),
            "post_management/add" => array("post-management"),
            "post_management/edit" => array("post-management"),

            "website_settings/view" => array("website-settings","view"),
            "website_settings/add" => array("website-settings","add"),
            "website_settings/edit" => array("website-settings","edit"),
            
            "settings/index" => array("settings","master-admin"),

            "testimonial/index" => array("testimonial","testimonials"),
            "testimonial/add" => array("testimonial","add","testimonials"),
            "brand_icon/index" => array("brand_icon", "testimonials"),
            "brand_icon/add" => array("brand_icon", "add","testimonials"),

            /*Front menu active class*/
            "Home/index" => array("home","index"),
            "product/index" => array("product","index"),
            "product/product_details" => array("product","index"),
            "about_us/index" => array("about-us","index"),
            "blog/index" => array("blog","index"),
            "blog/blog_detail" => array("blog","index"),
            "contact_us/index" => array("contact-us","index"),

        );
        if (array_key_exists($link, $menu)) {
            if (in_array($key, $menu[$link])) {
                echo "active";
            }
        } else {
            return "";
        }
    }
}

if (!function_exists('get_active_class_user')) {

    function get_active_class_user($key) {

        $ci = & get_instance();
        $link = $ci->router->fetch_class() . "/" . $ci->router->fetch_method();

        $menu = array(
            "account/changepassword" => array("my-account"),
            "account/editprofile" => array("my-account"),
            "account/document" => array("my-account"),
            "account/add_document" => array("my-account"),
            "account/details" => array("my-account"),
        );
        if (array_key_exists($link, $menu)) {
            if (in_array($key, $menu[$link])) {
                echo "active";
            }
        } else {
            return "";
        }
    }

}

if (!function_exists("get_client_ip")) {

    // Function to get the client IP address
    function get_client_ip() {
        $ipaddress = '';
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';
        return $ipaddress;
    }

}
if (!function_exists("get_current_url")) {

    function get_current_url() {
        $currentURL = current_url();
        $params = $_SERVER['QUERY_STRING'];
        return $currentURL . '?' . $params;
    }

}
