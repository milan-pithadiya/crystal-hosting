<?php
defined('BASEPATH') OR exit('No direct script access allowed');


if (!function_exists('getPlans')) {
    function getPlans($is_api_call=''){
		$CI =& get_instance();
		$rec = array();	
		$linux = array();	
		$windows = array();	

		$plan_metadata= array();
		$plan_metadata1= array();
		$plan_detail = $CI->Production_model->get_all_with_where('announce','id','desc',array('status'=>"1"));
		// print_r($plan_detail);
		if($plan_detail !=null) {
			$linux_count=0;
			$windows_count=0;
			foreach ($plan_detail as $key => $value) {
				$data =  $CI->Production_model->get_all_with_where('announce_meta','id','desc',array('announce_id'=>$value['id']));
				
				if($value['plan_type'] == '1'){
					
					$linux[$linux_count]['id'] = $value['id'];			
					$linux[$linux_count]['name'] = $value['name'];
					$linux[$linux_count]['price'] = $value['price'];
					foreach ($data as $key1 => $value1) {
						if($value['id'] == $value1['announce_id']){
							$plan_metadata[$key1]['detail'] = $value1['data'];							
							$linux[$linux_count]['details'] = $plan_metadata;
						}
					}
					$linux_count ++;
				}
				
				if($value['plan_type'] == '2')
				{	
					$windows[$windows_count]['id'] = $value['id'];			
					$windows[$windows_count]['name'] = $value['name'];
					$windows[$windows_count]['price'] = $value['price'];
					foreach ($data as $key1 => $value1) {
						if($value['id'] == $value1['announce_id']){
							$plan_metadata1[$key1]['detail'] = $value1['data'];
							$windows[$windows_count]['details'] = $plan_metadata1;
						}
					}
					$windows_count ++;
				}
			}
			$rec["windows"]=$windows;
			$rec["linux"]=$linux;
			// echo "<pre>";print_r($rec);

			
			createResponse($is_api_call,"Success","",$rec);
		}else{
			createResponse($is_api_call,"Fail","No Plan found!",null);
		}
	}
}

if (!function_exists('getMyPlans')) {
    function getMyPlans($user_id,$is_api_call=''){
		$CI =& get_instance();
		$rec = array();		
		$plan_metadata= array();
		// $plan_detail = $CI->Production_model->get_all_with_where('user_plans','id','desc',array('status'=>"1",'user_id'=>$user_id));

			$where['user_plans.status'] = "1";
			$where['user_plans.user_id'] = $user_id;

            $join[0]['table_name'] = 'users';
            $join[0]['column_name'] = 'users.id = user_plans.user_id';
            $join[0]['type'] = 'left';

            $user_plan_detail = $CI->Production_model->jointable_descending(array('user_plans.*','users.id as user_id','concat(users.first_name ," ",users.last_name) as name','users.email'),'user_plans','',$join,'user_plans.id','desc',$where,array(),'','','');
        
            // echo"<pre>"; echo $CI->db->last_query(); print_r($user_plan_detail); exit;

		
		
		if ($user_plan_detail !=null) {

			/*
				`id`, `type`, `user_id`, `domain_id`, `domain_name`, `provider_id`, `username`, `password`, `client_name`, `client_notification_email`, `client_phone`, `admin_notify_email`, `book_date`, `expiry_date`, `name_server`, `price`, `notification_off`, `status`, `created`, `updated`
			*/
			foreach ($user_plan_detail as $key => $value) {
				$rec[$key]['id'] = $value['id'];			
				$rec[$key]['name'] = $value['name'];
				$rec[$key]['email'] = $value['email'];
				$rec[$key]['domain_id'] = $value['domain_id'];
				$rec[$key]['username'] = $value['username'];
				$rec[$key]['password'] = $value['password'];
				$rec[$key]['provider_id'] = $value['provider_id'];
				$rec[$key]['name_server'] = $value['name_server'];
				$rec[$key]['price'] = $value['price'];
				$rec[$key]['book_date'] = $value['book_date'];
				$rec[$key]['expiry_date'] = $value['expiry_date'];
				$rec[$key]['status'] = $value['status'];
				
			}
			createResponse($is_api_call,"Success","",$rec);
		}else{
			createResponse($is_api_call,"Fail","No Plan found!",null);
		}
	}
}

if (!function_exists('generate_ticket')) {
    function generate_ticket($ticket_data){
		$CI =& get_instance();
		$is_api_call = isset($ticket_data['is_api_call']) ? $ticket_data['is_api_call'] : '';
		// $user_details = $CI->Production_model->get_all_with_where('users','','',array('id'=>$id));
		$create_date = date('Y-m-d h:i:s');

		$ticket_data['ticket_no'] = generate_invoice_no(6);
		$ticket_data['status'] = '0'; //0 deactive , 1 active
		$ticket_data['user_file']=$ticket_data['new_file_name'];
		$ticket_data['create_date'] = $create_date;
		
		// echo "<pre>";print_r($ticket_data);exit;

		$CI->form_validation->set_data($ticket_data);
    	$CI->form_validation->set_rules('name', '', 'trim|required', array('required' => 'Please enter name'));
    	$CI->form_validation->set_rules('subject', '', 'trim|required', array('required' => 'Please enter subject'));
    	$CI->form_validation->set_rules('email', '', 'trim|required|valid_email', array('required' => 'Please enter email'));
    	$CI->form_validation->set_rules('message', '', 'trim|required', array('required' => 'Please enter message'));
    	$CI->form_validation->set_rules('g-recaptcha-response', '', 'trim|required', array('required' => 'Please select captcha'));
    	if ($CI->form_validation->run() === FALSE) {
	        foreach ($CI->form_validation->error_array() as $key => $value) {
	            // $response_array['message'] = array($key => $value);
	            return createResponse($is_api_call,"Fail",$value,null);
	            //$response_array['message'] = $value;
	            break;
	        }
	    }else{  
	    	if (isset($ticket_data['new_file_name'])) {
	            unset($ticket_data['new_file_name']);
	        }
	        if (isset($ticket_data['g-recaptcha-response'])) {
	            unset($ticket_data['g-recaptcha-response']);
	        }
	        // echo "<pre>";print_r($ticket_data);exit;
			$record=$CI->Production_model->insert_record('tickets',$ticket_data);
			$get_user = $CI->Production_model->get_all_with_where('user_register','','',array('id'=>$ticket_data['user_id']));
			// echo "<pre>";print_r($get_user[0]);
			if(isset($get_user) && !empty($get_user)){
				extract($get_user[0]);
				$ticket_data = array_merge($ticket_data,$get_user[0]);
				$ticket_data['name']=$first_name." ".$last_name;
			}
			$send_mail = $CI->Production_model->mail_send('Ticket Generated..',$ticket_data['email'],'','mail_form/admin_send_mail/ticket_email',$ticket_data,'');
			$send_user_mail = $CI->Production_model->mail_send('Ticket Generated..',$ticket_data['email'],'','mail_form/ticket_user_mail',$ticket_data,'');
			if($record !=''){
				return createResponse($is_api_call,"Success","Ticket generated Successfully.",array('id'=>(string) $record));
			}else{
				return createResponse($is_api_call,"Fail","Error while Ticket generating.",null); 
			}
		}
	}
}

if (!function_exists('add_ticket_reply')) {
    function add_ticket_reply($ticket_data){
		$CI =& get_instance();
		$is_api_call = isset($ticket_data['is_api_call']) ? $ticket_data['is_api_call'] : '';
		// $user_details = $CI->Production_model->get_all_with_where('users','','',array('id'=>$id));
		$create_date = date('Y-m-d h:i:s');

		$ticket_data['reply_date'] = date('Y-m-d');
		$ticket_data['reply_time'] = date('h:i:s');
		$ticket_data['create_date'] = $create_date;
		// echo "<pre>";print_r($ticket_data);exit;

		$CI->form_validation->set_data($ticket_data);
    	$CI->form_validation->set_rules('reply_type', '', 'trim|required', array('required' => 'Please enter reply type'));
    	$CI->form_validation->set_rules('ticket_reply', '', 'trim|required', array('required' => 'Please enter message'));
    	//$CI->form_validation->set_rules('g-recaptcha-response', '', 'trim|required', array('required' => 'Please select captcha'));
    	if ($CI->form_validation->run() === FALSE) {
	        foreach ($CI->form_validation->error_array() as $key => $value) {
	            // $response_array['message'] = array($key => $value);
	            return createResponse($is_api_call,"Fail",$value,null);
	            //$response_array['message'] = $value;
	            break;
	        }
	    }else{  
	    	if (isset($ticket_data['new_file_name'])) {
	            unset($ticket_data['new_file_name']);
	        }
	        if (isset($ticket_data['g-recaptcha-response'])) {
	            unset($ticket_data['g-recaptcha-response']);
	        }
	        // echo "<pre>";print_r($ticket_data);exit;
			$record=$CI->Production_model->insert_record('ticket_reply',$ticket_data);
			// echo "<pre>".$CI->db->last_query();print_r($ticket_data);exit;
			// $get_user = $CI->Production_model->get_all_with_where('user_register','','',array('id'=>$ticket_data['user_id']));
			// // echo "<pre>";print_r($get_user[0]);
			// if(isset($get_user) && !empty($get_user)){
			// 	extract($get_user[0]);
			// 	$ticket_data = array_merge($ticket_data,$get_user[0]);
			// 	$ticket_data['name']=$first_name." ".$last_name;
			// }
			// $send_mail = $CI->Production_model->mail_send('Ticket Generated..',$ticket_data['email'],'','mail_form/admin_send_mail/ticket_email',$ticket_data,'');
			// $send_user_mail = $CI->Production_model->mail_send('Ticket Generated..',$ticket_data['email'],'','mail_form/ticket_user_mail',$ticket_data,'');
			if($record !=''){
				return createResponse($is_api_call,"Success","Ticket replied Successfully.",array('ticket_id'=>(string) $ticket_data['ticket_id']));
			}else{
				return createResponse($is_api_call,"Fail","Error while Ticket reply.",null); 
			}
		}
	}
}


?>
