<?php 
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('include/header');
?>	    
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/chat-model.css')?>">   
	<!-- Product Area Start Here -->
    <section class="s-space-bottom-full bg-accent-shadow-body">
        <div class="container-fluid">
            <div class="breadcrumbs-area">
                <ul>
                    <li><a href="<?= base_url()?>">Home</a> -</li>
                    <li><a href="<?= base_url()?>"><?= isset($post_details) && $post_details !=null ? $post_details[0]['category_name'] : '';?></a> -</li>
                    <?php
                    	if (isset($post_details[0]['sub_category_name']) && $post_details[0]['sub_category_name'] !=null) {
                    	?>
                    		<li class="active"><?= $post_details[0]['sub_category_name']?></li>
                    	<?php }
                    ?>
                </ul>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-3 col-lg-4 col-md-12 col-sm-12 col-12">
                    <div class="sidebar-item-box">
                        <div class="gradient-wrapper">
                            <div class="gradient-title">
                                <h2>Seller Information</h2>
                            </div>
                            <ul class="sidebar-seller-information">
                                <li>
                                    <div class="media">
                                        <img src="<?= base_url()?>assets/image/user/user1.png" alt="user" class="img-fluid pull-left">
                                        <div class="media-body">
                                            <span>Posted By</span>
                                            <h3><?= $post_details[0]['name'];?></h3>
                                        </div>
                                    </div>
                                    <?php
                                        if ($this->session->userdata('login_id') !=null && $this->session->userdata('login_id') != $post_details[0]['user_id'] && $post_details[0]['order_status'] != 1) {
                                        ?>
                                            <!-- <a href="javascript:void(0)" class="cp-default-btn-sm-primary chat-with-seller-btn chat_seller" data-product-id="<?= $post_details[0]['post_id']?>" data-seller-id="<?= $post_details[0]['user_id']?>" data-message-send-by="<?= $post_details[0]['user_id']?>" data-user-id="<?= $this->session->userdata('login_id')?>">
                                                CHAT WITH SELLER
                                            </a> -->

                                            <a href="javascript:void(0)" class="cp-default-btn-sm-primary chat-with-seller-btn chat_seller" data-product-id="<?= $post_details[0]['post_id']?>" data-buyer-id="<?= $this->session->userdata('login_id')?>" data-seller-id="<?= $post_details[0]['user_id']?>" data-message-send-by="<?= $this->session->userdata('login_id')?>" >
                                                CHAT WITH SELLER
                                            </a>
                                        <?php }
                                    ?>
                                </li>
                                <li>
                                    <div class="media">
                                        <img src="<?= base_url()?>assets/image/user/user2.png" alt="user" class="img-fluid pull-left">
                                        <div class="media-body">
                                            <span>Location</span>
                                            <h3><?= isset($post_details) && $post_details !=null ? $post_details[0]['country_name'].', '.$post_details[0]['state_name'].', '.$post_details[0]['city_name'] : '';?></h3>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                        <img src="<?= base_url()?>assets/image/user/user3.png" alt="user" class="img-fluid pull-left">
                                        <div class="media-body">
                                            <span>Contact Number</span>
                                            <h3>
                                            	<?= $this->session->userdata('login_id') !=null && $post_details[0]['mobile_no_display'] == '1' ? $post_details[0]['mobile_no'] : '** *** ****'?> 
                                            	<!-- <a href="#" class="fs-14">Show number</a> -->
                                        	</h3>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                        <img src="<?= base_url()?>assets/image/user/email.png" alt="user" class="img-fluid pull-left">
                                        <div class="media-body">
                                            <span>Email address</span>
                                            <h3>
                                                <?= $this->session->userdata('login_id') !=null ? $post_details[0]['user_email'] : '*******@gmail.com'?>             
                                            </h3>
                                        </div>
                                    </div>
                                </li>
                                <li>
                                    <div class="media">
                                        <img src="<?= base_url()?>assets/image/user/rupees.png" alt="user" class="img-fluid pull-left" style="height:30px">
                                        <div class="media-body">
                                            <span>Price</span>
                                            <h3>Rs.<?= isset($post_details) && $post_details !=null ? $post_details[0]['price'] : '';?></h3>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-xl-9 col-lg-8 col-md-12 col-sm-12 col-12">
                    <div class="gradient-wrapper item-mb">
                        <div class="gradient-title detailpage-title">
                            <h2><span class="color-light-gray">I want :</span> <?= isset($post_details) && $post_details !=null ? $post_details[0]['i_want'] : '';?></h2>
                        </div>
                        <div class="gradient-padding reduce-padding">
                            <ul class="item-actions border-bottom mb-3">
                                <?php
                                    $wishlist_details = $this->Production_model->get_all_with_where('product_like','','',array('product_id'=>$post_details[0]['post_id'],'user_id'=>$this->session->userdata('login_id')));
                                    
                                    if ($this->session->userdata('login_id') != $post_details[0]['user_id']) {
                                        if ($wishlist_details !=null) {
                                        ?>
                                            <li class="wishlist available" id="<?= $post_details[0]['post_id']?>"><a href="javascript:void(0)" class="wishlist"><i class="fa fa-heart" aria-hidden="true"></i>Save Ad</a></li>
                                        <?php }
                                        else{
                                        ?>
                                            <li class="wishlist" id="<?= $post_details[0]['post_id']?>"><a href="javascript:void(0)" class="wishlist"><i class="fa fa-heart-o" aria-hidden="true"></i>Save Ad</a></li>
                                        <?php }
                                    }
                                    $post_type = isset($post_details) && $post_details !=null && $post_details[0]['ads_status'] == 1 ? 'Paid add' : 'Free add';
                                    if ($post_details[0]['post_approve_reject'] == '1') {
                                        $post_status = 'Approve';  
                                    }
                                    elseif ($post_details[0]['post_approve_reject'] == '0') {
                                        $post_status = 'Reject';  
                                    }
                                    elseif ($post_details[0]['post_approve_reject'] == '2') {
                                        $post_status = 'Pending';  
                                    }
                                    $order_status = isset($post_details) && $post_details !=null && $post_details[0]['order_status'] == '1' ? 'Complete' : 'Running';
                                ?>
                                <li><a href="javascript:void(0)"><i class="fa fa-share-alt" aria-hidden="true"></i>Share ad</a></li>
                                <li><a href="javascript:void(0)"><i class="fa fa-search"></i><?= $post_type?></a></li>
                                <?php
                                    if ($this->session->userdata('login_id') == $post_details[0]['user_id']) {
                                    ?>
                                        <li><a href="javascript:void(0)"><i class="fa fa-share-alt" aria-hidden="true"></i><?= $post_status?></a></li>
                                    <?php }
                                ?>
                                <?php
                                    if ($this->session->userdata('login_id') == $post_details[0]['user_id']) {
                                    ?>
                                        <li><a href="javascript:void(0)"><i class="fa fa-share-alt" aria-hidden="true"></i><?= $order_status?></a></li>
                                    <?php }
                                ?>
                            </ul>
                            <div class="section-title-left-dark child-size-xl mb-10">
                                <!-- <h3>Product Details:</h3> -->
                                <!-- <p class="m-0"><?= isset($post_details) && $post_details !=null ? $post_details[0]['post_description'] : '';?></p> -->
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <?php
                                        if (isset($post_details) && $post_details !=null) {
                                            if ($post_details[0]['post_approve_reject'] == '1') {
                                                $post_status = 'Approve';  
                                            }
                                            elseif ($post_details[0]['post_approve_reject'] == '0') {
                                                $post_status = 'Reject';  
                                            }
                                            elseif ($post_details[0]['post_approve_reject'] == '2') {
                                                $post_status = 'Pending';  
                                            }
                                            $description = $post_details[0]['post_approve_reject_resion'];
                                        }
                                    ?>
                                    
                                    <div class="section-title-left-dark child-size-xl">
                                        <h3>Specification:</h3>
                                    </div>
                                    <ul class="specification-layout1 mb-10">
                                        <li><?= isset($post_details) && $post_details !=null ? $post_details[0]['post_description'] : '';?></li>

                                        <?php
                                            if (isset($post_details) && $post_details[0]['user_id'] == $this->session->userdata('login_id')) {
                                            ?>
                                                <li>Post status : <?= $post_status?></li>
                                                <?php
                                                    if (isset($description) && $description !=null && $post_details[0]['post_approve_reject'] == '0') {
                                                    ?>
                                                        <li>Description : <?= $description?></li>
                                                    <?php }
                                                ?>
                                            <?php }
                                        ?>
                                        <!-- <li>Intel Iris Graphics 6100</li>
                                        <li>8GB memory (up from 4GB in 2013 model)</li>
                                        <li>10 hour battery life</li>
                                        <li>13.3" Retina Display</li> -->
                                    </ul>
                                </div>
                                <div class="col-md-6">
                                    <div class="single-product-img-layout1 item-mb">
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                <div class="tab-content">
                                                    <?php
                                                		$counter =0;
                                                		if (isset($post_images) && $post_images !=null) {
                                                			foreach ($post_images as $key => $value) {
                                                				$counter ++;
                                                			?>
                			                                    <div role="tabpanel" class="tab-pane fade <?= $counter == 1 ? 'show active' : '';?>" id="related<?= $key+1?>">
                			                                        <a href="#" class="zoom ex1"><img alt="single" src="<?= base_url(POST_NEED_IMG.$value['similar_image'])?>" class="img-fluid"></a>
                			                                    </div>
                                                			<?php } 
                	                                	}
                	                               	?>
                                                </div>
                                            </div>
                                            <div class="col-lg-12 col-md-12 col-sm-12 col-12">
                                                <ul class="nav tab-nav tab-nav-inline cp-carousel nav-control-middle" data-loop="true" data-items="6" data-margin="15" data-autoplay="true" data-autoplay-timeout="5000" data-smart-speed="2000" data-dots="false" data-nav="true" data-nav-speed="false" data-r-x-small="2" data-r-x-small-nav="true" data-r-x-small-dots="false" data-r-x-medium="4" data-r-x-medium-nav="true" data-r-x-medium-dots="false" data-r-small="3" data-r-small-nav="true" data-r-small-dots="false" data-r-medium="4" data-r-medium-nav="true" data-r-medium-dots="false" data-r-Large="3" data-r-Large-nav="true" data-r-Large-dots="false">

                                                	<?php
                                                		$counter =0;
                                                		if (isset($post_images) && $post_images !=null) {
                                                			foreach ($post_images as $key => $value) {
                                                				$counter ++;
                                                			?>
			                                                    <li class="nav-item">
			                                                        <a class="<?= $counter == 1 ? 'active' : '';?>" href="#related<?= $key+1?>" data-toggle="tab" aria-expanded="false">
			                                                        	<img alt="related1" src="<?= base_url(POST_NEED_IMG.$value['similar_image'])?>" class="img-fluid">
			                                                        </a>
			                                                    </li>
                                                			<?php }
                                                		}
                                                	?>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Product Area End Here -->

    <!--Chat module-->
    <div id="chatModal" class="modal fade" role="dialog" tabindex="-1">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">          
                <div class="modal-body">
                    <div class="container">
                        <form method="post" id="form">
                            <input type="hidden" name="seller_id" class="seller_id">
                            <input type="hidden" name="buyer_id" class="buyer_id">
                            <input type="hidden" name="product_id" class="product_id">
                            <input type="hidden" name="message_send_by" class="message_send_by">

                            <h3 class=" text-center">Messaging</h3>
                            <div class="messaging">
                                <div class="inbox_msg">
                                    <div class="mesgs">
                                        <div class="msg_history">
                                            <!-- <div class="incoming_msg">
                                                <div class="incoming_msg_img"> <img src="https://ptetutorials.com/images/user-profile.png" alt="sunil"> </div>
                                                <div class="received_msg">
                                                    <div class="received_withd_msg">
                                                        <p>Test which is a new approach to have all
                                                            solutions
                                                        </p>
                                                        <span class="time_date"> 11:01 AM    |    June 9</span>
                                                    </div>
                                                </div>
                                            </div> -->

                                            <!-- send message -->
                                            <div class="send_msg">
                                                <!-- <div class="outgoing_msg">
                                                    <div class="sent_msg">
                                                        <p>Test which is a new approach to have all
                                                            solutions
                                                        </p>
                                                        <span class="time_date"> 11:01 AM    |    June 9</span> 
                                                    </div>
                                                </div> -->
                                            </div>
                                        </div>
                                        <div class="type_msg">
                                            <div class="input_msg_write">
                                                <span class="message_error"></span>
                                                <input type="text" class="write_msg" name="message" placeholder="Type a message" />
                                                <button class="msg_send_btn" type="button"><i class="fa fa-paper-plane-o" aria-hidden="true"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php $this->load->view('include/copyright');?>
<script src="<?php echo base_url(); ?>assets/validation/jquery.validate.js"></script>
<script type="text/javascript" src="http://stevenlevithan.com/assets/misc/date.format.js"></script>
<script>

    // ============= chat-module start =============//
    $(document).ready(function(){
        seller_chat();
        chat_histroy();
        
        $(document).on('click', '.chat_seller', function() {
            var seller_id = $(this).data('seller-id');
            var buyer_id = $(this).data('buyer-id');
            var product_id = $(this).data('product-id');
            var message_send_by = $(this).data('message-send-by');
            var message = $('.write_msg').val();
            $('.seller_id').val(seller_id);
            $('.buyer_id').val(buyer_id);
            $('.product_id').val(product_id);
            $('.message_send_by').val(message_send_by);

            myscroll = $('.msg_history');
            myscroll.scrollTop(myscroll.get(0).scrollHeight);

            $.ajax({
                type: 'post',
                url: '<?= base_url('chat/chat_invitation')?>',
                data: {seller_id:seller_id,buyer_id:buyer_id,product_id:product_id,message_send_by:message_send_by,message:message},
                dataType: 'json',
                success: function (response) {
                    if (response.seller_status == 1){
                        /*User chat histroty.*/
                        chat_histroy();
                        /*End*/

                        $('#chatModal').modal('show');
                    }
                    if (response.result == 'false' && response.seller_status == 2){
                        $.alert({
                            title: 'Success',
                            type: 'red',
                            content: 'You chat invitation is rejectd...!',
                        });
                    }
                    if (response.result == 'true' && response.seller_status == 0){
                        $.alert({
                            title: 'Success',
                            type: 'green',
                            content: 'Your invitation sent successfully...!',
                        });
                    }
                    if (response.result == 'false' && response.seller_status == 0){
                        $.alert({
                            title: 'Success',
                            type: 'red',
                            content: 'You already sent invitation...!',
                        });
                    }
                }
            });
        });
        
        $(document).on('click', '.msg_send_btn', function() {
            chat_form();
        });

        $("#form").keypress(function (e) {
            if (e.keyCode == 13) {
                chat_form();
                e.preventDefault();
            }
        });

        /* Click the chat button*/
       
        function chat_histroy(){
            $.ajax({
                type: 'post',
                url: '<?= base_url('chat/user_chat_list')?>',
                data: $('form').serialize(),
                dataType: 'html',
                success: function (response) {
                    $('.msg_history').html(response);
                    myscroll = $('.msg_history');
                    myscroll.scrollTop(myscroll.get(0).scrollHeight);
                }
            });
        }

        function chat_form(){
            var message = $('.write_msg').val();
            var datetime = dateFormat(new Date(), "d-m-yyyy h:MM:ss TT");

            $('.error').remove();
            if($.trim($('[name="message"]').val()) == ''){
                $('<label id="message-error" class="error" for="message" style="">Please type message</label>').insertAfter('.write_msg');
            } 
            else {
                $.ajax({
                    type: 'post',
                    url: '<?= base_url('chat/add')?>',
                    data: $('form').serialize(),
                    dataType: 'json',
                    success: function (response) {
                        if (response.result == 'success'){
                            $('.msg_history').append('<div class="outgoing_msg"><div class="sent_msg"><p>'+message+'</p><span class="time_date"> '+datetime+' </span></div></div>');
                            $('.write_msg').val('');                          
                            myscroll = $('.msg_history');
                            myscroll.scrollTop(myscroll.get(0).scrollHeight);
                        }
                    }
                });
            }
        }
    });
    
    setInterval(function(){
       seller_chat(); // this will run after every 5 seconds
    }, 5000);
    
    function seller_chat(){
        if ($('body').hasClass('modal-open')) {
            $.ajax({
                type: 'post',
                url: '<?= base_url('chat/seller_chat')?>',
                data: $('form').serialize(),
                dataType: 'json',
                success: function (response) {
                    if (response.result == 'success'){
                        $('.msg_history').append(response.html);
                        myscroll = $('.msg_history');
                        myscroll.scrollTop(myscroll.get(0).scrollHeight);
                    }
                }
            });
        }
    }

    // ============= chat-module end =============//

    // =============== wishlist start =============== //    
    $(document).on('click', '.wishlist',function(){    
        var product_id = $(this).attr('id');

        var current = $(this);
        $.ajax({
            url: "<?php echo base_url('wishlist/add_wishlist')?>",
            data: {product_id:product_id},
            type: "POST",
            dataType:'json',
    
            success: function(data) {
                if (data.success) {
                    current.addClass('available');
                    current.find('i').addClass('fa-heart');
                    current.find('i').removeClass('fa-heart-o');
                    current.find('a').attr('title', 'Remove wishlist');
                    $.alert({
                        title: 'Success',
                        type: 'green',
                        content: 'Wishlist added successfully...!',
                    });
                }
                if (data.delete) {
                    current.removeClass('available');
                    current.find('i').removeClass('fa-heart');
                    current.find('i').addClass('fa-heart-o');
                    current.find('a').attr('title', 'Add wishlist');
                    $.alert({
                        title: 'Success',
                        type: 'blue',
                        content: 'Wishlist removed successfully...!',
                    });
                }
                if (data.error == false) {
                    $.alert({
                        title: 'Error',
                        type: 'red',
                        content: 'Please login after add wishlist...!',
                    });
                }
            }
        });
    });    
    // =============== wishlist end =============== //
</script>

<?php $this->load->view('include/footer');?>