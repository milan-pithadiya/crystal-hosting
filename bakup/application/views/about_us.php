<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
    </head>
    <body>
        <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          <!-- about start -->
            <div class="allpage_banner_about allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'about.jpg')?>);">
                <h1 class="title_h1">About Us</h1>
                <p><a href="index.html">Home </a> / About Us</p>
            </div>
            <div class="about_all padding_all testimonial_all" >
                <div class="container">
                    
                    <div class="row text-center" >
                        <div class=" col-md-12">
                        <?php if (isset($about_data) && $about_data !=null){ 
                                foreach ($about_data as $key => $value) { ?>
                                <div class="row">
                                    <div class="col-md-4 back_about" style="background:none;"><img src="<?=base_url(ABOUT_IMAGE).$value['about_image']; ?>" width="100%"></div>
                                    <div class="col-md-8 white_about">
                                        <h3><?php echo $value['title']; ?></h3>
                                        <?php echo $value['description']; ?>
                                    </div>
                            </div><hr>
                        <?php } }?> 
                        </div>
                    </div>
                </div>
            </div>
      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?> 
   </body>
</html> 