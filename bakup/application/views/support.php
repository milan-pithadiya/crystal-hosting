<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
    </head>
    <body>
        <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          <!-- about start -->
            <div class="allpage_banner_email allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'email.jpg')?>);">
              <h1 class="title_h1">Support</h1>
              <p><a href="<?=base_url()?>">Home </a> / Support</p>
            </div>
            <div class="support_all">
              <div class="container padding_all">
                <div class="row margin_top">
                  <div class="col-md-4 col-xs-12 text-center">
                    <div class="support_box">
                      <i class="fa fa-envelope-o"></i>
                      <h2>Email Support</h2>
                      <p>Slightly you are needs to be sure chunks is therefore always.</p>
                      <h5><a href="javascript:;"> contact@crystalhosting.com</a></h5>
                      <!-- <h5><a href="javascript:;"> legalenq@ewebguru.com</a></h5>
                      <h5><a href="javascript:;">  abusecell@ewebguru.com</a></h5> -->
                    </div>
                  </div>
                  <div class="col-md-4 col-xs-12 text-center">
                    <div class="support_box">
                      <i class="fa fa-phone"></i>
                      <h2>Phone Support</h2>
                      <p>Call us at below number 24x7, for support keep your ticket number handy. </p>
                      <h5><span>Sales:</span> +918000999001</h5>
                      <!-- <h5><span>Support:</span> 0120-4806750(Ext 708,709)</h5> -->
                    </div>
                  </div>
                  <div class="col-md-4 col-xs-12 text-center">
                    <div class="support_box">
                      <i class="fa fa-commenting-o"></i>
                      <h2>Email Support</h2>
                      <p>Chat with our sales/support/billing team. Support are available 24x7.</p>
                      <button class="btn_chat btn_margin">Start a live chat</button>
                    </div>
                  </div>
                </div>
                <div class="row margin_top">
                  <div class="col-md-4 col-xs-12 text-center">
                    <div class="support_box">
                      <i class="fa fa-star-o"></i>
                      <h2>Knowledge Base</h2>
                      <p>Browse of knowledgebase articles</p>
                      <a href="<?=base_url('tutorials')?>" class="btn_chat btn_margin">Start Knowledge Base</a>
                      <!-- <button class="btn_chat btn_margin">Start Knowledge Base</button> -->
                    </div>
                  </div>
                  <div class="col-md-4 col-xs-12 text-center">
                    <div class="support_box">
                      <i class="fa fa-video-camera"></i>
                      <h2>Video Tutorials</h2>
                      <p>View video tutorials on how to use web hosting</p>
                      <a href="<?=base_url('tutorials/video-tutorials')?>" class="btn_chat btn_margin">Start Video Tutorials</a>
                      <!-- <button class="btn_chat btn_margin">Start Video Tutorials</button> -->
                    </div>
                  </div>
                  <div class="col-md-4 col-xs-12 text-center">
                    <div class="support_box">
                      <i class="fa fa-ticket"></i>
                      <h2>Submit a Ticket</h2>
                      <p>The fastest way to resolve your issue submit ticket from client area.</p>
                      <a href="<?=base_url('ticket/generate-ticket')?>" class="btn_chat btn_margin">Submit a Ticket</a>
                      <!-- <button class="btn_chat btn_margin">Submit a Ticket</button> -->
                    </div>
                  </div>
                </div>
              </div>      
            </div>
      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?> 
   </body>
</html> 