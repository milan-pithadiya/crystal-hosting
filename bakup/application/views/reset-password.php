<!doctype html>
<html class="no-js" lang="">
<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?= SITE_TITLE?></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="shortcut icon" type="image/x-icon" href="<?= base_url()?>assets/image/favicon.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/bootstrap.min.css">
    <!-- Animate CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/animate.min.css">
    <!-- Font-awesome CSS-->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/font-awesome.min.css">
    <!-- Main Menu CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/meanmenu.min.css">
    <!-- Select2 CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/select2.min.css">
    <!-- Custom CSS -->
    <link rel="stylesheet" href="<?= base_url()?>assets/css/style.css">

    <!--New add-->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/developer.css">
    <script>
        var BASE_URL = '<?php echo base_url(); ?>';
    </script>
</head>
<body>
    <!-- Preloader End Here -->
    <div id="wrapper">
        <header>
            <div class="header-top">
                <div class="container">
                    <div class="row no-gutters text-center">
                        <div class="col-12">
                            <div class="logo-area">
                                <a href="<?= base_url()?>" class="img-fluid">
                                    <?php
                                        $profile_photo = $this->Production_model->get_all_with_where('user','','',array());
                                        if ($profile_photo != null) {
                                        ?>
                                            <img src="<?= $profile_photo !=null ? base_url('assets/uploads/profile_photo/').$profile_photo[0]['profile_photo'] : base_url('assets/uploads/profile_photo/default-image.png')?>" alt="logo" class="p-3">
                                        <?php } 
                                    ?>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header>
        <!-- Login Area Start Here -->
        <section class="p-md-5 bg-accent-shadow-body">
            <div class="container">
                <div class="row">
                    <div class="offset-lg-2 col-lg-8 offset-md-1 col-md-10 col-12">         
                        <?php $this->load->view('include/messages');?>               

                        <div class="my-account-wrapper gradient-wrapper input-layout1 mt-3">
                            <div class="gradient-title text-center">
                                <h3>Reset Password</h3>
                            </div>

                            <form id="login-page-form" class="col-md-12 p-4" method="post" action="">
                                                          
                                <div class="row">
                                    <div class="col-xl-3 col-md-4 col-12">
                                        <label class="control-label">New Password</label>
                                    </div>
                                    <div class="col-xl-9 col-md-8 col-12">
                                        <div class="form-group">
                                            <input type="password" id="n-password" name="password" class="form-control" placeholder="Type Your Password">
                                        </div>
                                    </div>
                                    <div class="col-xl-3 col-md-4 col-12">
                                        <label class="control-label">Confirm Password</label>
                                    </div>
                                    <div class="col-xl-9 col-md-8 col-12">
                                        <div class="form-group">
                                            <input type="password" id="r-password" name="confirm_password" class="form-control" placeholder="Type Your Confirm Password">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-12">
                                        <div class="form-group">
                                            <button type="submit" class="cp-default-btn-sm-primary disabled border-none w-100">Reset Password</button>
                                        </div>
                                    </div>
                                </div>
                            </form>                   
                        </div>                       
                    </div>
                </div>
            </div>
        </section>
        <!-- Login Area End Here -->
        <!-- Footer Area Start Here -->
        <footer>
            <div class="footer-area-bottom">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12 text-center-mb">
                            <p>Copyright © Crystal Hosting</p>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-12 text-right text-center-mb">
                            <ul>
                                <li>
                                    <img src="<?= base_url()?>assets/image/footer/card1.jpg" alt="card">
                                </li>
                                <li>
                                    <img src="<?= base_url()?>assets/image/footer/card2.jpg" alt="card">
                                </li>
                                <li>
                                    <img src="<?= base_url()?>assets/image/footer/card3.jpg" alt="card">
                                </li>
                                <li>
                                    <img src="<?= base_url()?>assets/image/footer/card4.jpg" alt="card">
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <!-- Footer Area End Here -->
    </div>
    <!-- jquery-->
    <script src="<?= base_url()?>assets/js/jquery-3.2.1.min.js"></script>
    <!-- Popper js -->
    <script src="<?= base_url()?>assets/js/popper.js"></script>
    <!-- Bootstrap js -->
    <script src="<?= base_url()?>assets/js/bootstrap.min.js"></script>
    <!-- Meanmenu Js -->
    <script src="<?= base_url()?>assets/js/jquery.meanmenu.min.js"></script>
    <!-- Srollup js -->
    <script src="<?= base_url()?>assets/js/jquery.scrollUp.min.js"></script>
    <!-- Select2 Js -->
    <script src="<?= base_url()?>assets/js/select2.min.js"></script>
    <!-- Custom Js -->
    
    <!--new added -->
    <script src="<?php echo base_url(); ?>assets/validation/common.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.0/jquery-confirm.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/address.js"></script>
    <script src="<?php echo base_url(); ?>assets/validation/jquery.validate.js"></script>
    <!-- <script src="<?php echo base_url(); ?>assets/js/price-filter.js"></script> -->

    <script>
        /*FORM VALIDATION*/
        $("#login-page-form").validate({
            rules: {                
                'password': {required: true}, 
                'confirm_password': {required: true,equalTo: "#n-password"}, 
            },
            messages: {
                'password': "Please enter password",     
                'confirm_password': {required: 'Enter confirm password',equalTo: "Password and confirm password must be same"},
            }
        });
    </script>
</body>
</html>