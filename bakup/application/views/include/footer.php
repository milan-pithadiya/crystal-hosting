<!-- Footer Area Start Here -->
<!-- <footer>
    <div class="footer-area-top s-space-equal">
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="footer-box">
                        <h3 class="title-medium-light title-bar-left size-lg">Company</h3>
                        <ul class="useful-link">
                            
                            <li>
                                <a href="<?= base_url('about-us')?>">About us</a>
                            </li>
                            <li>
                                <a href="<?= base_url('leadership')?>">Leadership</a>
                            </li>
                            <li>
                                <a href="<?= base_url('contact')?>">Contact us</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="footer-box">
                        <h3 class="title-medium-light title-bar-left size-lg">Policy</h3>
                        <ul class="useful-link">
                            <li>
                                <a href="<?= base_url('copyright-policy')?>">Copyright policy</a>
                            </li>
                            <li>
                                <a href="<?= base_url('hyperlinking-policy')?>">Hyperlinking policy</a>
                            </li>
                            <li>
                                <a href="<?= base_url('privacy-policy')?>">Privacy Policy</a>
                            </li>
                            <li>
                                <a href="<?= base_url('cookie-policy')?>">Cookie Policy</a>
                            </li>
                            
                        </ul>
                    </div>
                </div>
                
                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="footer-box">
                        <h3 class="title-medium-light title-bar-left size-lg">Privacy</h3>
                        <ul class="useful-link">
                            
                            
                            <li>
                                <a href="<?= base_url('terms-condition')?>">Terms &amp; conditions</a>
                            </li>
                            <li>
                                <a href="<?= base_url('content-review')?>">Content review policy</a>
                            </li>
                            <li>
                                <a href="<?= base_url('intellectual-property')?>">Intellectual property rights</a>
                            </li>
                        </ul>
                    </div>
                </div>

                <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                    <div class="footer-box">
                        <h3 class="title-medium-light title-bar-left size-lg">Follow Us On</h3>
                        <ul class="folow-us">
                            <li>
                                <a href="#">
                                    <img src="<?php echo base_url(); ?>assets/image/footer/follow1.jpg" alt="follow">
                                </a>
                            </li>
                        </ul>
                        <ul class="social-link">
                            <?php
                                $social_media_link = $this->Production_model->get_all_with_where('social_links','','',array());
                            ?>
                            <li>
                                <a href="<?= $social_media_link !=null ? $social_media_link[0]['facebook_link'] : '';?>">
                                    <img src="<?php echo base_url(); ?>assets/image/footer/facebook.png" alt="social">
                                </a>
                            </li>
                            <li>
                                <a href="<?= $social_media_link !=null ? $social_media_link[0]['twiter_link'] : '';?>">
                                    <img src="<?php echo base_url(); ?>assets/image/footer/twitter.png" alt="social">
                                </a>
                            </li>
                            <li>
                                <a href="<?= $social_media_link !=null ? $social_media_link[0]['instagram_link'] : '';?>">
                                    <img src="<?php echo base_url(); ?>assets/image/footer/instagram.png" alt="social">
                                </a>
                            </li>
                            <li>
                                <a href="<?= $social_media_link !=null ? $social_media_link[0]['youtube_link'] : '';?>">
                                    <img src="<?php echo base_url(); ?>assets/image/footer/youtube.png" alt="social">
                                </a>
                            </li>
                            <li>
                                <a href="<?= $social_media_link !=null ? $social_media_link[0]['link_in_link'] : '';?>">
                                    <img src="<?php echo base_url(); ?>assets/image/footer/linkedin.png" alt="social">
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
</footer> -->

<footer>
	<div class="padding_all contact">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-xs-12">
					<h3>Hosting Packages</h3>
					<p>Linux Shared Hosting</p>
				</div>
				<div class="col-md-3 col-xs-12">
					<h3>Our Products</h3>
					<ul>
						<li><a href="javascript:;">Web Design</a></li>
						<li><a href="javascript:;">Logo Design</a></li>
						<li><a href="javascript:;">Register Domains</a></li>
						<li><a href="javascript:;">Search Advertising</a></li>
						<li><a href="javascript:;">Email Marketing</a></li>
					</ul>
				</div>
				<div class="col-md-3 col-xs-12">
					<h3>Company</h3>
					<ul>


                        
                        <li><a href="<?=base_url('about-us')?>">About Us</a></li>
                        <li><a href="<?=base_url('blog')?>">Blogs</a></li>                        
						<li><a href="<?=base_url('Privacy-policy')?>">Privacy Policy</a></li>
						<li><a href="<?=base_url('terms-condition')?>">Terms & Conditions</a></li>
						<li><a href="<?=base_url('testimonial')?>">Testimonials</a></li>
                        <li><a href="<?=base_url('contact')?>">Contact Us</a></li>

						
					</ul>
				</div>
				<div class="col-md-3 col-xs-12">
					<a href="javascript:;"><img src="<?=base_url()?>assets/image/footlogo.png" width="130" height="70" class="img_margin"></a><hr>
					<p>310, road, Somnath Complex,
						Bhupendra Road,
						Karanpara,Rajkot,
						Gujarat,360007,India
					</p><hr>
					<a href="#"><span class="fa fa-envelope"> Info@crystalinfoway.com </span></a><br>
					<a href="#"><span class="fa fa-phone"> +918347850002 </span></a><br>
					<a href="#"><span class="fa fa-skype"> crystal.infoway </span></a><br>
				</div>
			</div>
		</div>
	</div>
	<div class="footer_down">
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-xs-12">
					
				</div>
				<div class="col-xs-12 col-md-6 text-right">
					<a href="javascript:;">Copyright &copy; 2019 - 2020. Crystal Infoway. All Rights Reserved.</a>
				</div>
			</div>
		</div>
	</div>
</footer>

