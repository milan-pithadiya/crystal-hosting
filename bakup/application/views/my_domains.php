<?php defined('BASEPATH') OR exit('No direct script access allowed');?>
<!DOCTYPE html>
<html>
    <head>
      <?php $this->load->view('include/header_js');?>
      <link rel="stylesheet" href="<?=base_url('assets/css/jquery.dataTables.min.css')?>">
    </head>
    <body>
        <div class="main aos-all" id="transcroller-body">
          <?php $this->load->view('include/header');?>
          <a href="#top"><i class="fa fa-chevron-circle-up"></i></a>
          <!-- about start -->
            <div class="allpage_banner_email allpage_banner" id="top" style="background-image: url(<?=base_url(IMAGES.'email.jpg')?>);">
              <h1 class="title_h1">My Domains</h1>
              <p><a href="<?=base_url()?>">Home </a> / My Domains</p>
            </div>
            <div class="about_all padding_all testimonial_all my_domain">
              <div class="container">
                <div class="row">
                  <div class="col-md-12 ">
                    <div class="table-responsive">
                    <table id="myTable" class="table-bordered table-hover">  
                          <thead>  
                            <tr>  
                              <th>Domain</th>  
                              <th>Reg. Date</th>  
                              <th>Next Due</th>  
                              <th>Auto Renew</th>
                              <th>Status</th>  
                            </tr>  
                          </thead>  
                          <tbody>
                            <?php 
                                if(isset($user_domains_details) && $user_domains_details !=null){ 
                                  $i=1;
                                  $payment_status_class = 0;
                                  $payment_status_label = '';
                                  // echo "<pre>";print_r($user_domains_details);exit;
                                  
                                  foreach ($user_domains_details as $key => $value) { 

                                    switch ($value['status']) {
                                      case '1':
                                        $payment_status_class = 'pay-btn';
                                        $payment_status_label = 'Active';
                                        break;
                                      case '0':
                                        $payment_status_class = 'dactive-btn';
                                        $payment_status_label = 'Deactive';
                                        break;
                                      
                                      default:
                                        break;
                                    }
                                    ?>
                                    <tr>  
                                      <td><?=$value['domain_name']?><?=$value['title']?></td>
                                      <td><?=format_date_dmy($value['book_date'])?></td>
                                      <td><?=format_date_dmy($value['expiry_date'])?></td>
                                      <td><?=format_date_dmy($value['expiry_date'])?></td>
                                      <!-- <td>
                                        <div class="active-btn">Active</div>
                                      </td> -->
                                      <td class="payment_status">
                                        <a class="active-btn" href="">View</a>
                                        <a class="<?=$payment_status_class?>" href=""><?=$payment_status_label?></a>
                                        <a class="pay-btn" href="#" disabled>Pay</a>
                                      </td>
                                    </tr>
                                  <?php 
                                } 
                              }
                            ?>
                              <!-- <tr>  
                                <td>WWW.Abc.com</td>
                                <td>4/10/2019</td>
                                <td>Abc</td>
                                <td>5/10/2020</td>
                                <td>
                                  <div class="active-btn">Active</div>
                                </td>
                              </tr>
                              <tr>  
                                <td>WWW.efg.com</td>
                                <td>8/10/2019</td>
                                <td>Abc</td>
                                <td>8/10/2020</td>
                                <td>
                                  <div class="dactive-btn">Dactive</div>
                                </td>
                              </tr> -->
                          </tbody>  
                        </table>
                        </div>
                  </div>
                </div>
              </div>
            </div>
            
      <?php $this->load->view('include/footer');?>  
    </div>
      <?php $this->load->view('include/footer_js');?> 
      <script type="text/javascript" src="<?=base_url('assets/js/jquery.dataTables.min.js')?>"></script>
      <script>
        $(document).ready(function(){
            $('#myTable').dataTable({
              "ordering": false
            });
        });
      </script>
   </body>
</html> 