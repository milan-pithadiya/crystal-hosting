<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<style type="text/css">
    a:focus, a:hover{
        color: #000;
        text-decoration: none;
    }
    [data-toggle="collapse"].collapsed.card-header:after {
      transform: rotate(0deg) ;
    }
    .collapsed{
        text-align: left!important
    }
    .ticket_repies thead{
        background-color: lightgray;
    }
    .pop_up_message{
        background-color: lightgray;
    }    
</style>
<?php $this->view('authority/common/sidebar'); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>Ticket</h1>
        <ol class="breadcrumb">
            <li><a href="<?php echo site_url(); ?>authority/dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Ticket</li>
        </ol>
    </section>
    <section class="content">
        <!-- /.row -->
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <!-- /.box-header -->
                    <div class="box-body table-responsive no-padding">
                        <table id="mytable" class="table table-bordred">
                            <tbody>
                                <tr>
                                    <td><b>Ticket No.:</b></td>
                                    <td><?= $ticket_no;?></td>
                                </tr>
                                <tr>
                                    <td><b>User name:</b></td>
                                    <td><?= $first_name;?></td>
                                </tr>
                                <tr>
                                    <td><b>Email:</b></td>
                                    <td><?= $email; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Message:</b></td>
                                    <td><?= $message; ?></td>
                                </tr>
                                <tr>
                                    <td><b>Status:</b></td>
                                    <td><?php
                                        if($status == '1'){
                                            echo 'Processing';
                                        }else if($status == '2'){
                                            echo 'Completed';
                                        }else{
                                            echo 'Pending';
                                        } ?>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td><b>Create date:</b></td>
                                    <td><?= date('d-m-Y h:i:s A',strtotime($create_date)); ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <div >
                    <div id="accordion" role="tablist">
                        <div class="card">
                          <a class="collapsed" data-toggle="collapse" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                            <div class="card-header " role="tab" id="headingTwo">
                              <h3 style="margin-left: 1em;">
                                Replay
                              </h3>
                            </div>
                          </a>
                          <div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion">
                              <div class="card-body">
                                <form class="row" id="ticket_replies" method="post">
                                    <input type="hidden" name="reply_type" value="1">
                                    <input type="hidden" name="reply_from" value="1">
                                    <input type="hidden" name="reply_to" value="<?=$user_id?>">
                                    <input type="hidden" name="ticket_id" value="<?=$id?>">
                                  <div class="form-group col-md-12 col-lg-12 col-sm-12">
                                    <label for="ticket_reply">Messege :</label>
                                    <textarea class="form-control" id="ticket_reply" name="ticket_reply" rows="3"></textarea>
                                  </div>
                                  <div class="form-group col-md-12 col-lg-12 col-sm-12">
                                      <label for="ticket_document">Ticket Document :</label>
                                      <input name="ticket_document" id="ticket_document" type="file" class="input_all">
                                      <p class="err_p" id="ticket_document_err"></p>
                                  </div>
                                  <div class="form-group col-md-12">
                                    <input type="button" class="btn btn-primary check" value="Submit">
                                  </div>
                                </form>
                              </div>
                          </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <h3>Ticket Replies</h3>
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped ticket_repies">
                                <thead>
                                    <tr>
                                        <th>Reply Date & Time</th>
                                        <th>Replied By</th>
                                        <th>Reply</th>
                                        <th>Document</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($tickets_details)) {
                                        foreach ($tickets_details as $key => $value) {
                                            if($value['reply_type'] == '1'){
                                                $reply_type = 'Admin';
                                            }else{
                                                $reply_type = $first_name;
                                            }
                                            if($value['reply_from'] == '1'){
                                                $reply_from = 'Admin';
                                            }else{
                                                $reply_from = $first_name;
                                            }
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo isset($value['reply_date']) ? date('d-m-Y', strtotime($value['reply_date'])) . ' ' : ''; ?>
                                                    <?php echo isset($value['reply_time']) ? date('H:i A', strtotime($value['reply_time'])) : ''; ?>
                                                </td>
                                                <td>
                                                    <?=$reply_type?>
                                                </td>
                                                <td><a href="javascript:void(0);" class="view-reply">View</a></td>
                                                <td>
                                                    <?php
                                                    if ($value['ticket_document'] == '') {
                                                        echo 'Not available';
                                                    } else {
                                                        $path = base_url(TICKET_REPLY_FILE).$value['ticket_document'];
                                                        echo '<a href="'.$path.'" target="_blank" download>Download & View</a>';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr style="display:none;" class="pop_up_message">
                                                <td colspan="6">
                                                    <p><b>Reply</b></p>
                                                    <div class="problem">                                        
                                                        <?php echo isset($value['ticket_reply']) ? closetags($value['ticket_reply']) : ''; ?>
                                                    </div>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        echo '<tr><td colspan="5" align="center">Records not available</td></tr>';
                                    }
                                    ?>                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
</div>
<?php $this->view('authority/common/copyright'); ?>

<?php $this->view('authority/common/footer'); ?>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript">

    $('#ticket_replies').validate({
        rules: {
            ticket_reply: {
                required: true,
            },
        },
        messages: {
            ticket_reply: {
              required: "Please enter your message",
            },
        },
    });

    var form = $("#ticket_replies");
    form.validate();
    $(document).on('click','.check',function(){
      var formData = new FormData($('#ticket_replies')[0]);
      if(form.valid()){
        add_ticket_reply();
      }
    });
    function add_ticket_reply(){          
      var formData = new FormData($('#ticket_replies')[0]);
      var uurl = BASE_URL+"api/plans/add_ticket_reply";
      var ticketURL = BASE_URL+"authority/tickets/details/<?=$id?>";

       $.ajax({
           url: uurl,
           type: 'POST',
           data: formData,
           dataType:'json',
           //async: false,
           beforeSend: function(){
             $('.mask').show();
             $('#loader').show();
           },
           success: function(response){
             if (response.result=="Success") {
                $.alert({
                  title: 'Message',
                  type: 'green',
                  content: response.message,
                });
                setTimeout(function() { window.location.href = ticketURL; }, 2000);
             }else{
               $.alert({
                    title: 'Message',
                    type: 'red',
                    content: 'Reply not added. !!',
                });
             }
           },
           error: function(xhr) {
           //alert(xhr.responseText);
           },
           complete: function(){
             $('.mask').hide();
             $('#loader').hide();
           },
           cache: false,
           contentType: false,
           processData: false
       });
    }
</script> 