<style type="text/css">
    .notification {
        float: left;
        background-color: transparent;
        background-image: none;
        color: #0085a8;
        padding: 10px 15px;
        font-family: fontAwesome;
    }
    .noti-badge{
        display: inline-block;
        min-width: 10px;
        padding: 3px 7px;
        font-size: 18px;
        font-weight: 900;
        line-height: 1;
        color: #fff;
        text-align: center;
        white-space: nowrap;
        vertical-align: middle;
        border-radius: 50%;
        position: absolute;
        right: 160px;
        top: 5px;
        background: #cc3131;
    }
</style>
<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo site_url(); ?>authority/dashboard" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>MB</b></span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b><?php echo SITE_TITLE; ?></b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="javascript:void(0);" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>
        <div class="navbar-custom-menu">
            <a href="<?=base_url('authority/tickets')?>" class="notification" role="button">
                <i class="fa fa-ticket fa-2x"></i>
                <?php if(get_ticket_notification() > 0){ ?>
                    <div class="badge noti-badge"><?=get_ticket_notification()?></div>
                <?php }?>
                

            </a>                  
            <ul class="nav navbar-nav">
                <li class="dropdown user user-menu">
                    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="<?php echo base_url(); ?>assets/uploads/profile_photo/<?php echo $this->session->user_info['profile_photo']; ?>" class="user-image" alt="<?php echo $this->session->user_info['full_name']; ?>">
                        <span class="hidden-xs"><?php echo $this->session->user_info['full_name']; ?></span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="<?php echo base_url(); ?>assets/uploads/profile_photo/<?php echo $this->session->user_info['profile_photo']; ?>" class="img-circle" alt="<?php echo $this->session->user_info['full_name']; ?>">
                            <p>
                                <?php echo $this->session->user_info['full_name']; ?>
                                <?php
                                    if ($this->session->user_info['created_at'] != "0000-00-00") {
                                        ?>
                                        <small>Member since <?php echo date("M. Y", strtotime($this->session->user_info['updated_at'])); ?></small>
                                    <?php }
                                ?>
                            </p>
                        </li>
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="<?php echo site_url(); ?>authority/user/edit/<?php echo $this->session->user_info['id']; ?>" class="btn btn-default btn-flat">Profile</a>
                            </div>
                            <div class="pull-right">
                                <a href="<?php echo site_url(); ?>authority/login/logout" class="btn btn-default btn-flat">Sign out</a>
                            </div>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </nav>
</header>
<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?php echo base_url(); ?>assets/uploads/profile_photo/<?php echo $this->session->user_info['profile_photo']; ?>" class="img-circle" alt="<?php echo $this->session->user_info['full_name']; ?>">
            </div>
            <div class="pull-left info">
                <p><?php echo $this->session->user_info['full_name']; ?></p>
            </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
            <li class="<?php echo get_active_class("dashboard"); ?>">
                <a href="<?= base_url('authority/dashboard')?>">
                    <i class="fa fa-th"></i> <span>Dashboard</span>
                </a>
            </li>

            <li class="treeview <?php echo get_active_class("my-account"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-key"></i> <span>My account</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("change-password"); ?>"><a href="<?php echo site_url(); ?>authority/account/change-password"><i class="fa fa-circle-o"></i>Change password</a></li>
                </ul>
            </li>

            <li class="treeview <?php echo get_active_class("userlist"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-user"></i> <span>User manage</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <!-- <li class="<?php echo get_active_class("add"); ?>"><a href="<?php echo site_url(); ?>authority/userlist/add"><i class="fa fa-circle-o"></i> Add</a></li> -->

                    <li class="<?php echo get_active_class("view"); ?>"><a href="<?php echo site_url(); ?>authority/userlist/view"><i class="fa fa-circle-o"></i> View</a></li>
                </ul>
            </li>

            <!-- <li class="treeview <?php echo get_active_class("ticket_management"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-user"></i> <span>Ticket management</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("problem"); ?>"><a href="<?php echo site_url(); ?>authority/ticket-management/problem"><i class="fa fa-circle-o"></i> Problem</a></li>

                    <li class="<?php echo get_active_class("view-ticket"); ?>"><a href="<?php echo site_url(); ?>authority/ticket-management/view-ticket"><i class="fa fa-circle-o"></i> View user ticket</a></li>
                </ul>
            </li> -->
            


            <li class="treeview <?php echo get_active_class("feedback"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-comments"></i> <span>Feedback</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("view"); ?>"><a href="<?php echo site_url(); ?>authority/feedback/view"><i class="fa fa-circle-o"></i> View</a></li>
                </ul>
            </li>

            <li class="treeview <?php echo get_active_class("home_slider"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-file-image-o"></i> <span>Home slider</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("view"); ?>"><a href="<?php echo site_url(); ?>authority/home_slider/view"><i class="fa fa-circle-o"></i>view</a></li>
                </ul>
            </li>

            <!-- <li class="treeview <?php echo get_active_class("report"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-th"></i> <span>Report</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("users-registered"); ?>"><a href="<?php echo site_url(); ?>authority/report/users-registered"><i class="fa fa-circle-o"></i>Users registered</a></li>

                    <li class="<?php echo get_active_class("add-posted"); ?>"><a href="<?php echo site_url(); ?>authority/report/add_posted"><i class="fa fa-circle-o"></i>Ads posted</a></li>
                </ul>
            </li> -->

            <!-- <li class="treeview <?php echo get_active_class("order_manage"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-th"></i> <span>Order manage</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("order_manage"); ?>"><a href="<?php echo site_url(); ?>authority/order_manage"><i class="fa fa-circle-o"></i>View</a></li>
                </ul>
            </li> -->

            <li class="treeview <?php echo get_active_class("about"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-info-circle"></i> <span>About</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("view"); ?>"><a href="<?php echo site_url(); ?>authority/about/view"><i class="fa fa-circle-o"></i> view</a></li>
                </ul>
            </li>

            <!-- <li class="treeview <?php echo get_active_class("how-it-work"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-th"></i> <span>How it work</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("view"); ?>"><a href="<?php echo site_url(); ?>authority/how-it-work/view"><i class="fa fa-circle-o"></i> view</a></li>
                </ul>
            </li> -->

            <!-- <li class="treeview <?php echo get_active_class("faq"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-th"></i> <span>Faq</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("view"); ?>"><a href="<?php echo site_url(); ?>authority/faq/view"><i class="fa fa-circle-o"></i> view</a></li>
                </ul>
            </li> -->

            <li class="treeview <?php echo get_active_class("tutorials"); ?><?php echo get_active_class("tutorial_faq"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-th"></i> <span>Tutorials</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("tutorials"); ?>">
                        <a href="<?php echo site_url(); ?>authority/tutorials">
                            <i class="fa fa-circle-o"></i>Tutorials
                        </a>
                    </li>
                    <li class="<?php echo get_active_class("tutorial_faq"); ?>">
                        <a href="<?php echo site_url(); ?>authority/tutorial_faq">
                            <i class="fa fa-circle-o"></i>Tutorials FAQ
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview <?php echo get_active_class("hostings"); echo " ".get_active_class("hosting_price"); echo " ".get_active_class("user_hostings"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-th"></i> <span>Hostings</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("hostings"); ?>">
                        <a href="<?php echo site_url(); ?>authority/hostings">
                            <i class="fa fa-circle-o"></i>Hostings Types
                        </a>
                    </li>
                    <li class="<?php echo get_active_class("hosting_price");?>">
                        <a href="<?php echo site_url(); ?>authority/hosting-price">
                            <i class="fa fa-circle-o"></i>Hosting Plans
                        </a>
                    </li>
                    <li class="<?php echo get_active_class("user_hostings"); ?>">
                        <a href="<?php echo site_url(); ?>authority/user_hostings">
                            <i class="fa fa-circle-o"></i>User Hostings
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview <?php echo get_active_class("domains");echo " ".get_active_class("user_domains")." ".get_active_class("domain_requests");?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-server"></i> <span>Domains</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("domains"); ?>">
                        <a href="<?php echo site_url(); ?>authority/domains">
                            <i class="fa fa-circle-o"></i>Domains
                        </a>
                    </li>
                    <li class="<?php echo get_active_class("user_domains"); ?>">
                        <a href="<?php echo site_url(); ?>authority/user_domains">
                            <i class="fa fa-circle-o"></i>User Domains
                        </a>
                    </li>
                    <li class="<?php echo get_active_class("domain_requests"); ?>">
                        <a href="<?php echo site_url(); ?>authority/domain_requests">
                            <i class="fa fa-circle-o"></i>Domain Requests
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview <?php echo get_active_class("common_features"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-th"></i> <span>Hosting Features</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("common_features"); ?>">
                        <a href="<?php echo site_url(); ?>authority/common_features">
                            <i class="fa fa-circle-o"></i>Hosting Features
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview <?php echo get_active_class("securities"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-th"></i> <span>Security</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("security"); ?>">
                        <a href="<?php echo site_url(); ?>authority/security">
                            <i class="fa fa-circle-o"></i>Security
                        </a>
                    </li>
                    <li class="<?php echo get_active_class("security_features"); ?>">
                        <a href="<?php echo site_url(); ?>authority/security_features">
                            <i class="fa fa-circle-o"></i>Security Features
                        </a>
                    </li>
                </ul>
            </li>
            <li class="treeview <?php echo get_active_class("invoices"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-th"></i> <span>Invoices</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("invoices"); ?>">
                        <a href="<?php echo site_url(); ?>authority/invoices">
                            <i class="fa fa-circle-o"></i>Invoices
                        </a>
                    </li>
                    
                </ul>
            </li>
            <li class="treeview <?php echo get_active_class("tickets"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-ticket"></i> <span>Tickets
                        <?php if(get_ticket_notification() > 0){ ?>
                            <div class="badge noti-badge" style="right: 23px"><?=get_ticket_notification()?></div>
                        <?php }?>
                    </span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("tickets"); ?>">
                        <a href="<?php echo site_url(); ?>authority/tickets">
                            <i class="fa fa-circle-o"></i>Tickets
                        </a>
                    </li>
                    
                </ul>
            </li>
           
            <li class="treeview <?php echo get_active_class("social-links"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-link"></i> <span>Social links</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("view"); ?>"><a href="<?php echo site_url(); ?>authority/social-links/view"><i class="fa fa-circle-o"></i> view</a></li>
                </ul>
            </li>

            <!-- <li class="treeview <?php echo get_active_class("testimonial"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-th"></i> <span>Testimonial</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("testimonial"); ?>"><a href="<?php echo site_url(); ?>authority/testimonial"><i class="fa fa-circle-o"></i>Testimonial</a></li>
                </ul>
            </li> -->

            <!-- <li class="treeview <?php echo get_active_class("website-settings"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-th"></i> <span>Website Settings</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("view"); ?>"><a href="<?php echo site_url(); ?>authority/website-settings/view"><i class="fa fa-circle-o"></i> view</a></li>
                </ul>
            </li> -->

            <li class="treeview <?php echo get_active_class("contact_us"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-address-book"></i> <span>Contact</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("view"); ?>"><a href="<?php echo site_url(); ?>authority/contact_us/view"><i class="fa fa-circle-o"></i>view</a></li>
                </ul>
            </li>

            <!-- <li class="treeview <?php echo get_active_class("subscribe"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-user"></i> <span>Subscribe list</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("view"); ?>"><a href="<?php echo site_url(); ?>authority/subscribe/view"><i class="fa fa-circle-o"></i>view</a></li>
                </ul>
            </li> -->

            <!-- <li class="treeview <?php echo get_active_class("news_letter"); ?>">
                <a href="javascript:void(0);">
                    <i class="fa fa-th"></i> <span>News letter</span>
                    <span class="pull-right-container">
                        <i class="fa fa-angle-left pull-right"></i>
                    </span>
                </a>
                <ul class="treeview-menu">
                    <li class="<?php echo get_active_class("view"); ?>"><a href="<?php echo site_url(); ?>authority/news_letter/view"><i class="fa fa-circle-o"></i> view</a></li>
                </ul>
            </li> -->

			<li class="<?= $this->uri->segment(2) == "settings" ? "active" : ""; ?>"><a href="<?php echo site_url(); ?>authority/settings"><i class="fa fa-cog"></i>Settings</a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>