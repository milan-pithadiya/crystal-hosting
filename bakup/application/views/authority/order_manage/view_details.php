<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
?>
<?php $this->view('authority/common/header'); ?>
<?php $this->view('authority/common/sidebar'); ?>

<style>
.form-control.error{border:1px solid red;}
</style>

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
<style type="text/css">
    a.disabled {
    pointer-events: none;
    cursor: default;
    }

    input[type="radio"] {
    position: absolute;
    }
    .btn.btn-default.active, .btn.btn-default:focus, .btn.btn-default:hover {
        background:#449D44;
    }
    .active_order
    {
        background-color: #FB6752;
        color: #fff;
    }
</style>

<div id="main-container">
    <div id="breadcrumb">
        <ul class="breadcrumb" style="padding:8px 20px;">
            <li><i class="fa fa-home"></i><a href="<?php echo base_url('authority/dashboard'); ?>"> Home</a></li>
            <li class="active"> <i class="fa fa-cart-arrow-down"></i> Order view
            </li>
        </ul>
    </div>

    <!-- /breadcrumb-->
    <div class="main-header clearfix grey-container shortcut-wrapper">
        <div class="page-title">
            <h3 class="no-margin"><i class="fa fa-cart-arrow-down fa-2x"></i> Order view 
                (Order No : <?php echo ($order_details !=null)  ? $order_details[0]['order_id'].')' : "";?> 
                <?php
                    if (isset($payment_details) && $payment_details !=null) {
                        echo ', (Cashfree Refrence Id : '.$payment_details->referenceId.')';
                    }
                ?>
                <label class="hdr_status"></label>
            </h3>
        </div>
        <!-- /page-title -->
    </div>

    <!-- /main-header -->
    <div class="padding-md">
        <div class="panel panel-default table-responsive">
            <!-- <?php// $this->load->view('admin/include/messages');?> -->
            <div class="panel-heading">
                <div class="inline-block m-top-xs pull-right">
                    <span class="badge "></span>
                    <strong class="m-left-xs m-right-sm"></strong>
                </div>
                <!-- </form> -->
            </div>
            <div class="padding-md clearfix">
                <?php /*?>
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th colspan="7" align="right">
                                <h4><b><i class="fa fa-asterisk" aria-hidden="true"></i> Posting Details :-</b></h4>
                            </th>
                        </tr>
                        <tr>
                            <th>No</th>
                            <th>Post Details</th>
                            <th>Floor no</th>
                            <th>Instruction</th>
                            <th>Order Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (isset($order_details) && $order_details !=null) {
                            // foreach ($order_details as $key => $order_details_row) { 
                            ?>
                                <tr>
                                    <td>1</td>
                                    <td><?= $order_details[0]['address']?></td>
                                    <td><?= $order_details[0]['floor_no']?></td>
                                    <td><?= $order_details[0]['instruction']?></td>
                                    
                                    <td><label class="btn btn-xs btn-warning"><?= date('d-m-Y h:i:s A',strtotime($order_details[0]['order_date']))?></label></td>
                                </tr>
                            <?php
                            } 
                        ?>        
                    </tbody>
                </table>
                <?php */?>

                <div id="loading" style="display:none;position: absolute;margin: 0% 0 0 30%;"><img src="<?= base_url('assets/img/ajaxloader.gif')?>"></div>

                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th colspan="7" align="right">
                                <h4><b><i class="fa fa-asterisk" aria-hidden="true"></i> Post Details:-</b></h4>
                            </th>
                        </tr>
                        <tr>
                            <th>No</th>
                            <th>Post Details</th>
                            <th>Package Price </th>
                            <th>Create Date</th>
                            <th>Final Price </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            if (isset($order_details) && $order_details !=null) {
                                // echo"<pre>"; print_r($order_details); exit;
                                // foreach ($order_details as $key => $order_details_row) {
                                ?>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            <b>Category name :</b> <?= $order_details[0]['category_name']?><br>
                                            <?php
                                                if (isset($order_details[0]['sub_category_name'])) {
                                                ?>
                                                    <b>Sub category name :</b> <?= $order_details[0]['sub_category_name']?><br>
                                                <?php }
                                            ?>
                                            <b>Add type :</b> <?= $order_details[0]['add_type']?><br>
                                            <b>I want :</b> <?= $order_details[0]['i_want']?><br>
                                            <b>Post description :</b> <?= $order_details[0]['post_description']?><br>
                                            <b>Set Price :</b> 
                                                <?php
                                                if ($order_details[0]['currency'] == 'INR') {
                                                    echo 'Rs '.$order_details[0]['price'];
                                                }
                                                elseif ($order_details[0]['currency'] == 'USD') {
                                                    echo '$ '.$order_details[0]['price'];
                                                }
                                                elseif ($order_details[0]['currency'] == 'EUR') {
                                                    echo '€ '.$order_details[0]['price'];
                                                }
                                                elseif ($order_details[0]['currency'] == 'GBP') {
                                                    echo '£ '.$order_details[0]['price'];
                                                }
                                            ?> <br>
                                            <b>Country/state/city :</b> <?= $order_details[0]['country_name'].' , '.$order_details[0]['city_name'].' , ',$order_details[0]['city_name']?><br>
                                        </td>

                                        <td>
                                            <?php
                                                if ($order_details[0]['currency'] == 'INR') {
                                                    echo 'Rs '.$order_details[0]['package_price'];
                                                }
                                                elseif ($order_details[0]['currency'] == 'USD') {
                                                    echo '$ '.$order_details[0]['package_price'];
                                                }
                                                elseif ($order_details[0]['currency'] == 'EUR') {
                                                    echo '€ '.$order_details[0]['package_price'];
                                                }
                                                elseif ($order_details[0]['currency'] == 'GBP') {
                                                    echo '£ '.$order_details[0]['package_price'];
                                                }
                                            ?>    
                                        </td>

                                        <td><?= $order_details !=null ? date('d-m-Y h:i:s A',strtotime($order_details[0]['create_date']))  : '';?></td>
                                        <td>
                                            <?php
                                                if ($order_details[0]['currency'] == 'INR') {
                                                    echo 'Rs '.$order_details[0]['package_price'];
                                                }
                                                elseif ($order_details[0]['currency'] == 'USD') {
                                                    echo '$ '.$order_details[0]['package_price'];
                                                }
                                                elseif ($order_details[0]['currency'] == 'EUR') {
                                                    echo '€ '.$order_details[0]['package_price'];
                                                }
                                                elseif ($order_details[0]['currency'] == 'GBP') {
                                                    echo '£ '.$order_details[0]['package_price'];
                                                }
                                            ?>                                                
                                        </td>
                                    </tr>
                                <?php //}
                            }   
                        ?>
                    </tbody>
                </table>
            </div>            
            
            <div class="clearfix">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th colspan="7" align="right">
                                <h4><b><i class="fa fa-asterisk" aria-hidden="true"></i> Posting Images :-</b></h4>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                <?php
                                    if (isset($post_images) && $post_images !=null) {
                                        foreach ($post_images as $key => $value) { 
                                        ?>
                                            <div class="col-md-1">
                                                <img src="<?= base_url(POST_NEED_IMG.$value['similar_image'])?>" onerror="this.src='<?= base_url('assets/uploads/default_img.png')?>'" height="80px" width="80px">
                                            </div>
                                        <?php }
                                    } 
                                    else{
                                        echo"<h4>No Images...!</h4>";
                                    }
                                ?>  
                            </td>
                        </tr>      
                    </tbody>
                </table>
                <table class="table">
                    <thead>
                        <?php 
                            if(isset($order_details) && $order_details !=null) { 
                            // echo "<pre>"; print_r($order_details);
                                $status = $order_details[0]['order_status'];
                            ?>
                                <tr>
                                    <th colspan="5" style="float: right; margin: 0 20px 0 0;">
                                        <h4><b><i class="fa fa-check" aria-hidden="true"></i> Total Payment:-  
                                            <?php
                                                if ($order_details[0]['currency'] == 'INR') {
                                                    echo 'Rs '.$order_details[0]['package_price'];
                                                }
                                                elseif ($order_details[0]['currency'] == 'USD') {
                                                    echo '$ '.$order_details[0]['package_price'];
                                                }
                                                elseif ($order_details[0]['currency'] == 'EUR') {
                                                    echo '€ '.$order_details[0]['package_price'];
                                                }
                                                elseif ($order_details[0]['currency'] == 'GBP') {
                                                    echo '£ '.$order_details[0]['package_price'];
                                                }
                                            ?> 
                                        </b></h4>
                                    </th>
                                </tr>
                                <td>
                                    <input type="hidden" name="order_number" class="order_number" value="<?= $order_details[0]['post_id']?>">
                                    <?php
                                        if ($status !=3) {
                                        ?>
                                                <button type="radio" id="failed" class="order_status_value btn" value="0">Pending</button>
                                                
                                                <button type="radio" id="pending" class="order_status_value btn" value="1">Failed</button>
                                                
                                                <button type="radio" id="progress" class="order_status_value btn" value="2">In progress</button>
                                        <?php }
                                    ?>
                                    <button type="radio" id="success" class="order_status_value btn btn-<?= $status == 3 ? 'success' : ''?>" <?= $status == 3 ? 'disabled' : ''?> value="3">Complete</button>   
                                </td>
                            <?php
                            }    
                        ?>                    
                    </thead>
                </table>
            </div>            
        </div>
    </div>
</div>
</div>

<?php $this->view('authority/common/copyright'); ?>

<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-validate/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/bower_components/select2/dist/js/select2.full.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jQueryUI/jquery-ui.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery-ui-timepicker-addon.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/admin/plugins/jquery-ui-timepicker/js/jquery.ui.touch-punch.min.js"></script>

<?php $this->view('authority/common/footer'); ?>

<!-- Order Status -->
<script type="text/javascript">
    $(document).ready(function() {
        get_order_status();

        $('.order_status_value').click(function() {

            var order_status_id = $(this).val();
            var order_number = $('.order_number').val();
            var dataString = {order_number:order_number,order_status_id:order_status_id};

            $.ajax({
                type: "POST",
                url: "<?= base_url('authority/order_manage/order_status_change')?>",
                data:dataString,
                dataType: "html",
                beforeSend: function(){
                    // Show image container
                    $("#loading").show();
                    $("#main-container").css("opacity",0.2);
                },
                success: function(data) 
                {
                    if (data == 3){
                        $('#failed').hide();
                        $('#pending').hide();
                        $('#progress').hide();
                        $('#success').attr("disabled", true);
                    }

                    if (data == 0)
                    {
                        $("#failed").addClass("btn-warning");    
                        $(".hdr_status").html('<label class="btn btn-xs btn-warning">Pending</label>');   
                    }else{
                        $("#failed").removeClass('btn-warning');
                    }
                    if (data == 1)
                    {
                        $("#pending").addClass("btn-danger");
                        $(".hdr_status").html('<label class="btn btn-xs btn-danger">Failed</label>'); 
                    }
                    else{
                        $("#pending").removeClass('btn-danger');   
                    }
                    if (data == 2)
                    {
                        $("#progress").addClass("btn-primary");
                        $(".hdr_status").html('<label class="btn btn-xs btn-primary">In progress</label>');
                    }
                    else{
                        $("#progress").removeClass('btn-primary');
                    }
                    if (data == 3)
                    {
                        $("#success").addClass("btn-success");
                        $(".hdr_status").html('<label class="btn btn-xs btn-success">Complete</label>');
                    }
                    else{
                        $("#success").removeClass('btn-success');
                    }
                    // location.reload();
                },
                complete:function(data){
                    // Hide image container
                    $("#loading").hide();
                    $("#main-container").css("opacity",1.5);
                }
            }); 
        });
    });

    function get_order_status(){
        var order_number = $('.order_number').val();
        var dataString = {order_number:order_number};

        $.ajax({
            type: "POST",
            url: "<?= base_url('authority/order_manage/get_order_status')?>",
            data:dataString,
            dataType: "html",
            success: function(data) 
            {
                if (data == 0)
                {
                    $("#failed").addClass("btn-warning");
                    $(".hdr_status").html('<label class="btn btn-xs btn-warning">Pending</label>');   
                }else{
                    $("#failed").removeClass('btn-warning');
                }
                if (data == 1)
                {
                    $("#pending").addClass("btn-danger");
                    $(".hdr_status").html('<label class="btn btn-xs btn-danger">Failed</label>');
                }
                else{
                    $("#pending").removeClass('btn-danger');
                }
                if (data == 2)
                {
                    $("#progress").addClass("btn-primary");
                    $(".hdr_status").html('<label class="btn btn-xs btn-primary">In progress</label>');
                }
                else{
                    $("#progress").removeClass('btn-primary');
                }
                if (data == 3)
                {
                    $("#success").addClass("btn-success");
                    $(".hdr_status").html('<label class="btn btn-xs btn-success">Complete</label>');
                }
                else{
                    $("#success").removeClass('btn-success');
                }
                // alert(data);
            }
        }); 
    }
</script>