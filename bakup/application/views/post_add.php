<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$this->load->view('include/header');
?>        
<!-- Post Ad Page Start Here -->
<section class="s-space-bottom-full bg-accent-shadow-body">
    <div class="container">
        <div class="breadcrumbs-area">
            <ul>
                <li><a href="<?= base_url() ?>">Home</a> -</li>
                <li class="active">Post A Add</li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-sm-12 col-12 mb--sm">
                <?php $this->load->view('include/messages'); ?>

                <div class="gradient-wrapper">
                    <div class="gradient-title">
                        <h2>Post A Add</h2>
                    </div>
                    <div class="input-layout1 gradient-padding post-ad-page">
                        <?php
                            if (isset($post_details) && $post_details !=null) {
                                $post_id = $post_details[0]['post_id'];
                                $category_id = $post_details[0]['category_id'];
                                $sub_category_id = $post_details[0]['sub_category_id'];
                                $add_type = $post_details[0]['add_type'];
                                $i_want = $post_details[0]['i_want'];
                                $description = $post_details[0]['post_description'];
                                $price = $post_details[0]['price'];
                            }
                            $action = isset($post_details) && $post_details !=null ? base_url('post_management/update_post') : isset($post_details) && $post_details !=null;
                        ?>
                        <form id="post-add-form" method="post" action="<?= $action?>" enctype='multipart/form-data'>
                            <input type="hidden" name="post_id" class="post_id" value="<?= isset($post_id) && $post_id !=null ? $post_id : '';?>">
                            <input type="hidden" name="sub_category_id" class="sub_category_id" value="<?= isset($sub_category_id) && $sub_category_id !=null ? $sub_category_id : '';?>">
                            <div class="mb-20 pb-10">
                                <div class="section-title-left-dark border-bottom d-flex">
                                    <h3><img src="<?= base_url() ?>assets/image/post-add1.png" alt="post-add" class="img-fluid"> Product Information</h3>
                                </div>
                                <div class="row">
                                    <?php // Check profile complete or not. 
                                        $check_profile = 'TRUE';
                                        $user_details = $this->Production_model->get_all_with_where('user_register', '', '', array('user_id' => $this->session->userdata('login_id')));
                                        if (isset($user_details) && $user_details !=null) {
                                            if ($user_details[0]['id_country'] == 0 || $user_details[0]['id_state'] == 0 || $user_details[0]['id_city'] == 0) {
                                                $check_profile = 'FALSE';
                                            ?>
                                                <div class="col-sm-5 col-12"></div>
                                                <div class="col-sm-6 col-12">
                                                    <label class="control-label" style="color:red;">Please complete your profile after add this post...!</label>
                                                </div>
                                            <?php }
                                        }
                                    ?>
                                    <div class="col-sm-3 col-12">
                                        <label class="control-label">Category<span> *</span></label>
                                    </div>
                                    <div class="col-sm-9 col-12">
                                        <div class="form-group">
                                            <div class="custom-select">
                                                <select id="category_id" name="category_id" class='form-control select2'>
                                                    <option value="">Select a Category</option>
                                                    <?php
                                                    if (isset($category_details) && $category_details != null) {
                                                        foreach ($category_details as $key => $value) {
                                                            ?>
                                                            <option value="<?= $value['category_id'] ?>" <?= isset($category_id) && $category_id !=null && $category_id == $value['category_id'] ? 'selected' : '';?>><?= $value['category_name'] ?></option>
                                                        <?php
                                                        }
                                                    }
                                                    ?>
                                                </select>
                                                <div id="category_id_error"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3 col-12">
                                        <label class="control-label">Sub Category</label>
                                    </div>
                                    <div class="col-sm-9 col-12">
                                        <div class="form-group">
                                            <div class="custom-select">
                                                <select id="sub_category_id" name="sub_category_id" class='form-control select2'>
                                                    <option value="">Select a sub Category</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-sm-3 col-12">
                                        <label class="control-label possition-top" for="first-name">Add Type <span> *</span></label>
                                    </div>
                                    <div class="col-sm-9 col-12">
                                        <div class="form-group">
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="inlineRadio1" value="Individual" name="add_type" <?= isset($add_type) && $add_type !=null && $add_type == 'Individual' ? 'checked' : 'checked';?>>
                                                <label for="inlineRadio1">Individual</label>
                                            </div>
                                            <div class="radio radio-primary radio-inline">
                                                <input type="radio" id="inlineRadio2" value="Business" name="add_type" <?= isset($add_type) && $add_type !=null && $add_type == 'Business' ? 'checked' : '';?>>
                                                <label for="inlineRadio2"> Business </label>
                                            </div>
                                            <div id="add_type_error"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3 col-12">
                                        <label class="control-label" for="add-title">I want <span> *</span></label>
                                    </div>
                                    <div class="col-sm-9 col-12">
                                        <div class="form-group">
                                            <input type="text" id="i_want" name="i_want" class="form-control" placeholder="Ex. mobile" value="<?= isset($i_want) && $i_want !=null ? $i_want : '';?>">
                                            <div id="i_want_error"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3 col-12">
                                        <label class="control-label">Description<span> *</span></label>
                                    </div>
                                    <div class="col-sm-9 col-12">
                                        <div class="form-group">
                                            <textarea placeholder="What makes your ad unique" class="textarea form-control" name="post_description" id="post_description" rows="4" cols="20" data-error="Message field is required" required><?= isset($description) && $description !=null ? $description : '';?></textarea>
                                            <div id="post_description_error"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3 col-12">
                                        <label class="control-label" for="first-name">Set Price <span> *</span></label>
                                    </div>
                                    <div class="col-sm-9 col-12">
                                        <div class="form-group">
                                            <input type="text" id="price" name="price" class="form-control price-box only_digits" maxlength="10" placeholder="Rs. Set your Price Here" value="<?= isset($price) && $price !=null ? $price : '';?>">
                                            <div id="price_error"></div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-sm-3 col-12">
                                        <label class="control-label" for="add-title">Upload Picture</label><br><br>
                                        <p class="control-label">Upload by(815&#xd7;459)</p>
                                    </div>
                                    <div class="col-sm-9 col-12">
                                        <div class="form-group">
                                            <ul class="picture-upload">
                                                <li>
                                                    <input type="file" name="similar_image[]" id="similar_image" class="form-control" multiple accept="image/*">
                                                </li>
                                                <!-- <li>
                                                    <input type="file" id="img-upload2" class="form-control">
                                                </li>
                                                <li>
                                                    <input type="file" id="img-upload3" class="form-control">
                                                </li> -->
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <?php
                                        if ($this->session->userdata('login_id') !=null) {
                                        ?>
                                            <div class="col-sm-3 col-12">
                                                <label class="control-label" for="add-title">Uploaded Picture</label><br>
                                            </div>
                                        <?php }
                                    ?>
                                    <div class="col-sm-9 col-12">
                                        <div class="form-group">
                                            <?php                                               
                                                if (isset($post_images) && $post_images != null) {   
                                                    foreach ($post_images as $key => $value) {
                                                        $img_id = $value['image_id'];
                                                    ?>
                                                        <img src="<?= base_url(POST_NEED_IMG.'thumbnail/').$value['similar_image']?>" height="50px" width="50px">
                                                        <a href="<?= base_url('post_management/delete_image/'.$img_id)?>" title="Delete image" class="" onclick="return confirm('Are you sure you want to delete this image?');"><i class="fa fa-trash-o fa-2x" aria-hidden="true"></i></a>         
                                                    <?php } 
                                                }
                                            ?>
                                        </div>
                                    </div>
                                </div>
                                <?php
                                if ($this->session->userdata('login_id') == null) {
                                    ?>
                                    <div class="row">
                                        <div class="col-sm-3 col-12">
                                            <label class="control-label top-0">Full Name <span> *</span></label>
                                        </div>
                                        <div class="col-sm-9 col-12">
                                            <div class="form-group">
                                                <input type="text" id="name" name="name" class="form-control txtonly" placeholder="Enter full name" value="<?= isset($user_details) && $user_details != null ? $user_details[0]['name'] : ''; ?>">
                                                <div id="name_error"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3 col-12">
                                            <label class="control-label top-0">Phone number <span> *</span></label>
                                        </div>
                                        <div class="col-sm-9 col-12">
                                            <div class="form-group">
                                                <input type="text" id="mobile_no" name="mobile_no" class="form-control only_digits" placeholder="Enter phone number" maxlength="10" value="<?= isset($user_details) && $user_details != null ? $user_details[0]['mobile_no'] : ''; ?>">
                                                <div id="mobile_no_error"></div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-3 col-12">
                                            <label class="control-label top-0">Email <span> *</span></label>
                                        </div>
                                        <div class="col-sm-9 col-12">
                                            <div class="form-group">
                                                <input type="text" id="user_email" name="user_email" class="form-control" placeholder="Email Address" value="<?= isset($user_details) && $user_details != null ? $user_details[0]['user_email'] : ''; ?>">
                                                <div id="valid" style="color: #F66249"></div>
                                                <div id="user_email_error"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 col-12">
                                            <label class="control-label top-0">New Password <span> *</span></label>
                                        </div>
                                        <div class="col-sm-9 col-12">
                                            <div class="form-group">
                                                <input type="password" id="n-password" name="password" class="form-control" placeholder="New Password">
                                                <div id="password_error"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3 col-12">
                                            <label class="control-label top-0">Confirm Password <span> *</span></label>
                                        </div>
                                        <div class="col-sm-9 col-12">
                                            <div class="form-group">
                                                <input type="password" id="r-password" name="confirm_password" class="form-control" placeholder="Type Your Confirm Password">
                                                <div id="confirm_password_error"></div>
                                            </div>
                                        </div>
                                    </div>                                            
                                <?php }
                                ?>
                                <div class="row">
                                    <?php
                                    $package_id = '';
                                    if (isset($package_details) && $package_details != null) {
                                        foreach ($package_details as $key => $value) {
                                            if($key == 0) {
                                                $package_id = $value['id'];
                                            }
                                            ?>
                                            <div class="col-md-4">
                                                <div class="pricing-box bg-body active-pricing-box" package-id="<?php echo $value['id']; ?>" data-price='<?= $value['price']?>'>
                                                    <div class="plan-title"><?= $value['name'] ?></div>
                                                    <div class="price">
                                                        <span class="currency">
                                                            <?php
                                                                if ($value['currency'] == 'INR') {
                                                                    echo 'Rs';
                                                                }
                                                                elseif ($value['currency'] == 'USD') {
                                                                    echo '$';
                                                                }
                                                                elseif ($value['currency'] == 'EUR') {
                                                                    echo '€';
                                                                }
                                                                elseif ($value['currency'] == 'GBP') {
                                                                    echo '£';
                                                                }
                                                            ?>
                                                        </span><?= $value['price'] ?>
                                                        <span class="duration">/ Per <?= $value['day_month'] ?></span>
                                                    </div>
                                                    <p><?= $value['discription'] ?></p>
                                                </div>
                                            </div>
                                        <?php
                                        }
                                    }
                                    ?>
                                </div>
                            </div>
                            <label class="pricing_msg"></label>
                            <div class="form-group mb-0">
                                <input type="hidden" name="package_id" value="<?php echo $package_id; ?>"/>
                                <button type="submit" class="cp-default-btn-sm-primary border-0 w-100" <?= $check_profile == 'FALSE' ? 'disabled' : '';?>>Submit Now!</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Post Ad Page End Here -->
</div>
<!-- Modal Start-->
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <div class="title-default-bold mb-none">Login</div>
            </div>
            <div class="modal-body">
                <div class="login-form">
                    <form>
                        <label>Username or email address *</label>
                        <input type="text" placeholder="Name or E-mail" />
                        <label>Password *</label>
                        <input type="password" placeholder="Password" />
                        <div class="checkbox checkbox-primary">
                            <input id="checkbox2" type="checkbox">
                            <label for="checkbox2">Remember Me</label>
                        </div>
                        <button class="default-big-btn" type="submit" value="Login">Login</button>
                        <button class="default-big-btn form-cancel" type="submit" value="">Cancel</button>
                        <label class="lost-password"><a href="#">Lost your password?</a></label>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal End-->

<?php $this->load->view('include/copyright'); ?>

<script>
    /*FORM VALIDATION*/
    $("#post-add-form").validate({
        rules: {
            'category_id': {required: true},
            'add_type': {required: true},
            'i_want': {required: true},
            'post_description': {required: true},
            'address': {required: true},
            'price': {required: true, number: true},

            'name': {required: true},
            'mobile_no': {required: true, minlength: 10},
            'user_email': {required: true, email: true},
            'password': {required: true},
            'confirm_password': {required: true, equalTo: "#n-password"},
        },
        errorPlacement: function (error, element) {
            var name = $(element).attr("name");            
            error.appendTo($("#" + name + "_error"));
        },
        messages: {
            'category_id': "Please select category",
            'add_type': "Select add type",
            'i_want': "Please enter I want",
            'post_description': "Enter description",
            'address': "Please enter address",
            'price': {required: 'Please enter price', number: 'Please enter only number'},

            'name': "Please enter name",
            'mobile_no': {required: 'Please enter mobile no', minlength: 'minimum 10 digits'},
            'user_email': {required: 'Please enter email-id', email: 'Please enter valid email'},
            'password': "Please enter password",
            'confirm_password': {required: 'Enter confirm password', equalTo: "Password and confirm password must be same"},
        }
    });

    $(document).ready(function() {
        var post_id = $('.post_id').val();
        if (post_id == ''){
            $(".pricing-box").click(function () {
                var price = $(this).attr('data-price');
                if (price == 0)
                {
                    $("form").attr('action', '<?= base_url('post_add/add_post') ?>');
                } else {
                    $("form").attr('action', '<?= base_url('post_add/add_post_online') ?>');
                }
                $('[name="package_id"]').val($(this).attr('package-id'));        
                $(".pricing-box").removeClass('selected');
                $(this).closest('.pricing-box').addClass('selected');
            });

            $("form").submit(function () {
                if ($("form").valid()) {
                    if ($(".selected").length == 0) {
                        $(".pricing_msg").text('Please select one packages...!').css('color', '#e81123');
                        ;
                        return false;
                    }
                }
            });
        }

        $("#category_id").change(function () {
            var cat_id = $('#category_id').val();
            var sub_cat_id = $('.sub_category_id').val();
            $.ajax({
                url: "<?= base_url('post_add/get_sub_cat_name') ?>",
                data: {category_id: cat_id,sub_category_id:sub_cat_id},
                type: "POST",
                dataType: "html",
                success: function (data) {
                    $("#sub_category_id").html(data);
                }
            });
        }).trigger('change');
    });
</script>

<?php $this->load->view('include/footer'); ?>